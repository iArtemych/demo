//
//  AuthDataRepository.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import RealmSwift

protocol AuthDataRepositoryProtocol {
    func get() -> [AuthDataRealm]
    func save(authData: AuthDataRealm)
    func deleteAllData()
}

class AuthDataRepository: AuthDataRepositoryProtocol {
    private let realm: Realm
    
    init(realm: Realm) {
        self.realm = realm
    }
    
    func get() -> [AuthDataRealm] {
        let objects = realm.objects(AuthDataRealm.self)
        return objects.map({ object -> AuthDataRealm in
            return AuthDataRealm(value: object)
        })
    }
    
    func save(authData: AuthDataRealm) {
        deleteAllData()
        try! realm.write {
            realm.add(authData)
        }
    }
    
    func deleteAllData() {
        try! realm.write {
            realm.delete(realm.objects(AuthDataRealm.self))
        }
    }
}
