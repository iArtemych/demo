//
//  RepositoriesFactory.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation
import RealmSwift

class RepositoriesFactory: NSObject {
    private let realm = try! Realm()
    
    func createAuthDataRepository() -> AuthDataRepositoryProtocol {
        return AuthDataRepository(realm: realm)
    }
    
    func createClientDataRepository() -> ClientDataRepositoryProtocol {
        return ClientDataRepository(realm: realm)
    }
}
