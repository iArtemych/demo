//
//  ClientDataRepositoryProtocol.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import RealmSwift
import RxSwift
import RxRelay

protocol ClientDataRepositoryProtocol {
    var profileInfoUpdates: Observable<ClientApiObject?> { get }
    func get() -> ClientApiObject?
    func save(authData: ClientApiObject)
    func deleteAllOldAuthData()
}

class ClientDataRepository: ClientDataRepositoryProtocol {

    // MARK: - Public

    let profileInfoUpdates: Observable<ClientApiObject?>
    
    func get() -> ClientApiObject? {
        let objects = realm.objects(ClientRealm.self)
        return objects.first?.toNormal()
    }
    
    func save(authData: ClientApiObject) {
        deleteAllOldAuthData()
        try! realm.write {
            realm.add(authData.toRealm())
        }
        _profileInfoUpdates.accept(authData)
    }
    
    func deleteAllOldAuthData() {
        try! realm.write {
            realm.delete(realm.objects(ClientRealm.self))
        }
        _profileInfoUpdates.accept(nil)
    }

    // MARK: - Lifecycle

    init(realm: Realm) {
        self.realm = realm
        self.profileInfoUpdates = _profileInfoUpdates.asObservable()
    }

    // MARK: - Private

    private let realm: Realm
    private let _profileInfoUpdates = PublishRelay<ClientApiObject?>()
}

