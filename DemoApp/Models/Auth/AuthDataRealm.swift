//
//  AuthDataRealm.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation
import RealmSwift

class AuthData: Codable {
    var accessToken = ""
    var refreshToken = ""
    var accessTokenLifeTime = ""

    private enum CodingKeys: String, CodingKey {
        case accessToken
        case refreshToken
        case accessTokenLifeTime
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let accessToken = try? container.decode(String.self, forKey: .accessToken) {
            self.accessToken = accessToken
        }
        if let refreshToken = try? container.decode(String.self, forKey: .refreshToken) {
            self.refreshToken = refreshToken
        }
        if let accessTokenLifeTime = try? container.decode(String.self, forKey: .accessTokenLifeTime) {
            self.accessTokenLifeTime = accessTokenLifeTime
        }
    }
}

class AuthDataRealm: Object {
    @objc dynamic var accessToken: String = ""
    @objc dynamic var refreshToken: String = ""
    @objc dynamic var accessTokenLifeTime: String = ""
}
