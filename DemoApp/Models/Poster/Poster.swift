//
//  Poster.swift
//  
//
//  Created by Артем Чурсин on 19.04.2021.
//

import Foundation
import UIKit

// MARK: - Poster
class Poster: Codable {
    let activationDate: String?
    let id: Int?
    let imageURL: String?
    let rejectReason, status, text: String?
    
    var adStatus: AdStatus? {
        get {
            print(id!)
            return AdStatus(rawValue: status ?? "ACTIVE")
        }
    }

    enum CodingKeys: String, CodingKey {
        case activationDate, id
        case imageURL = "imageUrl"
        case rejectReason, status, text
    }
    
    enum AdStatus: String {
        case disabled = "DISABLED"
        case rejected = "REJECTED"
        case active = "ACTIVE"
        case verifaction = "VERIFICATION"
        case noActive = "ACTIVE_NOT_VERIFIED"
        
        var color: UIAdvertismentView.LeftStatusMode {
            switch self {
            case .disabled,
                 .rejected:
                return .red
            case .active,
                 .verifaction,
                 .noActive:
                return .green
            }
        }
        
        var text: String {
            switch self {
            case .disabled:
                return "Отключено"
            case .rejected:
                return "Отклонено"
            case .active:
                return "Активное"
            case .verifaction:
                return "Проверка"
            case .noActive:
                return "Активное, на проверке"
            }
        }
    }

    init(activationDate: String? = nil,
         id: Int? = nil,
         imageURL: String? = nil,
         rejectReason: String? = nil,
         status: String? = nil,
         text: String? = nil) {
        self.activationDate = activationDate
        self.id = id
        self.imageURL = imageURL
        self.rejectReason = rejectReason
        self.status = status
        self.text = text
    }
}
