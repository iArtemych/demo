//
//  SexModel.swift
//  
//
//  Created by Artem Chursin on 03.05.2021.
//

enum Sex: String {
    case male = "boys"
    case female = "girls"
    case all = "all"
    
    var sexString: String {
        switch self {
        case .male:
            return "male"
        case .female:
            return "female"
        case .all:
            return "all"
        }
    }
    
    var apiSex: String {
        switch self {
        case .male:
            return "boys"
        case .female:
            return "girls"
        case .all:
            return "all"
        }
    }
}
