//
//  ClientRealm.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import RealmSwift

// MARK: - Client
class ClientRealm: Object {
    @objc dynamic var id: Int = 1
    @objc dynamic var email: String = ""
    @objc dynamic var city: CityInfoRealm?
    @objc dynamic var currentRegion: RegionInfoRealm?
    @objc dynamic var name: String = ""
    @objc dynamic var sex: String = ""
    @objc dynamic var sexPreference: String = ""
    @objc dynamic var avatarURL: String = ""

    func toNormal() -> ClientApiObject {
        let data = ClientApiObject(id: id)
        data.id = id
        
        data.email = email
        data.city = city?.toNormal()
        data.currentRegion = currentRegion?.toNormal()
        data.name = name
        
        data.sex = sex
        data.sexPreference = sexPreference
        data.avatarURL = avatarURL
        
        return data
    }
}

// MARK: - CityInfo
class CityInfoRealm: Object {
    @objc dynamic var id: Int = 1
    @objc dynamic var title: String = ""
    
    func toNormal() -> City {
        let data = City()
        
        data.id = id
        data.title = title
        
        return data
    }
}

// MARK: - NotificationsSettings
class NotificationsSettingsRealm: Object {
    @objc dynamic var likes: Bool = true
    @objc dynamic var messages: Bool = true
    @objc dynamic var system: Bool = true
    
    func toNormal() -> NotificationsSettings {
        let data = NotificationsSettings()
        
        data.likes = likes
        data.messages = messages
        data.system = system
        
        return data
    }
}

// MARK: - RegionInfo
class RegionInfoRealm: Object {
    @objc dynamic var id: Int = 1
    @objc dynamic var title: String = ""
    @objc dynamic var location: LocationInfoRealm?
    
    func toNormal() -> Region {
        let data = Region()
        
        data.id = id
        data.title = title
        data.location = location?.toNormal()
        
        return data
    }
}

// MARK: - LocationInfo
class LocationInfoRealm: Object {
    @objc dynamic var latitude: Double = 0.0
    @objc dynamic var longitude: Double = 0.0
    
    func toNormal() -> Location {
        let data = Location()
        
        data.latitude = latitude
        data.longitude = longitude
        
        return data
    }
}
