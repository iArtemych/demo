//
//  CityList.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation

struct CityList: Codable {
    var cityList = [City]()
}
