//
//  AuthCodeInfo.swift
//  
//
//  Created by Artem Chursin on 31.03.2021.
//

struct AuthCodeInfo: Codable {
    let code: Int?
}
