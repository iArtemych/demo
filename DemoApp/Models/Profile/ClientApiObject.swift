//
//  ClientApiObject.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation
import UIKit

// MARK: - Client
class ClientApiObject: Codable {
    
    var id: Int?
    var city: City?
    var email: String?
    var name: String?
    var sex: String?
    var sexPreference: String?
    var currentRegion: Region?
    var avatarURL: String?
    var notificationsSettings: NotificationsSettings?
    
    //------------LOCAL-----------
    var originalImageName: String? = "avatar.jpg"
    var sexSelector: Sex? {
        get {
            return Sex(rawValue: sex ?? "")
        }
        set {
            self.sexSelector = newValue
            self.sex = newValue?.apiSex
        }
    }
    var sexPreferenceSelector: Sex? {
        get {
            return Sex(rawValue: sexPreference ?? "")
        }
        set {
            self.sexPreferenceSelector = newValue
            self.sexPreference = newValue?.apiSex
        }
    }
    var avatarData: String?
    var avatar: UIImage? {
        didSet {
            guard let image = avatar else {
                return
            }
            avatarData = image.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
        }
    }

    enum CodingKeys: String, CodingKey {
        case avatarURL = "avatarUrl"
        case city, email, id, name, sex, sexPreference, currentRegion, notificationsSettings
    }
    
    init(id: Int) {
        self.id = id
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init(id: 0)
        let container = try decoder.container(keyedBy: CodingKeys.self)

        
        if let id = try? container.decode(Int.self, forKey: .id) {
            self.id = id
        }
        
        if let city = try? container.decode(City.self, forKey: .city) {
            self.city = city
        }
        
        if let email = try? container.decode(String.self, forKey: .email) {
            self.email = email
        }
        if let name = try? container.decode(String.self, forKey: .name) {
            self.name = name
        }
        if let sex = try? container.decode(String.self, forKey: .sex) {
            self.sex = sex
        }
        if let sexPreference = try? container.decode(String.self, forKey: .sexPreference) {
            self.sexPreference = sexPreference
        }
        if let avatarURL = try? container.decode(String.self, forKey: .avatarURL) {
            self.avatarURL = avatarURL
        }
        if let currentRegion = try? container.decode(Region.self, forKey: .currentRegion) {
            self.currentRegion = currentRegion
        }
        
        if let notificationsSettings = try? container.decode(NotificationsSettings.self, forKey: .notificationsSettings) {
            self.notificationsSettings = notificationsSettings
        }
    }
    
    func toRealm() -> ClientRealm {
        let data = ClientRealm()
        
        data.id = id ?? 1
        data.email = email ?? ""
        data.city = city?.toRealm()
        data.name = name ?? ""
        data.sex = sex ?? ""
        data.currentRegion = currentRegion?.toRealm()
        
        data.sexPreference = sexPreference ?? ""
        data.avatarURL = avatarURL ?? ""
        
        return data
    }
    
    func toParametrs() -> [String: Any] {
        var parametrs = [String: Any]()
        parametrs["cityId"] = city?.id
        parametrs["image"] = avatarData
        parametrs["name"] = name
        parametrs["originalImageName"] = originalImageName
        parametrs["regionId"] = currentRegion?.id
        parametrs["sex"] = sexSelector?.apiSex
        parametrs["sexPreference"] = sexPreferenceSelector?.apiSex
        
        return parametrs
    }
//    func toProfile() -> Profile {
//        let profile = Profile()
//        profile.id = id
//        profile.email = email
//        profile.city = city
//        profile.name = name
//        profile.sex = Profile.Sex.init(rawValue: sex ?? "")
//        profile.region = currentRegion
//        profile.sexPreference = Profile.Sex.init(rawValue: sexPreference ?? "")
//        profile.avatarURL = avatarURL ?? ""
//        return profile
//    }
}

// MARK: - Notifications Settings
class NotificationsSettings: Codable {
    var likes: Bool?
    var messages: Bool?
    var system: Bool?
    
    enum CodingKeys: String, CodingKey {
        case likes, messages, system
    }
    
    convenience required init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let likes = try? container.decode(Bool.self, forKey: .likes) {
            self.likes = likes
        }
        if let messages = try? container.decode(Bool.self, forKey: .messages) {
            self.messages = messages
        }
        if let system = try? container.decode(Bool.self, forKey: .system) {
            self.system = system
        }
    }
    
    func toRealm() -> NotificationsSettingsRealm {
        let data = NotificationsSettingsRealm()
        data.likes = likes ?? true
        data.messages = messages ?? true
        data.system = system ?? true
        return data
    }
}

// MARK: - City
class City: Codable {
    var id: Int?
    var title: String?

    enum CodingKeys: String, CodingKey {
        case id, title
    }

    convenience required init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let id = try? container.decode(Int.self, forKey: .id) {
            self.id = id
        }
        if let title = try? container.decode(String.self, forKey: .title) {
            self.title = title
        }
    }
    
    func toRealm() -> CityInfoRealm {
        let data = CityInfoRealm()
        data.title = title ?? ""
        data.id = id ?? 1
        return data
    }
}

// MARK: - Region
class Region: Codable {
    var id: Int?
    var title: String?
    var location: Location?

    enum CodingKeys: String, CodingKey {
        case id, title, location
    }

    convenience required init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let id = try? container.decode(Int.self, forKey: .id) {
            self.id = id
        }
        if let title = try? container.decode(String.self, forKey: .title) {
            self.title = title
        }
        if let location = try? container.decode(Location.self, forKey: .location) {
            self.location = location
        }
    }
    
    func toRealm() -> RegionInfoRealm {
        let data = RegionInfoRealm()
        data.title = title ?? ""
        data.id = id ?? 1
        data.location = location?.toRealm()
        return data
    }
}

// MARK: - Location
class Location: Codable {
    var latitude: Double?
    var longitude: Double?

    enum CodingKeys: String, CodingKey {
        case latitude, longitude
    }

    convenience required init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let latitude = try? container.decode(Double.self, forKey: .latitude) {
            self.latitude = latitude
        }
        if let longitude = try? container.decode(Double.self, forKey: .longitude) {
            self.longitude = longitude
        }
    }
    
    func toRealm() -> LocationInfoRealm {
        let data = LocationInfoRealm()
        data.latitude = latitude ?? 0.0
        data.longitude = longitude ?? 0.0
        return data
    }
}

