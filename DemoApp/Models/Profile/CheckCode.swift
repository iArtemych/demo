//
//  CheckCode.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation

struct CheckCode: Codable {
    let refreshToken: String
    let token: String
}
