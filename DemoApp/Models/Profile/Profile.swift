//
//  Profile.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

//class Profile {
//    var id: Int?
//    var avatarURL: String?
//    var city: City?
//    var email: String?
//    var location: Location?
//    var name: String?
//    var originalImageName: String? = "avatar.jpg"
//    var sex: Sex?
//    var region: Region?
//    var sexPreference: Sex?
//    var avatar: UIImage? {
//        didSet {
//            guard let image = avatar else {
//                return
//            }
//            avatarURL = image.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
//        }
//    }
    
//    enum Sex: String {
//        case male = "boys"
//        case female = "girls"
//        case all = "all"
//
//        var sexString: String {
//            switch self {
//            case .male:
//                return "male"
//            case .female:
//                return "female"
//            case .all:
//                return "all"
//            }
//        }
//
//        var apiSex: String {
//            switch self {
//            case .male:
//                return "boys"
//            case .female:
//                return "girls"
//            case .all:
//                return "all"
//            }
//        }
//    }
//
//    func setupDefaultData() {
//        sexPreference = .female
//        region?.id = 39
//        region?.title = "Астраханская область"
//        location = Location()
//        location?.latitude = 46.347869000000003
//        location?.longitude = 48.033574000000002
//        region?.location = location
//        sex = .male
//    }
//    
//    func toParametrs() -> [String: Any] {
//        var parametrs = [String: Any]()
//        parametrs["cityId"] = city?.id
//        parametrs["image"] = avatarURL
//        parametrs["name"] = name
//        parametrs["originalImageName"] = originalImageName
//        parametrs["regionId"] = region?.id
//        parametrs["sex"] = sex!.apiSex
//        parametrs["sexPreference"] = sexPreference!.apiSex
//
//        return parametrs
//    }
//}

