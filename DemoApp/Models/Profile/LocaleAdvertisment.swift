//
//  LocaleAdvertisment.swift
//  
//
//  Created by Artem Chursin on 18.04.2021.
//

import Foundation
import UIKit

class LocaleAdvertisment {
    var image: UIImage? {
        didSet {
            guard let image = image else {
                return
            }
            imageData = image.jpegData(compressionQuality: 1)?.base64EncodedString() ?? ""
        }
    }
    var imageURL: String?
    var imageData: String?
    var text: String?
    var isHiden = false
    
}
