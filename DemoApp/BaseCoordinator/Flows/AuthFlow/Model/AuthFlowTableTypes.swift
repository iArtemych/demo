//
//  AuthFlowTableTypes.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

enum AuthTypeHeader {
    case none
}

struct AuthTableSection<Item> {
    let id: String
    let typeHeader: AuthTypeHeader
    let items: [Item]
}

struct AuthTypeItem {
    enum Kind {
        case titleWithRadio(id: Int, title: NSAttributedString, isSelect: Bool)
        case empty
        case none
    }
    
    let id: String
    let kind: Kind
}
