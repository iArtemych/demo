//
//  SexPreferViewModel.swift
//  
//
//  Created by Artem Chursin on 02.04.2021.
//

import UIKit
import RxSwift

class SexPreferViewModel {
    
    //MARK: - Private
    private let bag = DisposeBag()
    private var outEvent: ((SexPreferOutEvents) ->())?
    private var profile: ClientApiObject
    
    //MARK: - Binding
    var networkError: StringClosure?
    var isLoading: BoolClosure?
    var buttonEnable: BoolClosure?
    
    //MARK: - Init
    init(profile: ClientApiObject, outEvent: ((SexPreferOutEvents) ->())?) {
        self.outEvent = outEvent
        self.profile = profile
    }
}

// MARK: - Output
extension SexPreferViewModel {
    // MARK: View output
    func onViewEvent(_ event: SexPreferViewEvents) {
        switch event {
        case .viewDidLoad:
            break
        case .backAction:
            outEvent?(.backAction)
        case .sexSelect(let sex):
//            profile.sexPreferenceSelector = sex
            buttonEnable?(true)
        case .nextAction:
            outEvent?(.nextAction(profile))
        }
    }
}
