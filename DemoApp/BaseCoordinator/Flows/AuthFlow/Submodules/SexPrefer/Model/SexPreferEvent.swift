//
//  SexPrefervent.swift
//  
//
//  Created by Artem Chursin on 02.04.2021.
//

import Foundation

enum SexPreferOutEvents {
    case nextAction(ClientApiObject)
    case backAction
}

enum SexPreferViewEvents {
    case viewDidLoad
    case backAction
    case sexSelect(Sex)
    case nextAction
}
