//
//  SexPreferViewController.swift
//  
//
//  Created by Artem Chursin on 02.04.2021.
//

import UIKit
import SnapKit
import RxSwift

class SexPreferViewController: ViewController {
    
    let viewModel: SexPreferViewModel
    private var titleLabel: UILabel!
    
    private var nextButton: DefaultButton!
    private let bag = DisposeBag()
    
    let femaleView = UISexView()
    let maleView = UISexView()
    let allSexView = UISexView()
    
    // MARK: - Lifecycle
    init(viewModel: SexPreferViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        setupView()
        addNavBarImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar(animated: animated)
    }
}

//MARK: - Setup View
extension SexPreferViewController {
    
    private func setupView() {
        self.view.backgroundColor = Style.Colors.darkBackground
        
        titleLabel = UILabel()
        titleLabel.text = "Select Preferred Gender"
        titleLabel.font = Style.Font.titleSemiboldCustom(value: 24).font
        titleLabel.textAlignment = .center
        titleLabel.textColor = Style.Colors.dimWhite
        titleLabel.numberOfLines = 1
        self.view.addSubview(titleLabel)
        
        femaleView.setData(title: "Female"
                            .attributed
                            .color(Style.Colors.secondaryRed)
                            .font(Style.Font.titleMediumCustom(value: 16).font),
                         image: R.image.female())
        let femaleAction = UITapGestureRecognizer(target: self, action: #selector(femaleTap))
        femaleView.addGestureRecognizer(femaleAction)
        
        maleView.setData(title: "Male"
                            .attributed
                            .color(Style.Colors.secondaryRed)
                            .font(Style.Font.titleMediumCustom(value: 16).font),
                         image: R.image.male())
        let maleAction = UITapGestureRecognizer(target: self, action: #selector(maleTap))
        maleView.addGestureRecognizer(maleAction)
        
        allSexView.setData(title: "All"
                            .attributed
                            .color(Style.Colors.secondaryRed)
                            .font(Style.Font.titleMediumCustom(value: 16).font),
                         image: R.image.allSex())
        let allAction = UITapGestureRecognizer(target: self, action: #selector(allTap))
        allSexView.addGestureRecognizer(allAction)
        
        self.view.addSubview(maleView)
        self.view.addSubview(femaleView)
        self.view.addSubview(allSexView)
        
        let gradient = R.image.gradient()
        let gradientView = UIImageView(image: gradient)
        gradientView.contentMode = .scaleAspectFill
        self.view.addSubview(gradientView)
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(32)
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        
        nextButton = DefaultButton()
        nextButton.setTitle("Next", for: [])
        nextButton.style = .default
        nextButton.isEnabled = false
        nextButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.viewModel.onViewEvent(.nextAction)
                
            }).disposed(by: bag)
        self.view.addSubview(nextButton)
        
        gradientView.snp.makeConstraints {
            $0.bottom.left.right.equalToSuperview()
        }
        
        maleView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview().offset(100)
            $0.width.height.equalTo(150)
        }
        
        femaleView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview().offset(-100)
            $0.width.height.equalTo(150)
        }
        
        allSexView.snp.makeConstraints {
            $0.bottom.equalTo(maleView.snp.top).offset(-20)
            $0.centerX.equalToSuperview()
            $0.width.height.equalTo(150)
        }
        
        nextButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-50)
            $0.height.equalTo(56)
        }
    }
    
    private func setupNavigationBar(animated: Bool) {
        let backItem = ViewController.BarButtonItem(kind: .image(R.image.back()!)) { [weak self] (_) in
            self?.viewModel.onViewEvent(.backAction)
        }
        leftItems = [backItem]
    }
    
    private func setupBindings() {
        viewModel.networkError = { [weak self] message in
            let alert = CustomAlert(withNetworkWarning: message)
            self?.presentCustomAlert(alert)
        }
        
        viewModel.buttonEnable = { [weak self] isEnable in
            self?.nextButton.isEnabled = isEnable
        }
        
        viewModel.isLoading = { [weak self] isLoading in
            self?.view.presentActivityIndicator(isLoading)
        }
    }
}

//MARK: - Actions
extension SexPreferViewController {
    @objc
    private func femaleTap() {
        femaleView.sexImage.image = R.image.femaleE()
        maleView.sexImage.image = R.image.male()
        allSexView.sexImage.image = R.image.allSex()
        viewModel.onViewEvent(.sexSelect(.female))
    }
    
    @objc
    private func maleTap() {
        femaleView.sexImage.image = R.image.female()
        maleView.sexImage.image = R.image.maleE()
        allSexView.sexImage.image = R.image.allSex()
        viewModel.onViewEvent(.sexSelect(.male))
    }
    
    @objc
    private func allTap() {
        allSexView.sexImage.image = R.image.allSexE()
        maleView.sexImage.image = R.image.male()
        femaleView.sexImage.image = R.image.female()
        viewModel.onViewEvent(.sexSelect(.all))
    }
}
