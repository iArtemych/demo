//
//  AuthViewModel.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit
import RxSwift
import PhoneNumberKit

class AuthPhoneViewModel {
    
    //MARK: - Private
    private let bag = DisposeBag()
    private var outEvent: ((AuthOutEvents) ->())?
    
    //MARK: - Binding
    var networkError: StringClosure?
    var buttonEnable: BoolClosure?
    var isLoading: BoolClosure?
    var currentEmail: String?
    
    //MARK: - Init
    init(outEvent: ((AuthOutEvents) ->())?) {
        self.outEvent = outEvent
    }
}

// MARK: - Output
extension AuthPhoneViewModel {
    func onViewEvent(_ event: AuthViewEvents) {
        switch event {
        case .viewDidLoad:
            logout()
        case .getCode:
            getCode()
        case .textDidChange(let text):
            currentEmail = text
            revalidate()
        case .keyboardDone:
            if revalidate() {
                getCode()
            } else {
                networkError?("Please, enter valid e-mail")
            }
        }
    }
}

// MARK: - Private
extension AuthPhoneViewModel {
    @discardableResult
    private func revalidate() -> Bool {
        if let email = currentEmail,
           email.isValidEmail() {
            buttonEnable?(true)
            return true
        }
        buttonEnable?(false)
        return false
    }
}

// MARK: - API
private extension AuthPhoneViewModel {
    private func logout() {
        RequestManager.instance.authorizationInteractor?.logout()
    }
    
    private func getCode() {
        
        currentEmail = "tema320d@yandex.ru"
        currentEmail = "z"
        outEvent?(.nextStep(currentEmail ?? ""))
        
//        guard let email = currentEmail else {
//            return
//        }
//        isLoading?(true)
//        RequestManager.instance.authorizationInteractor?.getCode(email: email)
//            .subscribe(onNext: { [weak self] _ in
//                print("Get success")
//                self?.isLoading?(false)
//                self?.outEvent?(.nextStep(self?.currentEmail ?? ""))
//            }, onError: { [weak self] (error) in
//                print("Get error")
//                self?.networkError?(error.localizedDescription)
//                self?.isLoading?(false)
//            }, onCompleted: { [weak self] in
//                self?.isLoading?(false)
//            }, onDisposed: nil)
//            .disposed(by: bag)
//        currentEmail = "qqq"
//        outEvent?(.nextStep(currentEmail!))
    }
}
