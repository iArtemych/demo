//
//  AuthViewController.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit
import SnapKit
import RxSwift
import Photos

class AuthViewController: ViewController {
    
    let viewModel: AuthPhoneViewModel
    private var titleLabel: UILabel!
    private var emailView: UITextInput!
    private var subtitleLabel: UILabel!
    
    private var nextButton: DefaultButton!
    private let bag = DisposeBag()
    var allPhotos: PHFetchResult<PHAsset>? = nil
    
    // MARK: - Lifecycle
    init(viewModel: AuthPhoneViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        viewModel.onViewEvent(.viewDidLoad)
        setupView()
        emailView.textField.becomeFirstResponder()
        addNavBarImage()
        
        let fetchOptions = PHFetchOptions()
        self.allPhotos = PHAsset.fetchAssets(with: .image, options: fetchOptions)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerKeyboardNotifications()
    }
}

//MARK: - Setup View
extension AuthViewController {
    
    private func setupView() {
        self.view.backgroundColor = Style.Colors.darkBackground
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        titleLabel = UILabel()
        titleLabel.text = "Sign In"
        titleLabel.font = Style.Font.titleSemiboldCustom(value: 24).font
        titleLabel.textAlignment = .left
        titleLabel.textColor = Style.Colors.dimWhite
        titleLabel.numberOfLines = 1
        self.view.addSubview(titleLabel)
        
        subtitleLabel = UILabel()
        subtitleLabel.text = "Please, enter your email for sign in. We will send authorization code."
        subtitleLabel.font = Style.Font.titleLightCustom(value: 16).font
        subtitleLabel.textAlignment = .left
        subtitleLabel.textColor = Style.Colors.textWhite
        subtitleLabel.numberOfLines = 0
        self.view.addSubview(subtitleLabel)
        
        emailView = UITextInput()
        emailView.setData(title: nil,
                          placeholder: "E-mail"
                            .attributed
                            .color(Style.Colors.placeholderWhite)
                            .font(Style.Font.titleLightCustom(value: 16).font),
                          contentType: .emailAddress) { [weak self] (email) in
            self?.viewModel.onViewEvent(.textDidChange(email))
        } doneAction: { [weak self] in
            self?.viewModel.onViewEvent(.keyboardDone)
        }
        self.view.addSubview(emailView)
        
        let gradient = R.image.gradient()
        let gradientView = UIImageView(image: gradient)
        gradientView.contentMode = .scaleAspectFill
        self.view.addSubview(gradientView)
        
        nextButton = DefaultButton()
        nextButton.setTitle("Next", for: [])
        nextButton.style = .default
        nextButton.isEnabled = false
        nextButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.viewModel.onViewEvent(.getCode)
                
            }).disposed(by: bag)
        self.view.addSubview(nextButton)
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(32)
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(8)
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        
        emailView.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(26)
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.height.equalTo(55)
        }
        
        gradientView.snp.makeConstraints {
            $0.bottom.left.right.equalToSuperview()
        }
        
        nextButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-50)
            $0.height.equalTo(56)
        }
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    private func setupBindings() {
        viewModel.networkError = { [weak self] message in
            self?.showAlert(type: .error,
                      titleText: "Network error",
                      subtitle: message)
        }
        
        viewModel.isLoading = { [weak self] isLoading in
            self?.configureLoader(isLoading: isLoading, loader: self?.loaderView)
        }
        
        viewModel.buttonEnable = { [weak self] isEnable in
            self?.nextButton.isEnabled = isEnable
        }
    }
}

// MARK: - UIKeyBoard
extension AuthViewController {

    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            
            self.keyboardSize = keyboardSize.height
            print(keyboardSize.height)
        }
        
        nextButton.snp.remakeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-15 - (keyboardSize ?? 500))
            $0.height.equalTo(56)
        }
    }

    @objc
    func keyboardWillHide(notification: NSNotification) {
        nextButton.snp.remakeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-50)
            $0.height.equalTo(56)
        }
    }
}
