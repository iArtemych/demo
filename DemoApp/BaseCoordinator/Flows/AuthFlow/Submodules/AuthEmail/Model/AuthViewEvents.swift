//
//  AuthViewEvents.swift
//  
//
//  Created by Artem Chursin on 28.03.2021.
//

import Foundation

enum AuthViewEvents {
    case viewDidLoad
    case getCode
    case textDidChange(String?)
    case keyboardDone
}

enum AuthOutEvents {
    case nextStep(String)
}
