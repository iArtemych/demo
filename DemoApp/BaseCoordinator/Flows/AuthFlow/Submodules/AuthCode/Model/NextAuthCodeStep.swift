//
//  NextAuthCodeStep.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation

enum AuthCodeOutEvent {
    case showMainScreen
    case showRegistration(ClientApiObject)
    case backAction
}
