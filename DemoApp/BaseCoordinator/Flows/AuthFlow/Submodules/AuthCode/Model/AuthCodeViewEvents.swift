//
//  AuthCodeViewEvents.swift

//
//  Created by Artem Chursin on 31.03.2021.
//

import Foundation

enum AuthCodeViewEvents {
    case viewDidLoad
    case resetTimer
    case textDidChange(String?)
    case backAction
    case resend
}
