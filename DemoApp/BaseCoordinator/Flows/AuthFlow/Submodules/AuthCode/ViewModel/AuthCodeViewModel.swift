//
//  AuthCodeViewModel.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit
import RxSwift
import PINRemoteImage

class AuthCodeViewModel {
    
    enum ScreenState {
        case getCode, waiting, complited, error
    }
    
    //MARK: - Public
    let email: String
    var code: String?
    var profile = ClientApiObject(id: -1)
    
    //MARK: - Binding
    var updateUI: ((ScreenState)->())?
    var updateTimer: ((NSMutableAttributedString)->())?
    var isLoading: BoolClosure?
    var goToNextStep: ((AuthCodeOutEvent)->())?
    var showError: StringClosure?
    var networkError: StringClosure?
    var presentResendButton: BoolClosure?
    var clearCode: VoidClosure?
    
    //MARK: - Private
    private var isCodeCorrect = false
    private var timer: Timer!
    private var waitingTime = 60
    private var screenState = ScreenState.waiting
    private var authDataRepository: AuthDataRepositoryProtocol
    private let bag = DisposeBag()
    var backAction: VoidClosure?
    
    init(email: String,
         authDataRepository: AuthDataRepositoryProtocol,
         backAction: VoidClosure?,
         finish: ((AuthCodeOutEvent)->())?) {
        
        self.backAction = backAction
        self.goToNextStep = finish
        self.email = email
        self.authDataRepository = authDataRepository
    }
}

// MARK: - Private
private extension AuthCodeViewModel {
    
    private func viewDidLoad() {
        setupTimer()
    }
    
    private func resetTimer() {
        getCode()
        timerText(timerValue: "60")
        screenState = .waiting
        updateUI?(screenState)
        waitingTime = 60
        setupTimer()
    }
    
    private func codeInputChanged() {
        guard let string = code else {
            return
        }
        if string.count == 4 {
            checkCode(code: string)
        } else {
            presentResendButton?(false)
            if waitingTime == 0 {
                updateUI?(.getCode)
            } else {
                updateUI?(.waiting)
            }
        }
    }
}

// MARK: - Output
extension AuthCodeViewModel {
    func onViewEvent(_ event: AuthCodeViewEvents) {
        switch event {
        case .viewDidLoad:
            viewDidLoad()
        case .resetTimer:
            resetTimer()
        case .textDidChange(let text):
            code = text
            codeInputChanged()
        case .backAction:
            goToNextStep?(.backAction)
        case .resend:
            codeInputChanged()
        }
    }
}

// MARK: - API
extension AuthCodeViewModel {
    
    private func getCode() {
        RequestManager.instance.authorizationInteractor?.getCode(email: email)
            .subscribe(onNext: { _ in
            }, onError: { [weak self] (error) in
                self?.networkError?(error.localizedDescription)
            }, onCompleted: nil, onDisposed: nil)
            .disposed(by: bag)
    }
    
    private func checkCode(code: String) {
        goToNextStep?(.showRegistration(self.profile))
//        isLoading?(true)
//        RequestManager.instance.authorizationInteractor?.checkCode(code: code, email: email)
//            .subscribe(onNext: { [weak self] (codeObject) in
//                print("Confirm success")
//                let authRealm = AuthDataRealm()
//                authRealm.accessToken = codeObject.token
//                authRealm.refreshToken = codeObject.refreshToken
//                PINRemoteImageManager.shared().setValue("Bearer \(codeObject.token)", forHTTPHeaderField: "Authorization")
//                self?.authDataRepository.save(authData: authRealm)
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                    self?.checkProfile()
//                }
//            }, onError: { [weak self] (error) in
//                self?.updateUI?(.error)
//                //TODO: - Remake with code
//                if error.localizedDescription == "Wrong email or password" {
//                    self?.clearCode?()
//                } else {
//                    self?.presentResendButton?(true)
//                }
//                print("Confirm error")
//                self?.isLoading?(false)
//                self?.networkError?(error.localizedDescription)
//            }, onCompleted: nil, onDisposed: nil)
//            .disposed(by: bag)
    }
    
    private func checkProfile() {
        RequestManager.instance.authorizationInteractor?.checkProfile()
            .subscribe(onNext: { [weak self] (codeObject) in
                self?.isLoading?(false)
                print("Profile success")
                guard let self = self else {
                    return
                }
                if codeObject.sex == nil {
                    self.profile.email = self.email
                    self.goToNextStep?(.showRegistration(self.profile))
                } else {
                    self.goToNextStep?(.showMainScreen)
                }
            }, onError: { [weak self] (error) in
                self?.networkError?(error.localizedDescription)
                print("Profile error")
                self?.isLoading?(false)
            }, onCompleted: { [weak self] in
                self?.isLoading?(false)
            }, onDisposed: nil)
            .disposed(by: bag)
    }
}

//MARK: - Timer
extension AuthCodeViewModel {
    func timerText(timerValue: String) {
        let textAttributes = [NSAttributedString.Key.font : Style.Font.titleLightCustom(value: 14).font,
                              NSAttributedString.Key.foregroundColor : Style.Colors.textWhite]
        
        
        
        let attributedString1 = NSMutableAttributedString(string: "Request new code in \n", attributes: textAttributes)
        
        let attributedString2 = NSMutableAttributedString(string: timerValue, attributes: textAttributes)
        
        let attributedString3 = NSMutableAttributedString(string:" sec", attributes: textAttributes)
        
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        
        updateTimer?(attributedString1)
    }
    
    private func setupTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] timer in
            guard let self = self else {
                return
            }
            self.waitingTime -= 1
            self.timerText(timerValue: String(self.waitingTime))
            
            if self.waitingTime == 0 {
                self.screenState = .getCode
                self.updateUI?(self.screenState)
                timer.invalidate()
            }
        }
    }
}
