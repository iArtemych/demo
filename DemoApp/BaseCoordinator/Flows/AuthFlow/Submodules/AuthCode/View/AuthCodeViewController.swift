//
//  AuthCodeViewController.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit
import SnapKit
import RxSwift
import APNGKit

class AuthCodeViewController: ViewController {
    
    let viewModel: AuthCodeViewModel
    private var titleLabel: UILabel!
    private var subtitleLabel: UILabel!
    
    private var inputCodeView: UICodeInput!
    private var timerLabel: UILabel!
    private var descriptionLabel: UILabel!
    private var nextButton: DefaultButton!
    private var getCodeButton: UIButton!
    
    private let bag = DisposeBag()
    
    // MARK: - Lifecycle
    init(viewModel: AuthCodeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        setupView()
        addNavBarImage()
        viewModel.onViewEvent(.viewDidLoad)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar(animated: animated)
        registerKeyboardNotifications()
    }
}

//MARK: - Setup View
extension AuthCodeViewController {
    
    private func setupView() {
        self.view.backgroundColor = Style.Colors.darkBackground
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        titleLabel = UILabel()
        titleLabel.text = "Enter Code"
        titleLabel.font = Style.Font.titleSemiboldCustom(value: 24).font
        titleLabel.textAlignment = .left
        titleLabel.textColor = Style.Colors.dimWhite
        titleLabel.numberOfLines = 1
        self.view.addSubview(titleLabel)
        
        subtitleLabel = UILabel()
        subtitleLabel.text = "We’ve sent code to \(viewModel.email)."
        subtitleLabel.font = Style.Font.titleLightCustom(value: 16).font
        subtitleLabel.textAlignment = .left
        subtitleLabel.textColor = Style.Colors.textWhite
        subtitleLabel.numberOfLines = 0
        self.view.addSubview(subtitleLabel)
        
        inputCodeView = UICodeInput()
        inputCodeView.setData {[weak self] (code) in
            self?.viewModel.onViewEvent(.textDidChange(code))
        }
        let codeTap = UITapGestureRecognizer(target: self, action: #selector(codeAction))
        inputCodeView.addGestureRecognizer(codeTap)
        self.view.addSubview(inputCodeView)
        
        timerLabel = UILabel(with: "Request new code in \n 60 sec",
                             font: Style.Font.titleLightCustom(value: 14),
                             color: Style.Colors.textWhite)
        self.view.addSubview(timerLabel)
        
        getCodeButton = UIButton(type: .system)
        getCodeButton.tintColor = Style.Colors.primaryRed
        getCodeButton.isHidden = true
        getCodeButton.setTitle("Request Code", for: [])
        getCodeButton.rx.tap
            .subscribe { [weak self] (_) in
                self?.viewModel.onViewEvent(.resetTimer)
            }.disposed(by: bag)

        self.view.addSubview(getCodeButton)
        
        let gradient = R.image.gradient()
        let gradientView = UIImageView(image: gradient)
        gradientView.contentMode = .scaleAspectFill
        self.view.addSubview(gradientView)
        
        nextButton = DefaultButton()
        nextButton.setTitle("Sign In", for: [])
        nextButton.style = .default
        nextButton.isEnabled = true
        nextButton.isHidden = true
        nextButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.viewModel.onViewEvent(.resend)
                
            }).disposed(by: bag)
        self.view.addSubview(nextButton)
        
        let policyLinkLabel = UILabelTapableLinks()
        policyLinkLabel.textAlignment = .center
        let textAttributes = [NSAttributedString.Key.font : Style.Font.titleLightCustom(value: 12).font,
                              NSAttributedString.Key.foregroundColor : Style.Colors.placeholderWhite]
        
        
        let linkAttributes = [NSAttributedString.Key.font: Style.Font.titleLightCustom(value: 12).font,
                              NSAttributedString.Key.foregroundColor: Style.Colors.primaryRed,
                                    NSAttributedString.Key.underlineColor: Style.Colors.primaryRed,
                                    NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
                                    NSAttributedString.Key.attachment: URL(string: "http://google.com")!] as [NSAttributedString.Key : Any]
        
        let secondLinkAttributes = [NSAttributedString.Key.font: Style.Font.titleLightCustom(value: 12).font,
                              NSAttributedString.Key.foregroundColor: Style.Colors.primaryRed,
                                    NSAttributedString.Key.underlineColor: Style.Colors.primaryRed,
                                    NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
                                    NSAttributedString.Key.attachment: URL(string: "http://yandex.ru")!] as [NSAttributedString.Key : Any]
        
        let plainAttributedString = NSMutableAttributedString(string: "Продолжая ты соглашаешься с ",
            attributes: textAttributes)
        let string = "условиями использования сервиса "
        let andAttributedString = NSMutableAttributedString(string: "и", attributes: textAttributes)
        let secondString = " политикой конфиденциальности"
        
        let attributedLinkString = NSMutableAttributedString(string: string, attributes: linkAttributes)
        let secondAttributedLinkString = NSMutableAttributedString(string: secondString, attributes: secondLinkAttributes)
        
        let fullAttributedString = NSMutableAttributedString()
        fullAttributedString.append(plainAttributedString)
        fullAttributedString.append(attributedLinkString)
        fullAttributedString.append(andAttributedString)
        fullAttributedString.append(secondAttributedLinkString)
        policyLinkLabel.isUserInteractionEnabled = true
        policyLinkLabel.attributedText = fullAttributedString
        policyLinkLabel.numberOfLines = 0
        policyLinkLabel.didTap = { urlString in
            guard let urlFromString = URL(string: urlString) else {
                return
            }
            UIApplication.shared.open(urlFromString, options: [:], completionHandler: nil)
        }
        self.view.addSubview(policyLinkLabel)
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(32)
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(8)
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        
        inputCodeView.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(32)
            $0.height.equalTo(80)
            $0.centerX.equalToSuperview()
        }
        
        timerLabel.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(inputCodeView.snp.bottom).offset(32)
            $0.left.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-16)
        }
        
        getCodeButton.snp.makeConstraints {
            $0.centerX.equalTo(view.snp.centerX)
            $0.top.equalTo(inputCodeView.snp.bottom).offset(48)
            $0.left.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-16)
            
        }
        
        gradientView.snp.makeConstraints {
            $0.bottom.left.right.equalToSuperview()
        }
        
        policyLinkLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-114)
        }
        
        nextButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-50)
            $0.height.equalTo(56)
        }
    }
    
    private func setupNavigationBar(animated: Bool) {
        let backItem = ViewController.BarButtonItem(kind: .image(R.image.back()!)) { [weak self] (_) in
            self?.viewModel.backAction?()
        }
        leftItems = [backItem]
    }
    
    private func setupBindings() {
        viewModel.updateUI = { [weak self] state in
            switch state {
            case .getCode:
                self?.timerLabel.isHidden = true
                self?.getCodeButton.isHidden = false
                self?.inputCodeView.textField.textColor = Style.Colors.textWhite
            case .waiting:
                self?.timerLabel.isHidden = false
                self?.getCodeButton.isHidden = true
                self?.inputCodeView.textField.textColor = Style.Colors.textWhite
            case .complited:
                break
            case .error:
                self?.inputCodeView.textField.textColor = Style.Colors.textWhite
                let generator = UINotificationFeedbackGenerator()
                generator.notificationOccurred(.error)
                break
            }
        }
        
        viewModel.networkError = { [weak self] message in
            self?.showAlert(type: .error,
                      titleText: "Network error",
                      subtitle: message)
        }
        
        viewModel.updateTimer = { [weak self] str in
            self?.timerLabel.attributedText = str
        }
        
        viewModel.isLoading = { [weak self] isLoading in
            self?.configureLoader(isLoading: isLoading, loader: self?.loaderView)
        }
        
        viewModel.presentResendButton = { [weak self] sholdShow in
            self?.nextButton.isHidden = !sholdShow
        }
        
        viewModel.clearCode = { [weak self] in
            self?.inputCodeView.clearField()
        }
    }
}

//MARK: - Actions
extension AuthCodeViewController {
    @objc
    private func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc
    private func codeAction() {
        inputCodeView.textField.becomeFirstResponder()
    }
}

// MARK: - UIKeyBoard
extension AuthCodeViewController {

    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            
            self.keyboardSize = keyboardSize.height
            print(keyboardSize.height)
        }
        
        nextButton.snp.remakeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-15 - (keyboardSize ?? 500))
            $0.height.equalTo(56)
        }
    }

    @objc
    func keyboardWillHide(notification: NSNotification) {
        nextButton.snp.remakeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-50)
            $0.height.equalTo(56)
        }
    }
}
