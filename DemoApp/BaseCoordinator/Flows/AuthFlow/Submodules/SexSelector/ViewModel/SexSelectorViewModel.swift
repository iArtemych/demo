//
//  SexSelectorViewModel.swift
//  
//
//  Created by Artem Chursin on 30.03.2021.
//

import UIKit
import RxSwift

class SexSelectorViewModel {
    
    //MARK: - Private
    private let bag = DisposeBag()
    private var outEvent: ((SexOutEvents) ->())?
    private var profile: ClientApiObject
    
    //MARK: - Binding
    var networkError: StringClosure?
    var isLoading: BoolClosure?
    var buttonEnable: BoolClosure?
    
    //MARK: - Init
    init(profile: ClientApiObject, outEvent: ((SexOutEvents) ->())?) {
        self.outEvent = outEvent
        self.profile = profile
    }
}

// MARK: - Output
extension SexSelectorViewModel {
    func onViewEvent(_ event: SexViewEvents) {
        switch event {
        case .viewDidLoad:
            break
        case .sexSelect(let sex):
//            profile.sexSelector = sex
            buttonEnable?(true)
        case .nextAction:
            outEvent?(.nextAction(profile))
        case .backAction:
            outEvent?(.backAction)
        }
    }
}
