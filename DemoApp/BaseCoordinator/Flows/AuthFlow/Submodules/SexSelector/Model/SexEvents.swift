//
//  SexOutEvents.swift
//  
//
//  Created by Artem Chursin on 30.03.2021.
//

import Foundation

enum SexOutEvents {
    case nextAction(ClientApiObject)
    case backAction
}

enum SexViewEvents {
    case viewDidLoad
    case sexSelect(Sex)
    case nextAction
    case backAction
}
