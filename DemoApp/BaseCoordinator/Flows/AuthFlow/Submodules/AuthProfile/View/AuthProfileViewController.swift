//
//  AuthProfileViewController.swift
//  
//
//  Created by Artem Chursin on 02.04.2021.
//

import UIKit
import SnapKit
import RxSwift
import ConvenientImagePicker

class AuthProfileViewController: ViewController {
    
    let viewModel: AuthProfileViewModel
    private var titleLabel: UILabel!
    private var subtitleLabel: UILabel!
    private var nameView: UITextInput!
    
    private var nextButton: DefaultButton!
    private let bag = DisposeBag()
    
    let photoView = UIPhotoView()
    
    // MARK: - Lifecycle
    init(viewModel: AuthProfileViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.onViewEvent(.viewDidLoad)
        setupBindings()
        setupView()
        registerKeyboardNotifications()
        addNavBarImage()
//        presentCustomPhotoModal() { [weak self] event in
//            switch event {
//            case .camera:
//                self?.presentPhotoPicker()
//            case .library:
//                print("VC library")
//            }
//        }
//        presentPhotoPicker()
    }
    
    func presentPhotoPicker() {
        let pickerViewController = PickerViewController()
        pickerViewController.allowMultipleSelection = false
        pickerViewController.numberOfPictureInRow = 3
        pickerViewController.isSimpleMode = true
        pickerViewController.isDarkMode = true
        pickerViewController.isAnimated = true
        pickerViewController.delegate = self
        pickerViewController.isSupportLandscape = false // A Boolean value that determines whether the ability of landscape exists.
        self.present(pickerViewController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar(animated: animated)
    }
}

extension AuthProfileViewController: ConvenientImagePickerDelegate {
    func imagePickerDidCancel(_ selectedImages: [Int : UIImage]) {

    }
    
    func imageDidSelect(_ imagePicker: PickerViewController, index: Int, image: UIImage?) {
        self.viewModel.onViewEvent(.setPhoto(image))
        self.photoView.imagePhoto.image = image
    }
    
    func imageDidDeselect(_ imagePicker: PickerViewController, index: Int, image: UIImage?) {
        
    }
    
    func imageSelectMax(_ imagePicker: PickerViewController, wantToSelectIndex: Int, wantToSelectImage: UIImage?) {
        
    }
}

//MARK: - Setup View
extension AuthProfileViewController {
    
    private func setupView() {
        self.view.backgroundColor = Style.Colors.darkBackground
        
        let tap = UITapGestureRecognizer(target: self,
                                         action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        titleLabel = UILabel()
        titleLabel.text = "Fill Your Profile"
        titleLabel.font = Style.Font.titleSemiboldCustom(value: 24).font
        titleLabel.textAlignment = .left
        titleLabel.textColor = Style.Colors.dimWhite
        titleLabel.numberOfLines = 1
        self.view.addSubview(titleLabel)
        
        subtitleLabel = UILabel()
        subtitleLabel.text = "No one will see your data until you allow it"
        subtitleLabel.font = Style.Font.titleLightCustom(value: 16).font
        subtitleLabel.textAlignment = .left
        subtitleLabel.textColor = Style.Colors.textWhite
        subtitleLabel.numberOfLines = 0
        self.view.addSubview(subtitleLabel)
        
        photoView.setData(title: "Edit Photo"
                            .attributed
                            .color(Style.Colors.secondaryRed)
                            .font(Style.Font.titleLightCustom(value: 14).font),
                          image: R.image.smile())
        let photoTap = UITapGestureRecognizer(target: self, action: #selector(editTap))
        photoView.addGestureRecognizer(photoTap)
        self.view.addSubview(photoView)
        
        nameView = UITextInput()
        nameView.setData(title: nil,
                         placeholder: "Nickname"
                            .attributed
                            .color(Style.Colors.placeholderWhite)
                            .font(Style.Font.titleLightCustom(value: 16).font),
                         contentType: .name) { [weak self] (name) in
            self?.viewModel.onViewEvent(.setName(name))
        } doneAction: { [weak self] in
            self?.viewModel.onViewEvent(.keyboardDone)
        }
        self.view.addSubview(nameView)
        
        let gradient = R.image.gradient()
        let gradientView = UIImageView(image: gradient)
        gradientView.contentMode = .scaleAspectFill
        self.view.addSubview(gradientView)
        
        nextButton = DefaultButton()
        nextButton.setTitle("Next", for: [])
        nextButton.style = .default
        nextButton.isEnabled = false
        nextButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.viewModel.onViewEvent(.saveAction)
                
            }).disposed(by: bag)
        self.view.addSubview(nextButton)
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(32)
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(8)
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
        }
        
        
        gradientView.snp.makeConstraints {
            $0.bottom.left.right.equalToSuperview()
        }
        
        photoView.snp.makeConstraints {
            $0.width.equalTo(80)
            $0.height.equalTo(110)
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(32)
            $0.left.equalToSuperview().offset(24)
        }
        
        nameView.snp.makeConstraints {
            $0.top.equalTo(subtitleLabel.snp.bottom).offset(48)
            $0.left.equalTo(photoView.snp.right).offset(16)
            $0.right.equalToSuperview().offset(-24)
            $0.height.equalTo(55)
        }
        
        nextButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-50)
            $0.height.equalTo(56)
        }
    }
    
    private func setupNavigationBar(animated: Bool) {
        let backItem = ViewController.BarButtonItem(kind: .image(R.image.back()!)) { [weak self] (_) in
            self?.viewModel.onViewEvent(.backAction)
        }
        leftItems = [backItem]
    }
    
    private func setupBindings() {
        viewModel.networkError = { [weak self] message in
            self?.showAlert(type: .error,
                      titleText: "Network error",
                      subtitle: message)
        }
        
        viewModel.isLoading = { [weak self] isLoading in
            self?.configureLoader(isLoading: isLoading, loader: self?.loaderView)
        }
        
        viewModel.buttonEnable = { [weak self] isEnable in
            self?.nextButton.isEnabled = isEnable
        }
    }
}

// MARK: - ImagePickerDelegate
extension AuthProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        viewModel.onViewEvent(.setPhoto(image))
        photoView.imagePhoto.image = image
    }
}

// MARK: - Actions
extension AuthProfileViewController {
    @objc
    private func editTap() {
//        self.imagePicker.present(from: self.view)
        presentCustomPhotoModal() { [weak self] event in
            self?.dismiss(animated: true, completion: nil)
            switch event {
            case .camera:
                let vc = UIImagePickerController()
                vc.sourceType = .camera
                vc.allowsEditing = true
                vc.delegate = self
                self?.present(vc, animated: true)
            case .library:
                self?.presentPhotoPicker()
            }
        }
    }
    
    @objc
    private func dismissKeyboard() {
        self.view.endEditing(true)
    }
}

// MARK: - UIKeyBoard
extension AuthProfileViewController {

    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @objc
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            
            self.keyboardSize = keyboardSize.height
            print(keyboardSize.height)
        }
        
        nextButton.snp.remakeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-15 - (keyboardSize ?? 500))
            $0.height.equalTo(56)
        }
    }

    @objc
    func keyboardWillHide(notification: NSNotification) {
        nextButton.snp.remakeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-50)
            $0.height.equalTo(56)
        }
    }
}
