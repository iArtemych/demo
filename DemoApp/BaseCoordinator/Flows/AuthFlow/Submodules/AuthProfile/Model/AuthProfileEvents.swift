//
//  AuthProfileEvents.swift
//  
//
//  Created by Artem Chursin on 02.04.2021.
//

import UIKit

enum AuthProfileOutEvents {
    case nextAction
    case backAction
}

enum AuthProfileViewEvents {
    case viewDidLoad
    case backAction
    case setPhoto(UIImage?)
    case setName(String?)
    case saveAction
    case keyboardDone
}
