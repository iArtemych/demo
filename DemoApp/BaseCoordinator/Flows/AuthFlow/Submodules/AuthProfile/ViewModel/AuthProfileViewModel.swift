//
//  AuthProfileViewModel.swift

//
//  Created by Artem Chursin on 02.04.2021.
//

import UIKit
import RxSwift

class AuthProfileViewModel {
    
    //MARK: - Private
    private let bag = DisposeBag()
    private var outEvent: ((AuthProfileOutEvents) ->())?
    private var profile: ClientApiObject
    
    //MARK: - Binding
    var networkError: StringClosure?
    var isLoading: BoolClosure?
    var buttonEnable: BoolClosure?
    
    //MARK: - Init
    init(profile: ClientApiObject, outEvent: ((AuthProfileOutEvents)->())?){
        self.outEvent = outEvent
        self.profile = profile
    }
}

// MARK: - Output
extension AuthProfileViewModel {
    func onViewEvent(_ event: AuthProfileViewEvents) {
        switch event {
        case .viewDidLoad:
            //DEBUG
//            profile.setupDefaultData()
            break
        case .backAction:
            outEvent?(.backAction)
        case .setPhoto(let image):
            profile.avatar = image
            revalidate()
        case .setName(let name):
            profile.name = name
            revalidate()
        case .saveAction:
            uploadProfile()
        case .keyboardDone:
            break
        }
    }
}

// MARK: - Private
private extension AuthProfileViewModel {
    private func revalidate() {
        if profile.avatar != nil,
           let name = profile.name,
           name.count > 1 {
            buttonEnable?(true)
        } else {
            buttonEnable?(false)
        }
    }
}

// MARK: - API
private extension AuthProfileViewModel {
    private func uploadProfile() {
        outEvent?(.nextAction)
//        RequestManager.instance.authorizationInteractor?.uploadProfile(profile: profile)
//            .subscribe(onNext: { [weak self] _ in
//                self?.outEvent?(.nextAction)
//            }, onError: { [weak self] (error) in
//                self?.networkError?(error.localizedDescription)
//            }, onCompleted: nil, onDisposed: nil)
//            .disposed(by: bag)

    }
}

