//
//  CitySelectViewModel.swift
//  
//
//  Created by Artem Chursin on 03.04.2021.
//

import UIKit
import RxSwift
import MapKit

class RegionSelectViewModel {
    
    //MARK: - Public
    var filteredArray = [Region]()
    
    //MARK: - Binding
    var isLoading: BoolClosure?
    var networkError: StringClosure?
    var buttonEnable: BoolClosure?
    var bindData: (([AuthTableSection<AuthTypeItem>]) -> Void)?
    var data: [AuthTableSection<AuthTypeItem>] = []
    var outEvent: ((RegionOutEvents) ->())?
    var showEmpty: BoolClosure?
    
    //MARK: - Private
    private let bag = DisposeBag()
    let profile: ClientApiObject
    private var regionList = [Region]()
    private var locManager = CLLocationManager()
    private var currentLocation: CLLocation!

    // MARK: - Lifecycle
    init(profile: ClientApiObject,
         outEvent: ((RegionOutEvents) ->())?) {
        self.profile = profile
        self.outEvent = outEvent
    }
}

// MARK: - API
private extension RegionSelectViewModel {
    private func viewDidLoad() {
        outEvent?(.nextAction(profile))
//        getRegionList()
        
//        locManager.requestWhenInUseAuthorization()
    }
    
    private func touchIndex(indexPath: IndexPath) {
        let item = data[indexPath.section].items[indexPath.row]
        switch item.kind {
        case .titleWithRadio:
            guard let regionId = Int(item.id) else {
                return
            }
            buttonEnable?(true)
            profile.currentRegion = regionList.first(where: {$0.id ?? 0 == regionId})
            reloadData()
        default:
            break
        }
    }
    
    private func regionFiltering(_ text: String?) {
        filteredArray = regionList
        if let fieldText = text,
           !fieldText.isEmpty {
            filteredArray = regionList.filter({ ($0.title?.lowercased().contains(fieldText.lowercased()) ?? false) })
        } else {
            filteredArray = regionList
        }
        showEmpty?(filteredArray.isEmpty)
        reloadData()
    }
    
    private func reloadData() {
        data = generateSections()
        bindData?(data)
    }
}

// MARK: - Output
extension RegionSelectViewModel {
    
    func onViewEvent(_ event: RegionViewEvents) {
        switch event {
        case .viewDidLoad:
            viewDidLoad()
        case .cellTouch(let indexPath):
            touchIndex(indexPath: indexPath)
        case .nextAction:
            outEvent?(.nextAction(profile))
        case .filter(let text):
            regionFiltering(text)
        case .reloadData:
            reloadData()
        }
    }
}

// MARK: - API
extension RegionSelectViewModel {
    private func getRegionList() {
        RequestManager.instance.authorizationInteractor?.getRegionList()
            .subscribe(onNext: { [weak self] regions in
                self?.regionList = regions
                
                self?.regionFiltering(nil)
                self?.reloadData()
            }, onError: { [weak self] (error) in
                self?.networkError?(error.localizedDescription)
            }, onCompleted: nil, onDisposed: nil)
            .disposed(by: bag)
    }
}


