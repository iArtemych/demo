//
//  CitySelectViewController.swift
//  
//
//  Created by Artem Chursin on 03.04.2021.
//

import UIKit
import SnapKit
import RxSwift

class RegionSelectViewController: ViewController {
    
    //MARK: - Private
    private var sections = [AuthTableSection<AuthTypeItem>]()
    private let viewModel: RegionSelectViewModel
    private var tableView: UITableView!
    private var emptyLabel = UILabel()
    // Container view to create rounded corners
    private var tableViewContainer: UIView!
    private var searchBar: UISearchBar!
    private let bag = DisposeBag()
    private var nextButton: DefaultButton!
    
    // MARK: - Lifecycle
    init(viewModel: RegionSelectViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
        setupView()
        addNavBarImage()
        registerKeyboardNotifications()
        viewModel.onViewEvent(.viewDidLoad)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.onViewEvent(.filter(nil))
        viewModel.onViewEvent(.reloadData)
    }
}

//MARK: - Setup View
extension RegionSelectViewController {
    private func setupView() {
        self.view.backgroundColor = Style.Colors.darkBackground
        
        backActionEnable = false
        searchBar = UISearchBar()
        searchBar.placeholder = "Search"
        searchBar.barTintColor = Style.Colors.darkBackground
        searchBar.keyboardAppearance = .dark
        searchBar.delegate = self
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.backgroundColor = Style.Colors.darkBackground
            searchBar.searchTextField.layer.borderColor = Style.Colors.primaryRed.cgColor
            searchBar.searchTextField.layer.borderWidth = 2
            searchBar.searchTextField.layer.cornerRadius = 16
            searchBar.searchTextField.font = Style.Font.titleLightCustom(value: 16).font
            searchBar.searchTextField.textColor = Style.Colors.textWhite
        }
        view.addSubview(searchBar)
        
        let gradient = R.image.gradient()
        let gradientView = UIImageView(image: gradient)
        gradientView.contentMode = .scaleAspectFill
        self.view.addSubview(gradientView)
        
        let tableContainer = UIView()
        
        self.tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.allowsSelection = true
        tableView.alwaysBounceVertical = true
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 56
        tableView.keyboardDismissMode = .onDrag
        
        tableView.register(TitleWithRadioCell.self)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 80, right: 0)
        
        tableContainer.addSubview(tableView)
        view.addSubview(tableContainer)
        
        nextButton = DefaultButton()
        nextButton.setTitle("Next", for: [])
        nextButton.style = .default
        nextButton.isEnabled = false
        nextButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.viewModel.onViewEvent(.nextAction)
                
            }).disposed(by: bag)
        self.view.addSubview(nextButton)
        
        emptyLabel.text = "Region not found \nTry another search pharse"
        emptyLabel.font = Style.Font.titleSemiboldCustom(value: 17).font
        emptyLabel.textColor = Style.Colors.textWhite
        emptyLabel.numberOfLines = 0
        emptyLabel.textAlignment = .center
        emptyLabel.isHidden = true
        self.view.addSubview(emptyLabel)
        
        searchBar.snp.makeConstraints {
            $0.left.right.equalToSuperview()
            $0.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
        }
        
        tableContainer.snp.makeConstraints {
            $0.top.equalTo(searchBar.snp.bottom)
            $0.left.right.bottom.equalToSuperview()
        }
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        gradientView.snp.makeConstraints {
            $0.bottom.left.right.equalToSuperview()
        }
        
        nextButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-50)
            $0.height.equalTo(56)
        }
        
        emptyLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(30)
            $0.right.equalToSuperview().offset(-30)
            $0.centerY.equalToSuperview().offset(-200)
        }
        
        tableViewContainer = tableContainer
    }
    
    private func setupNavigationBar(animated: Bool) {
        let backItem = ViewController.BarButtonItem(kind: .text("")) { (_) in
        }
        leftItems = [backItem]
    }
    
    private func setupBindings() {
        viewModel.bindData = { [weak self] data in
            self?.sections = data
            self?.tableView.reloadData()
        }
        
        viewModel.networkError = { [weak self] message in
            self?.showAlert(type: .error,
                      titleText: "Network error",
                      subtitle: message)
        }
        
        viewModel.isLoading = { [weak self] isLoading in
            self?.configureLoader(isLoading: isLoading, loader: self?.loaderView)
        }
        
        viewModel.buttonEnable = { [weak self] isEnable in
            self?.nextButton.isEnabled = isEnable
        }
        
        viewModel.showEmpty = { [weak self] needShow in
            self?.emptyLabel.isHidden = !needShow
        }
    }
}

// MARK: - UITableViewDataSource
extension RegionSelectViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = sections[indexPath.section].items[indexPath.row]
        switch item.kind {
        case let .titleWithRadio(id: id, title: cellTitle, isSelect: isSelect):
            let cell = tableView.dequeueReusableCell(TitleWithRadioCell.self, for: indexPath)
            
            cell.setup(TitleWithRadioCellEntity(id: id,
                                                cellTitle: cellTitle,
                                                isSelectet: isSelect))

            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = sections[indexPath.section].items[indexPath.row]
        switch item.kind {
        case .titleWithRadio:
            return 55
        default:
            return 0
        }
    }
}

// MARK: - UITableViewDelegate
extension RegionSelectViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let section = sections[section]
        switch section.typeHeader {
        case .none:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionItem = sections[section]
        switch sectionItem.typeHeader {
        case .none:
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.endEditing(true)
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.onViewEvent(.cellTouch(indexPath))
    }
}

// MARK: - UISearchBarDelegate
extension RegionSelectViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.onViewEvent(.filter(searchText))
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel.onViewEvent(.filter(searchBar.text))
    }
}

// MARK: - UIKeyBoard
extension RegionSelectViewController {

    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
            
            self.keyboardSize = keyboardSize.height
            print(keyboardSize.height)
        }
        
        nextButton.snp.remakeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-15 - (keyboardSize ?? 500))
            $0.height.equalTo(56)
        }
    }

    @objc
    func keyboardWillHide(notification: NSNotification) {
        nextButton.snp.remakeConstraints {
            $0.left.equalToSuperview().offset(24)
            $0.right.equalToSuperview().offset(-24)
            $0.bottom.equalToSuperview().offset(-50)
            $0.height.equalTo(56)
        }
    }
}
