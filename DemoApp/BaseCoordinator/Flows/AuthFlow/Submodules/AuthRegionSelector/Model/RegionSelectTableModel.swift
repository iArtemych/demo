//
//  CitySelectTableModel.swift
//  
//
//  Created by Artem Chursin on 03.04.2021.
//

import Foundation

extension RegionSelectViewModel {
    func generateSections() -> [AuthTableSection<AuthTypeItem>] {
        
        var arraySections = [AuthTableSection<AuthTypeItem>]()
        
        var items = [AuthTypeItem]()
        
        filteredArray.forEach {
            items.append(AuthTypeItem(id: (String($0.id ?? 0)),
                                      kind: .titleWithRadio(id: 0,
                                                            title: ($0.title ?? "")
                                                                .attributed
                                                                .font(Style.Font.titleLightCustom(value: 16).font)
                                                                .color(Style.Colors.textWhite),
                                                            isSelect: profile.currentRegion?.id == $0.id)))
        }
        
        arraySections.append(AuthTableSection(id: "",
                                              typeHeader: .none,
                                              items: items))
        
        return arraySections
    }
}

