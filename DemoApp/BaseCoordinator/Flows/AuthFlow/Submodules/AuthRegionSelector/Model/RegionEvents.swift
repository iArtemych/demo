//
//  RegionEvents.swift
//  
//
//  Created by Artem Chursin on 03.04.2021.
//

import Foundation

enum RegionOutEvents {
    case nextAction(ClientApiObject)
}

enum RegionViewEvents {
    case viewDidLoad
    case nextAction
    case cellTouch(IndexPath)
    case filter(String?)
    case reloadData
}
