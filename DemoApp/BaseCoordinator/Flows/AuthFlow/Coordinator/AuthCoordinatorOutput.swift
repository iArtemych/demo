//
//  AuthCoordinatorOutput.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

protocol AuthCoordinatorOutput: class {
    var finishFlow: (() -> Void)? { get set }
}
