//
//  AuthCoordinator.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

import RxSwift

final class AuthCoordinator: BaseCoordinator, AuthCoordinatorOutput {
    
    var finishFlow: (() -> Void)?
    
    private let router: Router
    private let factory: AuthModuleFactory

    private let disposeBag = DisposeBag()
    private let authDataRepository: AuthDataRepositoryProtocol
    private let clientDataRepository: ClientDataRepositoryProtocol

    init(router: Router,
         factory: AuthModuleFactory,
         authDataRepository: AuthDataRepositoryProtocol,
         clientDataRepository: ClientDataRepositoryProtocol) {
        self.factory = factory
        self.router = router
        self.authDataRepository = authDataRepository
        self.clientDataRepository = clientDataRepository
    }
    
    override func start() {
        showAuthPhoneScreen()
//        showCodeEmailScreen(email: "qwerty@qwerty.ru")
//        showRegionSelectorScreen(profile: Profile())
//        showSexSelectorScreen(profile: Profile())
//        showSexPreferScreen(profile: Profile())
//        showAuthProfileScreen(profile: Profile())
    }
}

//MARK:- Presenters
extension AuthCoordinator {
    private func showAuthPhoneScreen() {
        let module = factory.makeAuthScreen { [weak self] event in
            switch event {
            case .nextStep(let email):
                self?.showCodeEmailScreen(email: email)
            }
        }
        router.setRootModule(module)
    }
    
    private func showCodeEmailScreen(email: String) {
        let module = factory.makeAuthCodeScreen(email: email,
                                                authDataRepository: authDataRepository) { [weak self] in
            self?.router.popModule(animated: true)
        } finishFlow: { [weak self] (nextFlow) in
            switch nextFlow {
            case .showMainScreen:
                self?.finishAndClose()
            break
            case .showRegistration(let profile):
                self?.showRegionSelectorScreen(profile: profile)
            case .backAction:
                self?.router.popModule(animated: true)
            }
        }
        router.push(module, animated: true)
    }
    
    private func showRegionSelectorScreen(profile: ClientApiObject) {
        let module = factory.makeRegionScreen(profile: profile) { [weak self] event in
            switch event {
            case .nextAction:
                self?.showSexSelectorScreen(profile: profile)
            }
        }
        router.push(module, animated: true)
    }
    
    private func showSexSelectorScreen(profile: ClientApiObject) {
        let module = factory.makeSexScreen(profile: profile) { [weak self] event in
            switch event {
            case .nextAction(let profile):
                self?.showSexPreferScreen(profile: profile)
            case .backAction:
                self?.router.popModule(animated: true)
            }
        }
        router.push(module, animated: true)
    }
    
    private func showSexPreferScreen(profile: ClientApiObject) {
        let module = factory.makeSexPreferScreen(profile: profile) { [weak self] event in
            switch event {
            case .nextAction(let profile):
                self?.showAuthProfileScreen(profile: profile)
            case .backAction:
                self?.router.popModule(animated: true)
            }
        }
        router.push(module, animated: true)
    }
    
    private func showAuthProfileScreen(profile: ClientApiObject) {
        let module = factory.makeAuthProfileScreen(profile: profile) { [weak self] event in
            switch event {
            case .nextAction:
                self?.finishAndClose()
            case .backAction:
                self?.router.popModule(animated: true)
            }
        }
        router.push(module, animated: true)
    }
    
    private func finishAndClose() {
        self.router.dismissModule()
        self.finishFlow?()
    }
}

