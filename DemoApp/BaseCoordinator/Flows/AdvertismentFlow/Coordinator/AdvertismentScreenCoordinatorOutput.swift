//
//  AdvertismentScreenCoordinatorOutput.swift
//  
//
//  Created by Артем Чурсин on 22.04.2021.
//

protocol AdvertismentScreenCoordinatorOutput: class {
    var finishFlow: VoidClosure? { get set }
}
