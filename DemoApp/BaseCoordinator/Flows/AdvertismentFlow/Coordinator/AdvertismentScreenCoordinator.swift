//
//  AdvertismentScreenCoordinator.swift
//  
//
//  Created by Артем Чурсин on 22.04.2021.
//

import RxSwift

final class AdvertismentScreenCoordinator: BaseCoordinator, AdvertismentScreenCoordinatorOutput {
    
    var finishFlow: (() -> Void)?
    
    private let disposeBag = DisposeBag()
    
    private let router: Router
    private weak var advertismentViewController: AdvertismentViewController!
    
    init(view: AdvertismentViewController,
         router: Router) {
        
        self.router = router
        advertismentViewController = view
    }
    
    override func start() {
        showAdvertismentScreen()
    }
    
    // MARK: - Run current flow's controllers
    private func showAdvertismentScreen() {
//        profileViewController.viewModel.outEvent = { [weak self] event in
//
//        }
    }
}






