//
//  ChattScreenCoordinator.swift

//
//  Created by Артем Чурсин on 22.04.2021.
//

import RxSwift

final class ChatScreenCoordinator: BaseCoordinator, ChatScreenCoordinatorOutput {
    
    var finishFlow: (() -> Void)?
    
    private let disposeBag = DisposeBag()
    
    private let router: Router
    private weak var chatViewController: ChatViewController!
    
    init(view: ChatViewController,
         router: Router) {
        
        self.router = router
        chatViewController = view
    }
    
    override func start() {
        showChatScreen()
    }
    
    // MARK: - Run current flow's controllers
    private func showChatScreen() {
//        profileViewController.viewModel.outEvent = { [weak self] event in
//
//        }
    }
}







