//
//  ChatScreenCoordinatorOutput.swift

//
//  Created by Артем Чурсин on 22.04.2021.
//

protocol ChatScreenCoordinatorOutput: class {
    var finishFlow: VoidClosure? { get set }
}
