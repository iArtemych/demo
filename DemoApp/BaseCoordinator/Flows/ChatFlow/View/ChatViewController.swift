//
//  ChatViewController.swift

//
//  Created by Артем Чурсин on 22.04.2021.
//

import UIKit
import RxSwift

final class ChatViewController: ViewController {
    
    // MARK: - Public
    var viewModel: ChatViewModel
    let bag = DisposeBag()
    // MARK: - Private

    // MARK: - Lifecycle
    init(viewModel: ChatViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Style.Colors.darkBackground
//        setupView()
//        setupBindings()
        self.view.backgroundColor = .green
//        viewModel.onViewEvent(.viewDidLoad)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        setupNavigationBar(animated: animated)
    }
}

