//
//  LaunchScreenViewModel.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit
import RxSwift

class LaunchScreenViewModel {
    
    //MARK: - Private
    private var finishFlow: ((NextLaunchStep)->())?
    private let bag = DisposeBag()
    private let authDataRepository: AuthDataRepositoryProtocol
    
    init(authDataRepository: AuthDataRepositoryProtocol,
         clientDataRepository: ClientDataRepositoryProtocol,
         finishFlow: @escaping ((NextLaunchStep)->())) {
        self.finishFlow = finishFlow
        self.authDataRepository = authDataRepository
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            if let email = clientDataRepository.get()?.email,
               !email.isEmpty {
                self.finishFlow?(.showMain)
            } else {
                self.finishFlow?(.showAuth)
            }
            
//            if authDataRepository.get().first?.accessToken == nil {
//                self.getTokens()
//            } else {
//                if let email = clientDataRepository.get()?.email,
//                   !email.isEmpty {
//                    self.finishFlow?(.showMain)
//                }
//                self.finishFlow?(.showAuth)
//            }
        }
    }
}

// MARK: - API
private extension LaunchScreenViewModel {
    private func getTokens() {
        finishFlow?(.showAuth)
//        RequestManager.instance.authorizationInteractor?.registration()
//            .subscribe(onNext: { [weak self] (langObjects) in
//                self?.finishFlow?(.showAuth)
//            }, onError: { (error) in
//                print(error.localizedDescription)
//            }, onCompleted: nil, onDisposed: nil)
//            .disposed(by: bag)
    }
}
