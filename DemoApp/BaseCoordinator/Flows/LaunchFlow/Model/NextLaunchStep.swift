//
//  NextStep.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation

enum NextLaunchStep {
    case showAuth, showMain
}
