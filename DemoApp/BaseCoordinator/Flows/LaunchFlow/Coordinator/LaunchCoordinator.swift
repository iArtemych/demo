//
//  LaunchCoordinator.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

import RxSwift

final class LaunchCoordinator: BaseCoordinator, LaunchCoordinatorOutput {
    
    var finishFlow: ((NextLaunchStep) -> Void)?
    
    private let router: Router
    private let factory: LaunchModuleFactory
    private let authDataRepository: AuthDataRepositoryProtocol
    private let clientDataRepository: ClientDataRepositoryProtocol

    private let disposeBag = DisposeBag()

    init(router: Router,
         factory: LaunchModuleFactory,
         authDataRepository: AuthDataRepositoryProtocol,
         clientDataRepository: ClientDataRepositoryProtocol) {
        self.factory = factory
        self.router = router
        self.authDataRepository = authDataRepository
        self.clientDataRepository = clientDataRepository
    }
    
    override func start() {
        showLauncScreen()
    }
}

//MARK:- Presenters
extension LaunchCoordinator {
    private func showLauncScreen() {
        let launchModule = factory.makeLaunchScreen(authDataRepository: authDataRepository,
                                                    clientDataRepository: clientDataRepository) { [weak self] nextFlow in
            self?.finishAndClose(nextFlow: nextFlow)
        }
        router.setRootModule(launchModule)
    }
    
    private func finishAndClose(nextFlow: NextLaunchStep) {
        self.router.dismissModule()
        self.finishFlow?(nextFlow)
    }
}
