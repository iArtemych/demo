//
//  LaunchCoordinatorOutput.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

protocol LaunchCoordinatorOutput: class {
  var finishFlow: ((NextLaunchStep) -> Void)? { get set }
}
