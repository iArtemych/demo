//
//  LaunchScreenViewController.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit
import SnapKit

class LaunchScreenViewController: UIViewController {
    
    let viewModel: LaunchScreenViewModel
    
    // MARK: - Lifecycle
    init(viewModel: LaunchScreenViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}

//MARK: - Setup View
extension LaunchScreenViewController {
    private func setupView() {
        self.view.backgroundColor = Style.Colors.darkBackground
        let logo = R.image.siFree()
        let gradient = R.image.gradient()
        
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        self.view.addSubview(imageView)
        imageView.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
        
        let gradientView = UIImageView(image: gradient)
        gradientView.contentMode = .scaleAspectFill
        self.view.addSubview(gradientView)
        gradientView.snp.makeConstraints {
            $0.bottom.left.right.equalToSuperview()
        }
    }
}
