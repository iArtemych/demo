//
//  ProfileCoordinator.swift

//
//  Created by Artem Chursin on 10.04.2021.
//

import RxSwift

final class ProfileScreenCoordinator: BaseCoordinator, ProfileScreenCoordinatorOutput {
    
    var finishFlow: (() -> Void)?
    
    private let disposeBag = DisposeBag()
    
    private let router: Router
    private weak var profileViewController: ProfileViewController!
    
    init(view: ProfileViewController,
         router: Router) {
        
        self.router = router
        profileViewController = view
    }
    
    override func start() {
        showProfileScreen()
    }
    
    // MARK: - Run current flow's controllers
    private func showProfileScreen() {
        
    }
}





