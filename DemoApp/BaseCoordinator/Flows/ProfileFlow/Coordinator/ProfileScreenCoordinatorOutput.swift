//
//  ProfileScreenCoordinatorOutput.swift

//
//  Created by Artem Chursin on 10.04.2021.
//

protocol ProfileScreenCoordinatorOutput: class {
    var finishFlow: VoidClosure? { get set }
}
