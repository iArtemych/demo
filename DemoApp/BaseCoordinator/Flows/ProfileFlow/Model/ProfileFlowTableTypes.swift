//
//  ProfileFlowTableTypes.swift

//
//  Created by Artem Chursin on 24.04.2021.
//

import UIKit

enum ProfileTypeHeader {
    case none
    case title(NSAttributedString)
}

struct ProfileTableSection<Item> {
    let id: String
    let typeHeader: ProfileTypeHeader
    let items: [Item]
}

struct ProfileTypeItem {
    enum Kind {
        case titleWithArrow(id: String, title: NSAttributedString)
        case titleSelector(id: String, title: NSAttributedString, isEnable: Bool, selectorState: ((Bool)->()))
        case profileCell(id: String,
                         text: NSAttributedString?,
                         placeholder: NSAttributedString?,
                         placeholderImage: UIImage?,
                         placeholderImageURL: String?,
                         imageUpdater: VoidClosure,
                         textUpdater: ((String?)->()))
        case empty
        case none
    }
    
    let id: String
    let kind: Kind
}
