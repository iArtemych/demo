//
//  ProfileViewController.swift

//
//  Created by Artem Chursin on 10.04.2021.
//

import UIKit
import RxSwift

final class ProfileViewController: ViewController {
    
    // MARK: - Public
    var viewModel: ProfileViewModel
    let bag = DisposeBag()

    // MARK: - Lifecycle
    init(viewModel: ProfileViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Style.Colors.darkBackground
        setupView()
        setupBindings()
        viewModel.onViewEvent(.viewDidLoad)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar(animated: animated)
        viewModel.onViewEvent(.viewWillAppear)
    }
}

//MARK: - Setup View
private extension ProfileViewController {
    private func setupView() {
        self.view.backgroundColor = Style.Colors.darkBackground
        
    }
    
    private func setupBindings() {
    }
    
    private func setupNavigationBar(animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
}
