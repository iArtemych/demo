//
//  ProfileScreenStates.swift

//
//  Created by Artem Chursin on 17.04.2021.
//

import Foundation

enum ProfileScreenState {
    case empty, loading
    case withAdvertisment
}
