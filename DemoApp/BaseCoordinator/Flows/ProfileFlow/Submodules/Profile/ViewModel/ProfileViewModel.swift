//
//  ProfileViewModel.swift

//
//  Created by Artem Chursin on 10.04.2021.
//

import UIKit
import RxSwift
import Rswift

final class ProfileViewModel {
    
    var clientDataRepository: ClientDataRepositoryProtocol
    
    init(clientDataRepository: ClientDataRepositoryProtocol) {
        self.clientDataRepository = clientDataRepository
    }
}

// MARK: - Output
extension ProfileViewModel {
    
    func onViewEvent(_ event: ProfileViewEvents) {
        switch event {
        default:
            break
        }
    }
}
