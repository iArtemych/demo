//
//  TabbarCoordinator.swift
//  
//
//  Created by Artem Chursin on 10.04.2021.
//

import RxSwift

class TabbarCoordinator: BaseCoordinator {
    
    private let coordinatorFactory: CoordinatorFactory
    private let router: Router
    private let clientDataRepository: ClientDataRepositoryProtocol
    private let disposeBag = DisposeBag()
    
    private weak var tabBarController: CustomTabBarController?
    private let tabbarItems: [TabItem] = TabItem.allCases
    
    init(coordinatorFactory: CoordinatorFactory,
         router: Router,
         clientDataRepository: ClientDataRepositoryProtocol) {
        
        self.coordinatorFactory = coordinatorFactory
        self.router = router
        self.clientDataRepository = clientDataRepository
        super.init()
    }
    
    override func start() {
        let tabbar = createTabbarModule()
        router.setRootModule(tabbar, hideBar: true)
    }
    
    private func createTabbarModule() -> Presentable {
        let tabbarViewController = CustomTabBarController()
        tabbarViewController.didSelectTab = { [weak self] index in
            guard let strongSelf = self else { return }

            //open auth
            let item = strongSelf.tabbarItems[index]
            switch item {
            case .meetings:
                break
            case .chats:
                break
            case .profile:
                break
            }
        }
        let items = self.tabbarItems.enumerated()
            .map({ (index, tab) -> (UITabBarItem, Coordinator, Presentable, UIViewController?) in
                let item = UITabBarItem(title: tab.displayTitle, image: tab.icon, tag: index)
                let module: (Coordinator, Presentable)

                var tabItemController: UIViewController?
                let routerFactory = { (controller: UIViewController) -> Router in
                    let navController = UINavigationController(rootViewController: controller)
                    tabItemController = navController
                    return RouterImp(rootController: navController)
                }

                switch tab {
                case .meetings:
                    print("meetings")
                    module = coordinatorFactory.makeAdvertismentCoordinatorBox(clientDataRepository: clientDataRepository, routerFactory: routerFactory)
                case .chats:
                    print("chats")
                    module = coordinatorFactory.makeChatsCoordinatorBox(clientDataRepository: clientDataRepository, routerFactory: routerFactory)
                case .profile:
                    print("profile")
                    module = coordinatorFactory.makeProfileCoordinatorBox(clientDataRepository: clientDataRepository, routerFactory: routerFactory)
//                case .profile:
//                    print("profile")
//                    module = coordinatorFactory.makeProfileCoordinatorBox(
//                        routerFactory: routerFactory,
//                        onEvent: { (event: ProfileCoordinatorEvent) in
//
//                        })
                }

                return (item, module.0, module.1, tabItemController)
            })
            .compactMap({ (tabBarItem, coordinator, presentable, tabItemController)
                -> (UITabBarItem, Coordinator, UIViewController)? in
                guard let controller = tabItemController else { return nil }
                print("\(tabBarItem) \(coordinator) \(controller)")
                return (tabBarItem, coordinator, controller)
            })

        items.forEach { (tabBarItem, coordinator, controller) in
            controller.tabBarItem = tabBarItem
            self.addDependency(coordinator)
            coordinator.start()
        }

        let viewControllers = items.map { $0.2 }
        tabbarViewController.setViewControllers(viewControllers, animated: false)
        
        self.tabBarController = tabbarViewController
        return tabbarViewController
    }
}

extension TabbarCoordinator: TabbarCoordinatorOutput {
    
}

