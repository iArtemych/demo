//
//  CustomTabBarController.swift
//  
//
//  Created by Artem Chursin on 10.04.2021.
//

import UIKit
import SnapKit

class CustomTabBarController: UITabBarController, TabbarView {
    
    var onViewDidLoad: VoidClosure?
    var didSelectTab: IntClosure?

    private(set) var customTabBar: TabNavigationMenu!
    let fullTabBarHeight: CGFloat = 100.0
    let visibleTabBarHeight: CGFloat = 80.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadTabBar()
        delegate = self
    }
    
    private func loadTabBar() {
        let tabItems: [TabItem] = [.meetings, .chats, .profile]
        self.setupCustomTabBar(tabItems)
        self.selectedIndex = 0
    }
    
    private func setupCustomTabBar(_ items: [TabItem]){
        let frame = CGRect(x: tabBar.frame.origin.x, y: tabBar.frame.origin.x, width: tabBar.frame.width, height: fullTabBarHeight)
        
        tabBar.isHidden = true
        
        self.customTabBar = TabNavigationMenu(menuItems: items, frame: frame, visibleHeight: visibleTabBarHeight)
        
        self.customTabBar.clipsToBounds = true
        self.customTabBar.itemTapped = self.changeTab
        
        self.view.addSubview(customTabBar)
        
        customTabBar.snp.makeConstraints {
            $0.left.right.bottom.equalToSuperview()
            $0.height.equalTo(fullTabBarHeight)
        }
        
        self.view.layoutIfNeeded()
    }
    
    func changeTab(tab: Int) {
        self.selectedIndex = tab
        didSelectTab?(tab)
    }
    
    func acticateTab(newTab: Int, oldTab: Int) {
        customTabBar.switchTab(from: oldTab, to: newTab)
    }
    
    func reloadTabbar(_ haveNotifications: Bool) {
        let tabItems: [TabItem] = [.meetings, .chats, .profile]
        self.setupCustomTabBar(tabItems)
    }
}

extension CustomTabBarController: UITabBarControllerDelegate {

    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return MyTransition(viewControllers: tabBarController.viewControllers)
    }
}

class MyTransition: NSObject, UIViewControllerAnimatedTransitioning {

    let viewControllers: [UIViewController]?
    let transitionDuration: Double = 0.25

    init(viewControllers: [UIViewController]?) {
        self.viewControllers = viewControllers
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return TimeInterval(transitionDuration)
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        guard
            let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let fromView = fromVC.view,
            let fromIndex = getIndex(forViewController: fromVC),
            let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
            let toView = toVC.view,
            let toIndex = getIndex(forViewController: toVC)
            else {
                transitionContext.completeTransition(false)
                return
        }

        let frame = transitionContext.initialFrame(for: fromVC)
        var fromFrameEnd = frame
        var toFrameStart = frame
        fromFrameEnd.origin.x = toIndex > fromIndex ? frame.origin.x - frame.width : frame.origin.x + frame.width
        toFrameStart.origin.x = toIndex > fromIndex ? frame.origin.x + frame.width : frame.origin.x - frame.width
        toView.frame = toFrameStart

        DispatchQueue.main.async {
            transitionContext.containerView.addSubview(toView)
            UIView.animate(withDuration: self.transitionDuration, animations: {
                fromView.frame = fromFrameEnd
                toView.frame = frame
            }, completion: {success in
                fromView.removeFromSuperview()
                transitionContext.completeTransition(success)
            })
        }
    }

    func getIndex(forViewController vc: UIViewController) -> Int? {
        guard let vcs = self.viewControllers else { return nil }
        for (index, thisVC) in vcs.enumerated() {
            if thisVC == vc { return index }
        }
        return nil
    }
}

// MARK: - CustomTabBarController + Convenience

extension UIViewController {
    var customTabbarController: CustomTabBarController? {
        self.tabBarController as? CustomTabBarController
    }
}

