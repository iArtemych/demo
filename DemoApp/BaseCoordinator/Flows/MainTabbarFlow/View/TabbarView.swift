//
//  TabbarView.swift
//  
//
//  Created by Artem Chursin on 10.04.2021.
//

protocol TabbarView: class {
    var onViewDidLoad: VoidClosure? { get set }
    var didSelectTab: IntClosure? { get set }
}
