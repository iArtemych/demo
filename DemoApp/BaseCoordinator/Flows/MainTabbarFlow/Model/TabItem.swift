//
//  TabItem.swift
//  
//
//  Created by Artem Chursin on 10.04.2021.
//

import UIKit

enum TabItem: CaseIterable {
    case meetings
    case chats
    case profile
    
    var stringValue: String {
        switch self {
        case .meetings:
            return "Meetings"
        case .chats:
            return "Chats"
        case .profile:
            return "Profile"
        }
    }
    
    var icon: UIImage? {
        switch self {
        case .meetings:
            return R.image.meetings()
        case .chats:
            return R.image.chats()
        case .profile:
            return R.image.profile()
        }
    }
    
    var displayTitle: String {
        return self.stringValue.capitalized(with: nil)
    }
}

