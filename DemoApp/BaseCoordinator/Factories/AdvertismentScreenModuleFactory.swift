//
//  AdvertismentScreenModuleFactory.swift

//
//  Created by Артем Чурсин on 22.04.2021.
//

protocol AdvertismentScreenModuleFactory {
    func makeAdvertismentScreenModule(clientDataRepository: ClientDataRepositoryProtocol) -> AdvertismentViewController
}
