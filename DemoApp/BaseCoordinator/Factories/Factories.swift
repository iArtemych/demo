//
//  Factories.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit
protocol CoordinatorFactory {
    
    //MARK: - Launch
    func makeLaunchCoordinatorBox(router: Router,
                                  authDataRepository: AuthDataRepositoryProtocol,
                                  clientDataRepository: ClientDataRepositoryProtocol) -> Coordinator & LaunchCoordinatorOutput
    
    //MARK: - Auth
    func makeAuthCoordinatorBox(router: Router,
                                authDataRepository: AuthDataRepositoryProtocol,
                                clientDataRepository: ClientDataRepositoryProtocol) -> Coordinator & AuthCoordinatorOutput
    
    //MARK: - Main
    func makeTabbarCoordinator(router: Router, clientDataRepository : ClientDataRepositoryProtocol) -> Coordinator & TabbarCoordinatorOutput

    //MARK: - Profile
    func makeProfileCoordinatorBox(clientDataRepository: ClientDataRepositoryProtocol, routerFactory: ((UIViewController) -> Router)) -> (coordinator: Coordinator & ProfileScreenCoordinatorOutput, presentable: Presentable)

    //MARK: - Advertisment
    func makeAdvertismentCoordinatorBox(clientDataRepository: ClientDataRepositoryProtocol,
                                   routerFactory: ((UIViewController) -> Router)) -> (coordinator: Coordinator & AdvertismentScreenCoordinatorOutput, presentable: Presentable)
    
    //MARK: - Chats
    func makeChatsCoordinatorBox(clientDataRepository: ClientDataRepositoryProtocol,
                                   routerFactory: ((UIViewController) -> Router)) -> (coordinator: Coordinator & ChatScreenCoordinatorOutput, presentable: Presentable)
}

