//
//  LaunchModuleFactory.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

protocol LaunchModuleFactory {
    func makeLaunchScreen(authDataRepository: AuthDataRepositoryProtocol,
                          clientDataRepository: ClientDataRepositoryProtocol,
                          finishFlow: @escaping (NextLaunchStep) -> ()) -> Presentable
}
