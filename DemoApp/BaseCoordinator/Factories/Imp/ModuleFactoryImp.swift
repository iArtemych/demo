//
//  ModuleFactoryImp.swift
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

final class ModuleFactoryImp {
    
}

////MARK:- Launch factory
extension ModuleFactoryImp: LaunchModuleFactory {
    func makeLaunchScreen(authDataRepository: AuthDataRepositoryProtocol,
                          clientDataRepository: ClientDataRepositoryProtocol,
                          finishFlow: @escaping (NextLaunchStep) -> ()) -> Presentable {
        let vm = LaunchScreenViewModel(authDataRepository: authDataRepository,
                                       clientDataRepository: clientDataRepository,
                                       finishFlow: finishFlow)
        let view = LaunchScreenViewController(viewModel: vm)
        return view
    }
}

////MARK:- Auth factory
extension ModuleFactoryImp: AuthModuleFactory {
    func makeAuthScreen(outEvent: ((AuthOutEvents) ->())?) -> Presentable {
        let vm = AuthPhoneViewModel(outEvent: outEvent)
        let view = AuthViewController(viewModel: vm)
        return view
    }
    
    func makeAuthCodeScreen(email: String,
                            authDataRepository: AuthDataRepositoryProtocol,
                            backAction: VoidClosure?,
                            finishFlow: @escaping (AuthCodeOutEvent) -> ()) -> Presentable {
        let vm = AuthCodeViewModel(email: email,
                                   authDataRepository: authDataRepository,
                                   backAction: backAction,
                                   finish: finishFlow)
        let view = AuthCodeViewController(viewModel: vm)
        return view
    }
    
    func makeRegionScreen(profile: ClientApiObject, outEvent: ((RegionOutEvents) ->())?) -> Presentable {
        let vm = RegionSelectViewModel(profile: profile, outEvent: outEvent)
        let view = RegionSelectViewController(viewModel: vm)
        return view
    }
    
    func makeSexScreen(profile: ClientApiObject, outEvent: ((SexOutEvents) ->())?) -> Presentable {
        let vm = SexSelectorViewModel(profile: profile, outEvent: outEvent)
        let view = SexSelectorViewController(viewModel: vm)
        return view
    }
    
    func makeSexPreferScreen(profile: ClientApiObject, outEvent: ((SexPreferOutEvents) ->())?) -> Presentable {
        let vm = SexPreferViewModel(profile: profile, outEvent: outEvent)
        let view = SexPreferViewController(viewModel: vm)
        return view
    }
    
    func makeAuthProfileScreen(profile: ClientApiObject,
                               outEvent: ((AuthProfileOutEvents) ->())?) -> Presentable {
        let vm = AuthProfileViewModel(profile: profile, outEvent: outEvent)
        let view = AuthProfileViewController(viewModel: vm)
        return view
    }
}

//MARK:- ProfileFlow factory
extension ModuleFactoryImp: ProfileScreenModuleFactory {
    
    func makeProfileScreenModule(clientDataRepository: ClientDataRepositoryProtocol) -> ProfileViewController {
        let vm = ProfileViewModel(clientDataRepository: clientDataRepository)
        return ProfileViewController(viewModel: vm)
    }
}

//MARK:- ChatFlow factory
extension ModuleFactoryImp: ChatScreenModuleFactory {
    func makeChatScreenModule(clientDataRepository: ClientDataRepositoryProtocol) -> ChatViewController {
        let vm = ChatViewModel()
        return ChatViewController(viewModel: vm)
    }
}

//MARK:- AdvertismentFlow factory
extension ModuleFactoryImp: AdvertismentScreenModuleFactory {
    func makeAdvertismentScreenModule(clientDataRepository: ClientDataRepositoryProtocol) -> AdvertismentViewController {
        let vm = AdvertismentViewModel()
        return AdvertismentViewController(viewModel: vm)
    }
}


