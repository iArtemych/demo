//
//  ChatScreenModuleFactory.swift

//
//  Created by Артем Чурсин on 22.04.2021.
//

protocol ChatScreenModuleFactory {
    func makeProfileScreenModule(clientDataRepository: ClientDataRepositoryProtocol) -> ProfileViewController
}
