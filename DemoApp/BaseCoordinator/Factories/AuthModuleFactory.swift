//
//  AuthModuleFactory.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

protocol AuthModuleFactory {
    func makeAuthScreen(outEvent: ((AuthOutEvents) ->())?) -> Presentable
    
    func makeAuthCodeScreen(email: String,
                            authDataRepository: AuthDataRepositoryProtocol,
                            backAction: VoidClosure?,
                            finishFlow: @escaping (AuthCodeOutEvent) -> ()) -> Presentable
    
    func makeRegionScreen(profile: ClientApiObject, outEvent: ((RegionOutEvents) ->())?) -> Presentable
    
    func makeSexScreen(profile: ClientApiObject, outEvent: ((SexOutEvents) ->())?) -> Presentable
    
    func makeSexPreferScreen(profile: ClientApiObject, outEvent: ((SexPreferOutEvents) ->())?) -> Presentable
    
    func makeAuthProfileScreen(profile: ClientApiObject,
                               outEvent: ((AuthProfileOutEvents) ->())?) -> Presentable
}
