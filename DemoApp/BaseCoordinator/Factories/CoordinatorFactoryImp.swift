//
//  CoordinatorFactoryImp.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit


final class CoordinatorFactoryImp {
    
    let moduleFactoryImp = ModuleFactoryImp()
    
    init() {
    }
    
}

extension CoordinatorFactoryImp: CoordinatorFactory {
    
    //MARK: - Launch
    func makeLaunchCoordinatorBox(router: Router,
                                  authDataRepository: AuthDataRepositoryProtocol,
                                  clientDataRepository: ClientDataRepositoryProtocol)
    -> Coordinator & LaunchCoordinatorOutput {
        
        let coordinator = LaunchCoordinator(router: router,
                                            factory: moduleFactoryImp,
                                            authDataRepository: authDataRepository,
                                            clientDataRepository: clientDataRepository)
        return coordinator
    }
    
    //MARK: - Auth
    func makeAuthCoordinatorBox(router: Router,
                                authDataRepository: AuthDataRepositoryProtocol,
                                clientDataRepository: ClientDataRepositoryProtocol) -> AuthCoordinatorOutput & Coordinator {
        let coordinator = AuthCoordinator(router: router,
                                          factory: moduleFactoryImp,
                                          authDataRepository: authDataRepository,
                                          clientDataRepository: clientDataRepository)
        return coordinator
    }
    
    //MARK: - TabBar
    func makeTabbarCoordinator(router: Router, clientDataRepository: ClientDataRepositoryProtocol) -> Coordinator & TabbarCoordinatorOutput {
        return TabbarCoordinator(coordinatorFactory: self, router: router, clientDataRepository: clientDataRepository)
    }
    
    //MARK: - Chats
    func makeChatsCoordinatorBox(clientDataRepository: ClientDataRepositoryProtocol,
                                   routerFactory: ((UIViewController) -> Router)) -> (coordinator: Coordinator & ChatScreenCoordinatorOutput, presentable: Presentable) {
        
        let module = ModuleFactoryImp().makeChatScreenModule(clientDataRepository: clientDataRepository)
        let router = routerFactory(module)
        let coordinator = ChatScreenCoordinator(view: module,
                                                   router: router)
        return (coordinator, module)
    }
    
    //MARK: - Advertisment
    func makeAdvertismentCoordinatorBox(clientDataRepository: ClientDataRepositoryProtocol,
                                   routerFactory: ((UIViewController) -> Router)) -> (coordinator: Coordinator & AdvertismentScreenCoordinatorOutput, presentable: Presentable) {
        
        let module = ModuleFactoryImp().makeAdvertismentScreenModule(clientDataRepository: clientDataRepository)
        let router = routerFactory(module)
        let coordinator = AdvertismentScreenCoordinator(view: module,
                                                   router: router)
        return (coordinator, module)
    }
    
    //MARK: - Profile
    func makeProfileCoordinatorBox(clientDataRepository: ClientDataRepositoryProtocol,
                                   routerFactory: ((UIViewController) -> Router)) -> (coordinator: Coordinator & ProfileScreenCoordinatorOutput, presentable: Presentable) {
        
        let module = ModuleFactoryImp().makeProfileScreenModule(clientDataRepository: clientDataRepository)
        let router = routerFactory(module)
        let coordinator = ProfileScreenCoordinator(view: module,
                                                   router: router)
        return (coordinator, module)
    }
    
    // MARK: - navigation
    private func router(_ navController: UINavigationController?) -> Router {
        return RouterImp(rootController: navigationController(navController))
    }
    private func navigationController(_ navController: UINavigationController?) -> UINavigationController {
        if let navController = navController {
            return navController
        } else {
            return UINavigationController()
        }
    }
}

