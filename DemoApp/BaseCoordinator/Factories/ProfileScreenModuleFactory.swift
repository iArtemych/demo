//
//  ProfileScreenModuleFactory.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

protocol ProfileScreenModuleFactory {
    func makeProfileScreenModule(clientDataRepository: ClientDataRepositoryProtocol) -> ProfileViewController
}
