//
//  RouterImp.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

final class RouterImp: NSObject {

    init(rootController: UINavigationController) {
        self.rootController = rootController
        self.completions = [:]
    }
    
    private weak var rootController: UINavigationController?
    private var completions: [UIViewController : () -> Void]

    private func runCompletion(for controller: UIViewController) {
        guard let completion = completions[controller] else { return }
        completion()
        completions.removeValue(forKey: controller)
    }
}

// MARK: - Router

extension RouterImp: Router {
    func toPresent() -> UIViewController? {
        return rootController
    }

    func present(_ module: Presentable?, animated: Bool, withNavigation: Bool) -> UIViewController? {
        guard let controller = module?.toPresent() else { return nil }
        let newController = withNavigation ? UINavigationController(rootViewController: controller) : controller
        if #available(iOS 13.0, *) {
            newController.isModalInPresentation = true
        }
        rootController?.present(newController, animated: animated, completion: nil)
        return newController
    }

    func dismissModule(animated: Bool, completion: (() -> Void)?) {
        rootController?.dismiss(animated: animated, completion: completion)
    }

    func push(_ module: Presentable?, animated: Bool, hideBottomBar: Bool, completion: (() -> Void)?) {
        guard
            let controller = module?.toPresent(),
            (controller is UINavigationController == false)
            else { assertionFailure("Deprecated push UINavigationController."); return }
        if let completion = completion {
            completions[controller] = completion
        }
        controller.hidesBottomBarWhenPushed = hideBottomBar
        rootController?.pushViewController(controller, animated: animated)
    }

    func popModule(animated: Bool) {
        if let controller = rootController?.popViewController(animated: animated) {
            runCompletion(for: controller)
        }
    }

    func setRootModule(_ module: Presentable?, hideBar: Bool) {
        guard let controller = module?.toPresent() else { return }
        rootController?.setViewControllers([controller], animated: false)
        rootController?.isNavigationBarHidden = hideBar
    }

    func popToRootModule(animated: Bool) {
        if let controllers = rootController?.popToRootViewController(animated: animated) {
            controllers.forEach { controller in
                runCompletion(for: controller)
            }
        }
    }
}

