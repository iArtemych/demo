//
//  Router.swift

//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

protocol Router: Presentable {
    func present(_ module: Presentable?, animated: Bool, withNavigation: Bool) -> UIViewController?
    func dismissModule(animated: Bool, completion: (() -> Void)?)

    func push(_ module: Presentable?, animated: Bool, hideBottomBar: Bool, completion: (() -> Void)?)
    func popModule(animated: Bool)

    func setRootModule(_ module: Presentable?, hideBar: Bool)
    func popToRootModule(animated: Bool)
}

// MARK: - Convenience

extension Router {
    func present(_ module: Presentable?) {
        present(module, animated: true)
    }
    func present(_ module: Presentable?, animated: Bool) {
        _ = present(module, animated: animated, withNavigation: false)
    }

    func dismissModule() {
        dismissModule(animated: true, completion: nil)
    }

    func push(_ module: Presentable?) {
        push(module, animated: true)
    }
    func push(_ module: Presentable?, hideBottomBar: Bool) {
        push(module, animated: true, hideBottomBar: hideBottomBar, completion: nil)
    }
    func push(_ module: Presentable?, animated: Bool) {
        push(module, animated: animated, completion: nil)
    }
    func push(_ module: Presentable?, animated: Bool, completion: (() -> Void)?) {
        push(module, animated: animated, hideBottomBar: false, completion: completion)
    }

    func popModule() {
        popModule(animated: true)
    }

    func setRootModule(_ module: Presentable?) {
        setRootModule(module, hideBar: false)
    }
}

// MARK: - Alert

extension Router {
    func showAlertController(
        title: String?,
        message: String?,
        preferredStyle: UIAlertController.Style,
        actions: [UIAlertAction]) {

        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        for action in actions {
            alert.addAction(action)
        }
        self.present(alert, animated: true)
    }
}

// MARK: - CustomModal

//extension Router {
//    func showCustomModal(
//        title: String,
//        data: [TableViewSectionProtocol],
//        didSelect: ((EntityTableView.TableRowSelection) -> ())?,
//        willDismiss: (() -> Void)? = nil) {
//
//        let vc = CustomModalViewController(
//            title: title,
//            data: data,
//            didSelect: { [weak self] item in
//                didSelect?(item)
//                self?.dismissModule(animated: true, completion: nil)
//            },
//            willDismiss: willDismiss)
//
//        vc.modalPresentationStyle = .overFullScreen
//        self.present(vc, animated: true)
//    }
//}

