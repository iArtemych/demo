//
//  Coordinator.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

protocol Coordinator: class {
  func start()
  func start(with option: DeepLinkOption?)
}
