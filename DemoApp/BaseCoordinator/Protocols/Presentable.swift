//
//  Presentable.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

protocol Presentable: class {
  func toPresent() -> UIViewController?
}

extension UIViewController: Presentable {
  
  func toPresent() -> UIViewController? {
    return self
  }
}

