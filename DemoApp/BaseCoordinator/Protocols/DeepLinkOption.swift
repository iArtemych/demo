//
//  DeepLinkOption.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation

struct DeepLinkURLConstants {
  static let Onboarding = "onboarding"
  static let Login = "login"
}

enum DeepLinkOption {
  
  case onboarding
  case login
  
  static func build(with userActivity: NSUserActivity) -> DeepLinkOption? {
    nil
  }
  
  static func build(with dict: [String : AnyObject]?) -> DeepLinkOption? {
    guard let id = dict?["launch_id"] as? String else { return nil }
    switch id {
    case DeepLinkURLConstants.Onboarding:
        return .onboarding
    case DeepLinkURLConstants.Login:
        return .login
    default:
        return .none
    }
  }
}

