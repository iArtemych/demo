//
//  DefaultButton.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

final class DefaultButton: UIButton {
    var action: Action?
    private var shadowLayer: CAShapeLayer!
    
    var style: ButtonStyle = .default {
        didSet {
            self.setupColors()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.commonInit()
    }
    
    private func commonInit() {
        self.style = .default
        self.titleLabel?.font = Style.Font.titleBoldCustom(value: 15).font
        
        self.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }
    
    @objc private func buttonAction(_ sender: UIButton) {
        self.action?()
    }
    
    override var isHighlighted: Bool {
        didSet {
            self.titleLabel?.alpha = self.isHighlighted ? 0.5 : 1
            self.imageView?.alpha = self.isHighlighted ? 0.5 : 1
            
            self.setupColors()
        }
    }
    
    override var isEnabled: Bool {
        didSet {
            self.setupColors()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 60), bottom: 5, right: 5)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)!)
        }
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 14).cgPath

            shadowLayer.shadowColor = !self.isEnabled ? self.style.disabledBackgroundColor.cgColor : self.isHighlighted ? self.style.highlitedBackgroundColor.cgColor : self.style.backgroundColor.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0, height: 10)
            shadowLayer.shadowOpacity = 0.3
            shadowLayer.shadowRadius = 10
            shadowLayer.fillColor = !self.isEnabled ? self.style.disabledBackgroundColor.cgColor : self.isHighlighted ? self.style.highlitedBackgroundColor.cgColor : self.style.backgroundColor.cgColor
            layer.insertSublayer(shadowLayer, at: 0)
        }
        
//        self.layer.cornerRadius = self.frame.height * 0.5
//        self.layer.cornerRadius = 14
        
    }
    
    private func setupColors() {
        self.setTitleColor(self.isEnabled ? self.style.titleColor : self.style.disabledTitleColor,
                           for: .normal)
        if shadowLayer != nil {
            shadowLayer.fillColor = !self.isEnabled ? self.style.disabledBackgroundColor.cgColor : self.isHighlighted ? self.style.highlitedBackgroundColor.cgColor : self.style.backgroundColor.cgColor
            shadowLayer.shadowColor = !self.isEnabled ? self.style.disabledBackgroundColor.cgColor : self.isHighlighted ? self.style.highlitedBackgroundColor.cgColor : self.style.backgroundColor.cgColor
        }
        if style == .bordered {
            self.layer.borderWidth = 2
            self.layer.borderColor = Style.Colors.secondaryRed.cgColor
            self.layer.cornerRadius = 16
        } else {
            self.layer.borderWidth = 0
        }
    }
    
}

extension DefaultButton {
    
    enum ButtonStyle {
        
        case `default`
        case gray
        case clear
        case bordered
        
        var titleColor: UIColor {
            switch self {
            case .default:
                return Style.Colors.white
            case .gray:
                return Style.Colors.white
            case .clear:
                return Style.Colors.white
            case .bordered:
                return Style.Colors.primaryRed
            }
        }
        
        var disabledTitleColor: UIColor {
            switch self {
            case .default, .gray, .clear, .bordered:
                return .white
            }
        }
        
        var backgroundColor: UIColor {
            switch self {
            case .default:
                return Style.Colors.secondaryRed
            case .gray:
                return Style.Colors.buttonDisabled
            case .clear:
                return Style.Colors.white
            case .bordered:
                return Style.Colors.darkBackground
            }
        }
        
        var disabledBackgroundColor: UIColor {
            switch self {
            case .default, .gray, .clear, .bordered:
                return Style.Colors.buttonDisabled
            }
        }
        
        var highlitedBackgroundColor: UIColor {
            switch self {
            case .default:
                return Style.Colors.secondaryRed.withAlphaComponent(0.8)
            case .gray:
                return Style.Colors.secondaryRed.withAlphaComponent(0.8)
            case .clear:
                return Style.Colors.white
            case .bordered:
                return Style.Colors.darkBackground
            }
        }
    }
}

extension DefaultButton {
    typealias Action = () -> Void
    
    struct Model {
        
        private var title: String
        private var style: ButtonStyle
        
        private(set) var action: Action?
        
        init(title: String, style: ButtonStyle, action: Action?) {
            self.title = title
            self.action = action
            self.style = style
        }
        
        static func `default`(title: String, style: ButtonStyle = .default, action: Action? = nil) -> Self {
            return self.init(title: title, style: style, action: action)
        }
        
        func button(index: Int? = nil) -> DefaultButton {
            let button = DefaultButton()
            
            button.style = self.style
            button.setTitle(self.title, for: .normal)
            
            button.action = { self.action?() }
            
            if let index = index {
                button.tag = index
            }
            
            return button
        }
        
    }
}

