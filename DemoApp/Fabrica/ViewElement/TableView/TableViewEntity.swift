//
//  TableViewEntity.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit
import RxDataSources

protocol AnyTableEntity: AnyViewEntity { }

protocol AnyTableCellEntity: AnyTableEntity {
    
    static var tableCellClass: UITableViewCell.Type { get }
    
    func cellHeight(tableView: UITableView, indexPath: IndexPath) -> CGFloat
}

protocol AnyViewEntity {
    
    static var entityIdentifier: String { get }
    
    func setup(anyView: Any)
}

extension AnyViewEntity {
    
    static var entityIdentifier: String { String(describing: self) }
}

extension AnyViewEntity where Self: ViewEntity {
    
    func setup(anyView: Any) {
        guard let view = anyView as? Self.View else { preconditionFailure() }
        
        self.setup(view: view)
    }
}

protocol __View { }

protocol ViewEntity: AnyViewEntity {
    
    associatedtype View: __View
    
    func setup(view: View)
}

extension ViewEntity {
    
    static var entityIdentifier: String { String(describing: self.View.self) }
}

protocol AnySizableTableCellEntity: AnyTableCellEntity {

    static var anySizingCell: UITableViewCell { get }
}

protocol SizableTableCellEntity: TableCellEntity, AnySizableTableCellEntity where Self.View: ContentSizable {
    
    static var viewForSize: Self.View { get }
}

extension SizableTableCellEntity {
    
    static var anySizingCell: UITableViewCell { self.viewForSize }
}

extension SizableTableCellEntity {
    
    func cellHeight(tableView: UITableView, indexPath: IndexPath) -> CGFloat {
        Self.viewForSize.frame.size.width = tableView.frame.width
        self.setup(view: Self.viewForSize)
        
        return Self.viewForSize.contentHeight
    }
}

protocol ContentSizable: class {
    
    var contentHeight: CGFloat { get }
}

extension UITableViewCell: __View { }

extension TableCellEntity {
    
    static var tableCellClass: UITableViewCell.Type { self.View.self }
    
    func cellHeight(tableView: UITableView, indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

protocol AnyTableHeaderFooterViewEntity: AnyTableEntity {
    
    static var tableHeaderFooterViewClass: UITableViewHeaderFooterView.Type { get }
    
    func viewHeight(tableView: UITableView, section: Int) -> CGFloat
}

extension UITableViewHeaderFooterView: __View { }

protocol TableHeaderFooterViewEnity: AnyTableHeaderFooterViewEntity, ViewEntity where Self.View: UITableViewHeaderFooterView { }

extension TableHeaderFooterViewEnity {
    
    static var tableHeaderFooterViewClass: UITableViewHeaderFooterView.Type { self.View.self }
    
    func viewHeight(tableView: UITableView, section: Int) -> CGFloat {
        return 0
    }
}

