//
//  OnlyTitleHeader.swift
//  
//
//  Created by Artem Chursin on 24.04.2021.
//

import UIKit

class OnlyTitleHeader: UITableViewHeaderFooterView, Reusable {
    // MARK: - Private

    private let listSelectore = UITitleHeader()
    
    // MARK: - Public
    func configure(title: NSAttributedString) {
        listSelectore.setData(title: title)
    }
    
    // MARK: - Lifecycle
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        if #available(iOS 14.0, *) {
            self.backgroundConfiguration = UIBackgroundConfiguration.listGroupedHeaderFooter()
            self.backgroundConfiguration?.backgroundColor = Style.Colors.darkBackground
        } else {
            contentView.backgroundColor = Style.Colors.darkBackground
        }
        self.contentView.addSubview(listSelectore)

        listSelectore.snp.makeConstraints {
            $0.top.bottom.left.right.equalToSuperview()
            $0.height.equalTo(24)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }
}


