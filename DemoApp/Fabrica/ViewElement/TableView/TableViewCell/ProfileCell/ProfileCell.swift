//
//  ProfileCell.swift
//  
//
//  Created by Artem Chursin on 24.04.2021.
//

import UIKit

class ProfileCell: UITableViewCell, ContentSizable, Reusable {
    
    var contentHeight: CGFloat {
        140
    }
    
    private lazy var profileEditor: UIProfileEditor = {
        return UIProfileEditor()
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.commonInit()
        self.selectionStyle = .none
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        self.backgroundColor = .clear
        contentView.addSubview(profileEditor)
        profileEditor.becomeFirstResponder()
        profileEditor.snp.makeConstraints {
            $0.top.bottom.left.right.equalToSuperview()
        }
    }
    
    func setup(_ item: ProfileCellEntity) {
        profileEditor.setData(text: item.text,
                              placeholder: item.placeholder,
                              placeholderImage: item.placeholderImage,
                              placeholderImageURL: item.placeholderImageURL,
                              imageUpdater: item.imageUpdater,
                              textReader: item.textReader)
        
    }
}




