//
//  ProfileCellEntity.swift
//  
//
//  Created by Artem Chursin on 24.04.2021.
//

import UIKit

class ProfileCellEntity: TableCellEntity, SizableTableCellEntity {
    static let viewForSize = View()
    
    private(set) var id: String
    private(set) var text: NSAttributedString?
    private(set) var placeholder: NSAttributedString?
    private(set) var placeholderImage: UIImage?
    private(set) var placeholderImageURL: String?
    private(set) var imageUpdater: VoidClosure
    private(set) var textReader: ((String?)->())

    init(id: String,
         text: NSAttributedString?,
         placeholder: NSAttributedString?,
         placeholderImage: UIImage?,
         placeholderImageURL: String?,
         imageUpdater: @escaping VoidClosure,
         textReader: @escaping ((String?)->())) {
        self.id = id
        self.text = text
        self.placeholder = placeholder
        self.placeholderImage = placeholderImage
        self.placeholderImageURL = placeholderImageURL
        self.imageUpdater = imageUpdater
        self.textReader = textReader
    }
    
    func setup(view: ProfileCell) {
        view.setup(self)
    }
}
