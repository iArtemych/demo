//
//  TitleWithRadioCell.swift
//  
//
//  Created by Artem Chursin on 03.04.2021.
//

import UIKit

class TitleWithRadioCell: UITableViewCell, ContentSizable, Reusable {
    
    var contentHeight: CGFloat {
        55
    }
    
    private lazy var titleWithArrow: UITitleWithRadio = {
        return UITitleWithRadio()
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.commonInit()
        self.selectionStyle = .none
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        self.backgroundColor = .clear
        contentView.addSubview(titleWithArrow)
        titleWithArrow.becomeFirstResponder()
        titleWithArrow.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
    }
    
    func setup(_ item: TitleWithRadioCellEntity) {
        titleWithArrow.setData(title: item.cellTitle, isSelect: item.isSelectet)
    }
}


