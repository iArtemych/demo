//
//  TitleWithRadioCellEntity.swift
//  
//
//  Created by Artem Chursin on 03.04.2021.
//

import UIKit

class TitleWithRadioCellEntity: TableCellEntity, SizableTableCellEntity {
    static let viewForSize = View()
    
    private(set) var id: Int
    private(set) var cellTitle: NSAttributedString
    private(set) var isSelectet: Bool

    init(id: Int, cellTitle: NSAttributedString, isSelectet: Bool) {
        self.id = id
        self.cellTitle = cellTitle
        self.isSelectet = isSelectet
    }
    
    func setup(view: TitleWithRadioCell) {
        view.setup(self)
    }
}
