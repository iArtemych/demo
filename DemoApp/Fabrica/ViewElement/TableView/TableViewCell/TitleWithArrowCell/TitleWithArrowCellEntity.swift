//
//  TitleWithArrowCellEntity.swift
//  
//
//  Created by Artem Chursin on 24.04.2021.
//

import UIKit

class TitleWithArrowCellEntity: TableCellEntity, SizableTableCellEntity {
    static let viewForSize = View()
    
    private(set) var id: String
    private(set) var cellTitle: NSAttributedString
    private(set) var needArrow: Bool

    init(id: String, cellTitle: NSAttributedString, needArrow: Bool) {
        self.id = id
        self.cellTitle = cellTitle
        self.needArrow = needArrow
    }
    
    func setup(view: TitleWithArrowCell) {
        view.setup(self)
    }
}
