//
//  TitleWithArrowCell.swift
//  
//
//  Created by Artem Chursin on 24.04.2021.
//

import UIKit

class TitleWithArrowCell: UITableViewCell, ContentSizable, Reusable {
    
    var contentHeight: CGFloat {
        56
    }
    
    private lazy var titleWithArrow: UITitleWithArrow = {
        return UITitleWithArrow()
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.commonInit()
        self.selectionStyle = .gray
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        self.backgroundColor = .clear
        contentView.addSubview(titleWithArrow)
        titleWithArrow.becomeFirstResponder()
        titleWithArrow.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
    }
    
    func setup(_ item: TitleWithArrowCellEntity) {
        titleWithArrow.setData(title: item.cellTitle, needArrow: item.needArrow)
    }
}


