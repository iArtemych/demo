//
//  TitleWithSelectorCell.swift
//  
//
//  Created by Artem Chursin on 24.04.2021.
//

import UIKit

class TitleWithSelectorCell: UITableViewCell, ContentSizable, Reusable {
    
    var contentHeight: CGFloat {
        56
    }
    
    private lazy var titleWithSelector: UITitleWithSelector = {
        return UITitleWithSelector()
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.commonInit()
        self.selectionStyle = .none
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        self.backgroundColor = .clear
        contentView.addSubview(titleWithSelector)
        titleWithSelector.becomeFirstResponder()
        titleWithSelector.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
    }
    
    func setup(_ item: TitleWithSelectorCellEntity) {
        titleWithSelector.setData(title: item.title,
                                  isEnable: item.isEnable,
                                  selectorState: item.selectorState)
        
    }
}



