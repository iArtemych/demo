//
//  TitleWithSelectorCellEntity.swift
//  
//
//  Created by Artem Chursin on 24.04.2021.
//

import UIKit

class TitleWithSelectorCellEntity: TableCellEntity, SizableTableCellEntity {
    static let viewForSize = View()
    
    private(set) var id: String
    private(set) var title: NSAttributedString
    private(set) var isEnable: Bool
    private(set) var selectorState: ((Bool)->())

    init(id: String, title: NSAttributedString, isEnable: Bool, selectorState: @escaping ((Bool)->())) {
        self.id = id
        self.title = title
        self.isEnable = isEnable
        self.selectorState = selectorState
    }
    
    func setup(view: TitleWithSelectorCell) {
        view.setup(self)
    }
}
