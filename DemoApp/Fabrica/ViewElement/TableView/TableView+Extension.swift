//
//  TableView+Extension.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

protocol TableCellEntity: AnyTableCellEntity, ViewEntity where Self.View: UITableViewCell { }

protocol SelectorableCellEntity {
    var title: String! {get}
    var selected: Bool! {get set}
}

extension UITableView {
    func updateHeaderSize() {
        let view = self.tableHeaderView
        view?.frame.size.width = self.frame.width
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        let fittingSize = view?.systemLayoutSizeFitting(CGSize(width: view?.frame.width ?? .zero, height: 0),
                                                       withHorizontalFittingPriority: .required,
                                                       verticalFittingPriority: .defaultLow)
        view?.frame.size.height = fittingSize?.height ?? 0
        self.tableHeaderView = view
    }
}

extension UITableView {
    
    func register(models: [AnyTableEntity.Type]) {
        for model in models {
            if let cell = model as? AnyTableCellEntity.Type {
                self.register(model: cell)
            }
        }
    }
    
    private func register(model: AnyTableCellEntity.Type) {
        self.register(cellType: model.tableCellClass, name: model.entityIdentifier)
    }
    
    func dequeue<T: TableCellEntity>(model: T) -> T.View {
        let cell = self.dequeue(cellType: T.View.self, name: T.entityIdentifier)
        
        model.setup(view: cell)
        
        return cell
    }
    
    func dequeue(any model: AnyTableCellEntity) -> UITableViewCell {
        let cell = self.dequeue(cellType: type(of: model).tableCellClass, name: type(of: model).entityIdentifier)
        
        model.setup(anyView: cell)
        
        return cell
    }
    
    func dequeue<T: UITableViewCell>(any model: AnyTableCellEntity,
                                     `as` expectedType: T.Type) -> T {
        guard let cell = self.dequeue(any: model) as? T else { preconditionFailure() }
        
        return cell
    }
    
    func dequeue<T: TableCellEntity>(model: T, at indexPath: IndexPath) -> T.View {
        let cell = self.dequeue(cellType: T.View.self, name: T.entityIdentifier, indexPath: indexPath)
        
        model.setup(view: cell)
        
        return cell
    }
    
    func dequeue(any model: AnyTableCellEntity, at indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dequeue(cellType: type(of: model).tableCellClass, name: type(of: model).entityIdentifier, indexPath: indexPath)
        
        model.setup(anyView: cell)
        
        return cell
    }
    
    func dequeue<T: UITableViewCell>(any model: AnyTableCellEntity,
                                     at indexPath: IndexPath,
                                     `as` expectedType: T.Type) -> T {
        guard let cell = self.dequeue(any: model, at: indexPath) as? T else { preconditionFailure() }
        
        return cell
    }
    
    func dequeue(any model: AnyTableHeaderFooterViewEntity, section: Int? = nil) -> UITableViewHeaderFooterView {
        let view = self.dequeue(viewType: type(of: model).tableHeaderFooterViewClass, name: type(of: model).entityIdentifier)
        
        if let section = section {
            view.tag = section
        }
        
        model.setup(anyView: view)
        
        return view
    }
    
    func dequeue<T: UITableViewHeaderFooterView>(viewType: T.Type, name viewId: String) -> T {
        self.registerSectionIfNeeded(viewType: viewType, name: viewId)
        
        guard let view = self.dequeueReusableHeaderFooterView(withIdentifier: viewId) as? T else { preconditionFailure() }
        
        return view
    }
    
    private func registerSectionIfNeeded<T: UITableViewHeaderFooterView>(viewType: T.Type, name viewID: String) {
        guard self.dequeueReusableHeaderFooterView(withIdentifier: viewID) == nil else {
            return
        }
        
        self.register(viewType: viewType, name: viewID)
    }
    
    func register<T: UITableViewHeaderFooterView>(viewType: T.Type, name viewId: String) {
        self.register(viewType, forHeaderFooterViewReuseIdentifier: viewId)
    }
    
}

extension UITableView {

    private func registerCellIfNeeded<T: UITableViewCell>(cellType: T.Type, name cellID: String) {
        guard self.dequeueReusableCell(withIdentifier: cellID) == nil else { return }
        
        self.register(cellType: cellType, name: cellID)
    }
    
    func register<T: UITableViewCell>(cellType: T.Type,
                                      name cellId: String) {
        self.register(cellType, forCellReuseIdentifier: cellId)
    }
    
    func dequeue<T: UITableViewCell>(cellType: T.Type,
                                     name cellId: String) -> T {
        self.registerCellIfNeeded(cellType: cellType, name: cellId)
        
        guard let cell = self.dequeueReusableCell(withIdentifier: cellId) as? T else { preconditionFailure() }
        
        return cell
    }
    
    func dequeue<T: UITableViewCell>(cellType: T.Type,
                                     name cellId: String,
                                     indexPath: IndexPath) -> T {
        self.registerCellIfNeeded(cellType: cellType, name: cellId)
        
        guard let cell = self.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? T else { preconditionFailure() }
        
        return cell
    }
    
}

