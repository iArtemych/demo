//
//  TableViewSection.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

protocol TableViewSectionProtocol {
    func header() -> AnyTableHeaderFooterViewEntity?
    func rows() -> [AnyTableCellEntity]
}

struct TableViewSection: TableViewSectionProtocol {
    var headerModel: AnyTableHeaderFooterViewEntity?
    var rowsModel: [AnyTableCellEntity] = []
    
    init(header: AnyTableHeaderFooterViewEntity? = nil,
         rows: [AnyTableCellEntity] = []) {
        self.headerModel = header
        self.rowsModel = rows
    }
    
    func row<T: AnyTableCellEntity>(at index: Int, as type: T) -> T? {
        guard let value = self.rowsModel[safe: index] as? T else { return nil }
        
        return value
    }
}

extension TableViewSection {
    func header() -> AnyTableHeaderFooterViewEntity? {
        return self.headerModel
    }
    
    func rows() -> [AnyTableCellEntity] {
        return self.rowsModel
    }
}

struct EmptyHeaderFooter: TableHeaderFooterViewEnity {
    
    var height: CGFloat
    
    init(height: CGFloat = 40) {
        self.height = height
    }
    
    func setup(view: UITableViewHeaderFooterView) {
        view.tintColor = .white
    }
    
    func viewHeight(tableView: UITableView, section: Int) -> CGFloat {
        return self.height
    }
}

