//
//  TableView.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

class TableView: EntityTableView {
    
}

class EntityTableView: UITableView {
    typealias TableRowSelection = (table: EntityTableView, indexPath: IndexPath, model: AnyTableCellEntity)
    typealias Action = (TableRowSelection) -> Void
    
    public var animateCellsShowing = false
    
    public private(set) var sections: [TableViewSectionProtocol] = []
    
    private var scrollViewIsDragging = false
    private var shouldUpdateOnDraggingEnd = false
    
    var didSelect: Action?
    
    public convenience init(style: UITableView.Style = .plain) {
        self.init(frame: .zero, style: style)
    }
    
    public override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        self.commonInit()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.commonInit()
    }
    
    private func commonInit() {
        self.separatorStyle = .none
        self.showsVerticalScrollIndicator = false
        
        self.delegate = self
        self.dataSource = self
        
        self.backgroundColor = .clear
    }
    
    func update(sections: [TableViewSectionProtocol]) {
        self.sections = sections
        self.reloadData()
    }
    
    func updateSection(sectionNum: Int, sections: [TableViewSectionProtocol]) {
        self.beginUpdates()
        self.sections = sections
        self.reloadSections([sectionNum], animationStyle: .none)
        self.endUpdates()
    }
}

extension EntityTableView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView,
                        numberOfRowsInSection section: Int) -> Int {
        guard let section = self.sections[safe: section] else { return 0 }
        
        return section.rows().count
    }
    
    func tableView(_ tableView: UITableView,
                        heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let section = self.sections[safe: indexPath.section] else { return 0 }
        
        let row = section.rows()[indexPath.row]
        
        let height = row.cellHeight(tableView: tableView, indexPath: indexPath)
        
        return max(height, 0)
    }
    
    func tableView(_ tableView: UITableView,
                        cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = self.sections[safe: indexPath.section] else { preconditionFailure()  }
        let row = section.rows()[indexPath.row]
        
        let cell = tableView.dequeue(any: row, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                        didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        guard let section = self.sections[safe: indexPath.section] else { return }
        let row = section.rows()[indexPath.row]
        
        self.didSelect?((self, indexPath, row))
    }
    
    func tableView(_ tableView: UITableView,
                        heightForHeaderInSection sectionIndex: Int) -> CGFloat {
        guard self.sections.count > sectionIndex,
            let section = self.sections[safe: sectionIndex],
            let header = section.header() else { return .leastNormalMagnitude }
        
        let height = header.viewHeight(tableView: tableView, section: sectionIndex)
        
        return max(height, 0)
    }
    
    func tableView(_ tableView: UITableView,
                        viewForHeaderInSection index: Int) -> UIView? {
        guard self.sections.count > index,
            let section = self.sections[safe: index],
            let header = section.header() else { return nil }
        
        let view = tableView.dequeue(any: header, section: index)
                
        view.tag = index
        
        return view
    }
}

