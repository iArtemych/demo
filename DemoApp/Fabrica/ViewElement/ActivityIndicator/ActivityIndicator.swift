//
//  ActivityIndicator.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

final public class ActivityIndicator: UIView {
    
    private let spinner = UIActivityIndicatorView(style: .white)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        self.isHidden = false
        
        spinner.color = Style.Colors.darkBackground
            
        self.addSubview(spinner)
        self.spinner.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
}

// MARK: - ActivityIndicatorView

extension ActivityIndicator: ActivityIndicatorView {
    public func start() {
        self.spinner.startAnimating()
        self.isHidden = false
    }

    public func stop() {
        self.spinner.stopAnimating()
        self.isHidden = true
    }
}

