//
//  ActivityIndicatorView.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

protocol ActivityIndicatorView: UIView {
    func start()
    func stop()
}
