//
//  UIView+ActivityIndicator.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

extension UIView {
    static let activityTag = 1001
    
    func presentActivityIndicator(_ isShowing: Bool) {
        if isShowing {
            showActivityIndicator()
        } else {
            hideActivityIndicator()
        }
    }

    /// Shows activity indicator
    ///
    /// Activity indicator is positioned in the center of reciever or in the center of `referenceView` or `referenceLayoutGuide` if provided
    func showActivityIndicator(
        _ activity: ActivityIndicatorView = ActivityIndicator(),
        referenceView: UIView? = nil,
        referenceLayoutGuide: UILayoutGuide? = nil,
        backgroundColor: UIColor = Style.Colors.white.withAlphaComponent(0.5)) {

        guard self.viewInSubviews(tag: Self.activityTag) == nil else { return }
        
        let backgroundView = UIView()
        
        backgroundView.backgroundColor = backgroundColor
        backgroundView.tag = Self.activityTag
        
        self.addSubview(backgroundView)
        backgroundView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        backgroundView.addSubview(activity)

        if let referenceView = referenceView,
            referenceView.isDescendant(of: self) {
            activity.snp.makeConstraints {
                $0.center.equalTo(referenceView)
            }

        } else if let referenceLayoutGuide = referenceLayoutGuide {
            activity.snp.makeConstraints {
                $0.center.equalTo(referenceLayoutGuide)
            }

        } else {
            activity.snp.makeConstraints {
                $0.center.equalTo(backgroundView)
            }
        }
        
        activity.start()
    }
    
    func hideActivityIndicator() {
        self.viewInSubviews(tag: Self.activityTag)?.removeFromSuperview()
    }
    
    func viewInSubviews(tag: Int) -> UIView? {
        for view in self.subviews as [UIView] where view.tag == tag {
            return view
        }
        
        return nil
    }
}
