//
//  UICustomTextInput.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation
import InputMask
import RxSwift
//import RxCocoa

enum StatusTextField {
    case active
    case notActive
    case error
}

class UICustomTextInput: UIView {

    typealias TextChangedCallback = (_ textField: UITextField, _ value: String, _ complete: Bool) -> Void

    // MARK: - Public

    let textField = UITextField(frame: CGRect.zero)
    let viewWithBorder = UIView()
    private(set) var maskedInputDelegate: MaskedTextFieldDelegate?

    func configure(text: String,
                   placeholder: String,
                   isEnabled: Bool = true,
                   isSecureTextEntry: Bool = false,
                   keyboardType: UIKeyboardType = .default,
                   mask: String? = nil,
                   primaryFormat: String? = nil,
                   
                   leftIcon: UIImage? = nil,
                   needVisibilityIcon: Bool = false,
                   inputMaskFormat: (primary: String, affine: [String])? = nil,
                   onMaskedTextChangedCallback: TextChangedCallback?) {

        textField.isUserInteractionEnabled = isEnabled
        textField.isEnabled = isEnabled
        textField.isSecureTextEntry = isSecureTextEntry

        textField.text = text
        textField.attributedPlaceholder = NSAttributedString(
            string: placeholder,
            attributes: [NSAttributedString.Key.foregroundColor: Style.Colors.primaryRed])

        textField.keyboardType = keyboardType
        switch keyboardType {
        case .default:
            textField.autocapitalizationType = .sentences
            textField.autocorrectionType = .default
        default:
            textField.autocapitalizationType = .none
            textField.autocorrectionType = .no
        }

        // Left icon
        textField.leftViewMode = leftIcon != nil ? .always: .never
        let imageView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.contentMode = .scaleAspectFit
        imageView.image = leftIcon
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
        paddingView.addSubview(imageView)
        textField.leftView = paddingView

        // Visibility button
        textField.rightView = visibilityButton
        textField.rightViewMode = needVisibilityIcon ? .always : .never
        visibilityButton.addTarget(self, action: #selector(visibilityButtonTap), for: .touchUpInside)


        // Setup listeners
        textField.addTarget(self, action: #selector(UICustomTextInput.textFieldDidChange(_:)), for: UIControl.Event.allEditingEvents)
        textField.addTarget(self, action: #selector(UICustomTextInput.textFieldDidEnd(_:)), for: UIControl.Event.editingDidEnd)
        textField.addTarget(self, action: #selector(UICustomTextInput.textFieldDidBegin(_:)), for: UIControl.Event.editingDidBegin)

        if let inputMaskFormat = inputMaskFormat {
            maskedInputDelegate = MaskedTextFieldDelegate(
                primaryFormat: inputMaskFormat.primary,
                autocomplete: false,
                affineFormats: inputMaskFormat.affine,
                affinityCalculationStrategy: .wholeString)
            maskedInputDelegate?.onMaskedTextChangedCallback = onMaskedTextChangedCallback
            textField.delegate = maskedInputDelegate
        } else {
            maskedInputDelegate = nil
            self.onMaskedTextChangedCallback = onMaskedTextChangedCallback
            textField.delegate = self
        }

    }
    
    func addRighticon(rightIcon: UIImage?) {
        // Right icon
        textField.rightViewMode = .always

        let rightIimageView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        rightIimageView.contentMode = .scaleAspectFit
        rightIimageView.image = rightIcon
        let rightPaddingView = UIView(frame: CGRect(x: 10, y: 0, width: 30, height: 20))
        rightPaddingView.addSubview(rightIimageView)
        textField.rightView = rightIimageView
    }

    func setActiveType(type: StatusTextField) {
//        switch type {
//        case .active:
//            viewWithBorder.layer.borderColor = Style.Colors.iconGrey.cgColor
//            viewWithBorder.layer.borderWidth = 1
//        case .notActive:
//            viewWithBorder.layer.borderColor = Style.Colors.iconGrey.cgColor
//            viewWithBorder.layer.borderWidth = 1
//        case .error:
//            viewWithBorder.layer.borderColor = Style.Colors.primaryRed.cgColor
//            viewWithBorder.layer.borderWidth = 1
//        }
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        createViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.viewWithBorder.layer.cornerRadius =
            min(viewWithBorder.bounds.width, viewWithBorder.bounds.height) / 2
    }

    private func createViews() {
        addSubview(viewWithBorder)
//        viewWithBorder.setBorder(radius: viewWithBorder.bounds.height/2, color: Style.Colors.iconGrey, borderWidth: 1)
        viewWithBorder.backgroundColor = Style.Colors.white
        viewWithBorder.snp.remakeConstraints { make in
            make.edges.equalToSuperview()
            make.height.equalTo(Self.defaultHeight).priority(100)
        }

        textField.font = Style.Font.titleRegularCustom(value: 32).font
        textField.borderStyle = UITextField.BorderStyle.none
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.keyboardType = UIKeyboardType.default
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.textColor = Style.Colors.primaryRed
        textField.tintColor = .clear
        textField.returnKeyType = UIReturnKeyType.done
        textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        textField.textAlignment = .center
        viewWithBorder.addSubview(textField)
        
        textField.snp.remakeConstraints { make in
            make.top.equalTo(12)
            make.bottom.equalTo(-12)
            make.left.equalTo(16)
            make.right.equalTo(-16)
        }
    }

    // MARK: - Private

    private static let defaultHeight: CGFloat = 51

    private let bag = DisposeBag()
    private let visibilityButton = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))

    private var onMaskedTextChangedCallback: ((_ textField: UITextField, _ value: String, _ complete: Bool) -> Void)?

    @objc
    private func visibilityButtonTap() {
        visibilityButton.isSelected = !visibilityButton.isSelected
        textField.isSecureTextEntry = !textField.isSecureTextEntry
    }
}

// MARK: - UITextFieldDelegate
extension UICustomTextInput: UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        onMaskedTextChangedCallback?(textField, textField.text ?? "", false)
    }
    @objc func textFieldDidEnd(_ textField: UITextField) {
        setActiveType(type: .notActive)
    }
    @objc func textFieldDidBegin(_ textField: UITextField) {
        setActiveType(type: .active)
    }
}

// MARK: - MaskedTextFieldDelegateListener

extension UICustomTextInput: MaskedTextFieldDelegateListener {
    open func textField(
        _ textField: UITextField,
        didFillMandatoryCharacters complete: Bool,
        didExtractValue value: String) {

    }
}

