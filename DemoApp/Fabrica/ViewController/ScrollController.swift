//
//  ScrollController.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

class ScrollController <ScrollView: UIScrollView>: ViewController {
    
    private var buttons = [DefaultButton.Model]()
    
    public private(set) lazy var scrollView: ScrollView = {
        return self.setupScrollView()
    }()
    
    open func setupScrollView() -> ScrollView {
        return ScrollView()
    }
    
    private lazy var buttonsStackView: UIStackView = {
        let view = UIStackView()
        
        view.axis = .vertical
        view.spacing = 16
        
        return view
    }()
    
    private lazy var buttonsView: UIView = {
        let view = UIView()
        
        view.backgroundColor = .clear
        
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
                
        view.insertSubview(blurEffectView, at: 0)
        blurEffectView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        view.addSubview(self.buttonsStackView)
        self.buttonsStackView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(16)
            $0.height.equalTo(54)
            $0.bottom.equalTo(self.view.safeAreaInsets).offset(-58)
            $0.left.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-16)
        }
        
        view.isHidden = true
        
        return view
    }()
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    private func setupViews() {
        view.addSubview(self.scrollView)
        self.scrollView.snp.makeConstraints {
            if let _ = self.tabBarController?.tabBar.frame.height {
                $0.top.left.right.equalToSuperview()
                $0.bottom.equalToSuperview().offset(-70)
            } else {
                $0.edges.equalTo(view.safeAreaInsets)
            }
        }
        
        view.addSubview(self.buttonsView)
        self.buttonsView.snp.makeConstraints {
            if let _ = self.tabBarController?.tabBar.frame.height {
                $0.left.right.equalToSuperview()
                $0.bottom.equalToSuperview().offset(-70)
            } else {
                $0.left.right.bottom.equalToSuperview()
            }
        }
    }
    
    public func setup(buttons: [DefaultButton.Model?]) {
        self.buttons = buttons.compactMap { $0 }
//        print(self.tabBarController?.tabBar.frame.height)
        scrollView.contentInset.bottom = !self.buttons.isEmpty ? 10 : 0
        buttonsView.isHidden = self.buttons.isEmpty
        
        buttonsStackView.arrangedSubviews.forEach {
            $0.removeFromSuperview()
        }
        
        for (index, button) in self.buttons.enumerated() {
            let view = button.button(index: index)
            buttonsStackView.addArrangedSubview(view)
        }
        
        buttonsView.layoutIfNeeded()
        buttonsView.setNeedsLayout()
        
        self.scrollView.contentInset.bottom = buttonsView.frame.height
    }
    
    public func button(for index: Int = 0) -> DefaultButton? {
        return buttonsStackView.arrangedSubviews[safe: index] as? DefaultButton
    }
}


