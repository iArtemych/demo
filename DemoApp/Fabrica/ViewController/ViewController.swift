//
//  ViewController.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit
import RxSwift
import SwiftMessages

class ViewController: UIViewController, BaseView, LoaderPresentable {
    var loaderView: UILoaderView!
    var keyboardSize: CGFloat?
    var backActionEnable = true
    
    var rightItems: [BarButtonItem] = [] {
        didSet {
            let selector = #selector(rightButtonAction)
            
            self.navigationItem.rightBarButtonItems = self.rightItems
                .enumerated()
                .compactMap { index, item in
                    let button = item.button(target: self, selector: selector)
                    
                    button?.tag = index
                    
                    return button
            }
        }
    }
    
    var leftItems: [BarButtonItem] = [] {
        didSet {
            let selector = #selector(leftButtonAction)
            
            self.navigationItem.leftBarButtonItems = self.leftItems
                .enumerated()
                .compactMap { index, item in
                    let button = item.button(target: self, selector: selector)
                    
                    button?.tag = index
                    
                    return button
            }
        }
    }
    
    var subNavigationView: UIView!
    private let bag = DisposeBag()
    private var subNavigationTitle: UILabel!
    private var backButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        loaderView = UILoaderView()
        self.view.backgroundColor = .white
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.setupNavigationBar()
    }
    
    func addNavBarImage() {
        let image = R.image.logo()
        let imageView = UIImageView(image: image)
        
        imageView.contentMode = .scaleAspectFill
        navigationItem.titleView = imageView
//        navigationItem.titleView?.snp.removeConstraints()
//        navigationItem.titleView?.snp.remakeConstraints{
//            $0.height.equalTo(24)
//            $0.width.equalTo(90)
//        }
    }
    
    private func setupNavigationBar() {
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = Style.Colors.darkBackground
        self.navigationController?.navigationBar.tintColor = Style.Colors.darkBackground
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
//        self.navigationController?.navigationBar.layer.shadowColor = Style.Colors.textGrey.cgColor
//        self.navigationController?.navigationBar.layer.shadowOpacity = 0.1
//        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 10)
//        self.navigationController?.navigationBar.layer.shadowRadius = 10
                
        self.navigationController?.navigationBar.titleTextAttributes = StringAttributes().font(Style.Font.titleBoldCustom(value: 17).font)
    }
    
    @objc
    private func leftButtonAction(_ sender: UIBarButtonItem) {
        guard let item = self.leftItems[safe: sender.tag] else { return }
        
        item.action?(sender)
    }
    
    @objc
    private func rightButtonAction(_ sender: UIBarButtonItem) {
        guard let item = self.rightItems[safe: sender.tag] else { return }

        item.action?(sender)
    }
}

extension ViewController {
    
    typealias Action = (UIBarButtonItem) -> Void

    enum Kind {
        case text(String)
        case image(UIImage)
    }
    
    struct BarButtonItem {

        private(set) var kind: Kind
        private(set) var action: Action?
        
        init(kind: Kind, action: Action? = nil) {
            self.kind = kind
            self.action = action
        }
        
        func button(target: Any?, selector: Selector) -> UIBarButtonItem? {
            switch self.kind {
            case .text(let text):
                return UIBarButtonItem(title: text, style: .plain, target: target, action: selector)
            case .image(let image):
                return UIBarButtonItem(image: image, style: .plain, target: target, action: selector)
            }
        }
        
        static func text(_ string: String, action: @escaping Action) -> BarButtonItem {
            return BarButtonItem(kind: .text(string), action: action)
        }

        static func image(_ image: UIImage, action: @escaping Action) -> BarButtonItem {
            return BarButtonItem(kind: .image(image), action: action)
        }
    }
}

extension ViewController {
    func showAlert(type: Theme, titleText: String?, subtitle: String) {
        let view = MessageView.viewFromNib(layout: .cardView)
        
        view.configureTheme(.error)
        
        if type == .error {
            (view.backgroundView as? CornerRoundingView)?.backgroundColor = #colorLiteral(red: 0.5176470588, green: 0.01568627451, blue: 0.02745098039, alpha: 1)
        }
        
        view.configureDropShadow()
        
        if titleText == nil {
            view.titleLabel?.isHidden = true
        }
        view.button?.isHidden = true
        view.iconLabel?.isHidden = true
        
        view.configureContent(title: titleText ?? "",
                              body: subtitle,
                              iconText: "")
        
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 10, right: 20)
        
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        view.titleLabel?.numberOfLines = 1
        view.titleLabel?.font = Style.Font.titleSemiboldCustom(value: 16).font
        view.titleLabel?.textColor = Style.Colors.textWhite
        
        view.bodyLabel?.numberOfLines = 2
        view.bodyLabel?.font = Style.Font.titleLightCustom(value: 14).font
        view.bodyLabel?.textColor = Style.Colors.textWhite
        
        
        SwiftMessages.show(view: view)
    }
}

extension ViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return backActionEnable
    }
}
