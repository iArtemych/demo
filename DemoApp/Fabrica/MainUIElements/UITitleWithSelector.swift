//
//  UITitleWithSelector.swift
//  
//
//  Created by Artem Chursin on 24.04.2021.
//

import UIKit
import SnapKit
import RxSwift

class UITitleWithSelector: UIView {
    private let bag = DisposeBag()
    private lazy var titleLabel: UILabel = {
        var view = UILabel()
        view.font = Style.Font.titleRegularCustom(value: 17).font
        return view
    }()
    
    private lazy var selector: UISwitch = {
        let view = UISwitch()
        return view
    }()
    
    private var selectorState: ((Bool)->())?
    
    init() {
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
       commonInit()
   }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        commonInit()
    }
    
    func setData(title: NSAttributedString, isEnable: Bool, selectorState: ((Bool)->())) {
        self.titleLabel.attributedText = title
        self.selector.isOn = isEnable
        selector.addTarget(self, action: #selector(switchChanged), for: .valueChanged)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func commonInit() {
        
        addSubview(titleLabel)
        addSubview(selector)
        
        selector.thumbTintColor = Style.Colors.textWhite
        selector.onTintColor = Style.Colors.secondaryRed
        
        selector.snp.makeConstraints {
            $0.centerY.equalTo(self.snp.centerY)
            $0.right.equalTo(-16)
        }
        
        titleLabel.snp.remakeConstraints {
            $0.centerY.equalToSuperview()
            $0.left.equalTo(16)
            $0.right.lessThanOrEqualTo(selector.snp.left).offset(8)
        }
    }
    
    @objc
    private func switchChanged(mySwitch: UISwitch) {
        selectorState?(mySwitch.isOn)
    }
}



