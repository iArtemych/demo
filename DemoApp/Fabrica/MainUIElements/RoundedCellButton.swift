//
//  RoundedCellButton.swift
//  
//
//  Created by Artem Chursin on 08.04.2021.
//

import UIKit
import RxSwift

final class RoundedCellButton: UIView {
    
    private let bag = DisposeBag()
    lazy var imagePhoto = UIImageView()
    lazy var titlePhoto = UILabel()
    
    private var action: VoidClosure?
    
    init() {
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
       commonInit()
   }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        commonInit()
    }
    
    func setData(title: NSAttributedString, image: UIImage?, action: VoidClosure?) {
        titlePhoto.attributedText = title
        imagePhoto.image = image
        self.action = action
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func commonInit() {
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1
        self.layer.borderColor = Style.Colors.primaryRed.cgColor
        self.layer.cornerRadius = 12
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        self.addGestureRecognizer(tap)
        
        addSubview(imagePhoto)
        addSubview(titlePhoto)
        
        titlePhoto.textAlignment = .center
        
        imagePhoto.snp.makeConstraints {
            $0.top.equalToSuperview().offset(32)
            $0.width.height.equalTo(26)
            $0.centerX.equalToSuperview()
        }
        
        titlePhoto.snp.makeConstraints {
            $0.top.equalTo(imagePhoto.snp.bottom).offset(12)
            $0.left.right.equalToSuperview()
        }
    }
    
    @objc
    private func tapAction() {
        action?()
    }
}

