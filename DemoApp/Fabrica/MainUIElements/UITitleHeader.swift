//
//  UITitleHeader.swift
//  
//
//  Created by Artem Chursin on 24.04.2021.
//

import UIKit
import RxSwift

class UITitleHeader: UIView {
    private lazy var titleLabel: UILabel = {
        var view = UILabel()
        view.font = Style.Font.titleSemiboldCustom(value: 16).font
        return view
    }()
    
    init() {
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
       commonInit()
   }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        commonInit()
    }
    
    func setData(title: NSAttributedString) {
        self.titleLabel.attributedText = title
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit() {
        self.addSubview(titleLabel)
        titleLabel.snp.makeConstraints {
            $0.top.right.bottom.equalToSuperview()
            $0.left.equalToSuperview().offset(16)
        }
    }
}

