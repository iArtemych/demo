//
//  CustomAlert.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

class CustomAlert: UIView {
    
    private var shadowLayer: CAShapeLayer!
    private var title: String
    private var message: String
    private var okButton: String?
    private var closeText: String
    private var action: VoidClosure?
    var closeAction: VoidClosure?
    var vcCloseAction: VoidClosure?
    
    private var alertMessage: UILabel!
    private var alertTitle: UILabel!
    
    private var alertClose: UIButton!
    private var alertAction: UIButton!
    
    init(withWarning title: String,
         message: String,
         closeText: String,
         vcCloseAction: VoidClosure? = nil) {
        self.title = title
        self.message = message
        self.closeText = closeText
        self.vcCloseAction = vcCloseAction
        super.init(frame: .zero)
        setupWarningAlert()
    }
    
    init(withNetworkWarning message: String,
         vcCloseAction: VoidClosure? = nil) {
        self.title = "Netwotk error"
        self.message = message
        self.closeText = "ХОРОШО"
        self.vcCloseAction = vcCloseAction
        super.init(frame: .zero)
        setupWarningAlert()
    }
    
    init(title: String,
         message: String,
         closeText: String,
         okButton: String,
         withAction action: @escaping VoidClosure) {
        self.title = title
        self.message = message
        self.closeText = closeText
        self.okButton = okButton
        self.action = action
        super.init(frame: .zero)
        setupActionsAlert()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 15).cgPath

            shadowLayer.shadowColor = Style.Colors.secondaryRed.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0, height: 10)
            shadowLayer.shadowOpacity = 0.3
            shadowLayer.shadowRadius = 10
            shadowLayer.fillColor = Style.Colors.darkBackground.cgColor
            layer.insertSublayer(shadowLayer, at: 0)
        }
        
        self.layer.cornerRadius = 15
        self.layer.borderWidth = 1
        self.layer.borderColor = Style.Colors.primaryRed.cgColor
    }
    
    private func setupAlert() {
        
        alertTitle = UILabel()
        alertTitle.text = title
        alertTitle.font = Style.Font.titleSemiboldCustom(value: 14).font
        alertTitle.textColor = Style.Colors.textWhite
        alertTitle.textAlignment = .center
        alertTitle.numberOfLines = 2
        
        alertMessage = UILabel()
        alertMessage.text = message
        alertMessage.font = Style.Font.titleLightCustom(value: 14).font
        alertMessage.textColor = Style.Colors.textWhite
        alertMessage.textAlignment = .center
        alertMessage.numberOfLines = 0
        
        self.addSubview(alertMessage)
        self.addSubview(alertTitle)
        
        alertTitle.snp.makeConstraints {
            $0.top.equalToSuperview().offset(16)
            $0.left.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-16)
        }
        
        alertMessage.snp.makeConstraints {
            $0.top.equalTo(alertTitle.snp.bottom).offset(16)
            $0.bottom.equalToSuperview().offset(-52)
            $0.left.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-16)
        }
    }
    
    private func setupWarningAlert() {
        setupAlert()

        alertClose = UIButton(type: .system)
        alertClose.setTitle(closeText, for: [])
        alertClose.titleLabel?.font = Style.Font.titleBoldCustom(value: 18).font
        alertClose.tintColor = Style.Colors.secondaryRed
        alertClose.addTarget(self, action: #selector(closeButtonTap), for: .touchUpInside)

        self.addSubview(alertClose)

        alertClose.snp.makeConstraints {
            $0.bottom.left.right.equalToSuperview()
            $0.height.equalTo(44)
        }
    }
    
    private func setupActionsAlert() {
        setupAlert()
        
        alertClose = UIButton(type: .system)
        alertClose.setTitle(closeText, for: [])
        alertClose.titleLabel?.font = Style.Font.titleRegularCustom(value: 13).font
        alertClose.tintColor = Style.Colors.primaryRed
        alertClose.addTarget(self, action: #selector(closeButtonTap), for: .touchUpInside)
        
        alertAction = UIButton(type: .system)
        alertAction.setTitle(okButton, for: [])
        alertAction.titleLabel?.font = Style.Font.titleRegularCustom(value: 13).font
        alertAction.tintColor = Style.Colors.secondaryRed
        alertAction.addTarget(self, action: #selector(actionButtonTap), for: .touchUpInside)
        
        self.addSubview(alertClose)
        self.addSubview(alertAction)
            
        alertClose.snp.makeConstraints { (make) in
            make.bottom.left.equalToSuperview()
            make.width.equalTo(135)
            make.height.equalTo(44)
        }
        
        alertAction.snp.makeConstraints { (make) in
            make.bottom.right.equalToSuperview()
            make.width.equalTo(135)
            make.height.equalTo(44)
        }
    }
    
    @objc func closeButtonTap() {
        vcCloseAction?()
        closeAction?()
    }
    
    @objc func actionButtonTap() {
        action?()
        closeAction?()
    }
}


