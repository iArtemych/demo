//
//  UITextInput.swift
//  
//
//  Created by Artem Chursin on 27.03.2021.
//

import UIKit
import RxSwift

final class UITextInput: UIView {
    
    private let bag = DisposeBag()
    lazy var textField: UITextField = {
        var view = UITextField()
        view = UITextField()
        view.textColor = Style.Colors.textWhite
        view.borderStyle = .none
        view.tintColor = Style.Colors.primaryRed
        view.keyboardAppearance = .dark
        return view
    }()
    private lazy var underline = UIView()
    
    var textReader: ((String?)->())?
    var doneAction: VoidClosure?
    
    init() {
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
       commonInit()
   }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        commonInit()
    }
    
    func setData(title: NSAttributedString?,
                 placeholder: NSAttributedString?,
                 contentType: UITextContentType,
                 textReader: ((String?)->())?,
                 doneAction: VoidClosure?) {
        self.textReader = textReader
        self.doneAction = doneAction
        textField.textContentType = contentType
        textField.attributedPlaceholder = placeholder
        switch contentType {
        case .emailAddress:
            textField.keyboardType = .emailAddress
            textField.autocapitalizationType = .none
        default:
            break
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        
        textField.delegate = self
        textField.addTarget(self, action: #selector(textUpdate), for: .allEditingEvents)
        underline.backgroundColor = Style.Colors.secondaryRed
        
        addSubview(textField)
        addSubview(underline)
        
        textField.snp.makeConstraints {
            $0.right.left.equalToSuperview()
            $0.height.equalTo(48)
            $0.centerY.equalTo(self.snp.centerY)
        }
        
        underline.snp.remakeConstraints {
            $0.top.equalTo(textField.snp.bottom).offset(1)
            $0.left.equalTo(textField.snp.left)
            $0.right.equalTo(textField.snp.right)
            $0.height.equalTo(1)
        }
    }
    
    @objc
    private func textUpdate() {
        textReader?(textField.text)
    }
}

extension UITextInput: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        underline.backgroundColor = Style.Colors.primaryRed
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        underline.backgroundColor = Style.Colors.secondaryRed
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        performAction()
        return false
    }
    
    private func performAction() {
        doneAction?()
    }
}
