//
//  UIAdvertismentView.swift
//  
//
//  Created by Artem Chursin on 17.04.2021.
//

import UIKit
import RxSwift
import PINRemoteImage

final class UIAdvertismentView: UIView {
    
    enum LeftStatusMode {
        case green, red
    }
    
    private let bag = DisposeBag()
    lazy var imagePhoto = UIImageView()
    lazy var titlePhoto = UILabel()
    
    lazy var stateView = UIView()
    lazy var stateLabel = UILabel()
    lazy var rightStateImage = UIImageView()
    
    lazy var rightButton = UIButton()
    
    lazy var gradientView = UIView()
    lazy var titleLabel = UILabel()
    lazy var descriptionLabel = UILabel()
    
    private var leftStatusMode: LeftStatusMode = .green
    private var rightButtonAction: StringClosure?
    private var statusAction: VoidClosure?
    
    var gradientLayer: CAGradientLayer! {
        didSet {
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0, y: 1)
            gradientLayer.colors = [UIColor.clear,
                                    Style.Colors.darkBackground.withAlphaComponent(0.7).cgColor,
                                    Style.Colors.darkBackground.withAlphaComponent(0.9).cgColor]
        }
    }
    
    init() {
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
       commonInit()
   }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        commonInit()
    }
    
    func setData(title: NSAttributedString?,
                 description: NSAttributedString?,
                 leftStatusMode: LeftStatusMode,
                 leftStatusText: NSAttributedString?,
                 leftStatusImage: UIImage?,
                 rightStatusImage: UIImage?,
                 image: String?,
                 rightButtonAction: StringClosure?,
                 statusAction: VoidClosure?) {
        
        if let urlString = image,
           let url = URL(string: urlString) {
            print("url: \(url)")
            imagePhoto.pin_setImage(from: url)
        }
        
        self.titleLabel.attributedText = title
        self.descriptionLabel.attributedText = description
        self.rightButtonAction = rightButtonAction
        self.leftStatusMode = leftStatusMode
        self.rightStateImage.isHidden = rightStatusImage == nil
        self.rightStateImage.image = rightStatusImage
        self.stateLabel.attributedText = leftStatusText
        self.statusAction = rightStatusImage != nil ? statusAction : nil
        setupStatusView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        gradientLayer.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: 120)
    }

    func commonInit() {
        gradientLayer = CAGradientLayer()
        
        imagePhoto.layer.masksToBounds = true
        imagePhoto.layer.cornerRadius = 16
        imagePhoto.contentMode = .scaleAspectFill
        self.addSubview(imagePhoto)
        gradientView.layer.addSublayer(gradientLayer)
        
        
        rightButton.setImage(R.image.edit_small(), for: [])
        rightButton.backgroundColor = Style.Colors.darkBackground.withAlphaComponent(0.5)
        rightButton.layer.cornerRadius = 12
        rightButton.layer.masksToBounds = true
        rightButton.rx.tap
            .subscribe { [weak self] (_) in
                self?.rightButtonAction?("")
            }
            .disposed(by: bag)
        self.addSubview(rightButton)
        
        stateView.layer.cornerRadius = 12
        self.addSubview(stateView)
        
        let stateTap = UITapGestureRecognizer()
        stateView.addGestureRecognizer(stateTap)
        stateTap.rx.event.bind { [weak self] _ in
            self?.statusAction?()
        }
        .disposed(by: bag)
        
        stateView.addSubview(stateLabel)
        stateView.addSubview(rightStateImage)
        
        self.addSubview(titleLabel)
        
        descriptionLabel.numberOfLines = 2
        self.addSubview(descriptionLabel)
        
        imagePhoto.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        
        imagePhoto.addSubview(gradientView)
        gradientView.snp.makeConstraints {
            $0.left.right.bottom.equalToSuperview()
            $0.height.equalTo(120)
        }
        
        titleLabel.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-63)
            $0.left.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-16)
        }
        
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(4)
            $0.left.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-16)
        }
        
        rightButton.snp.makeConstraints {
            $0.top.equalTo(imagePhoto.snp.top).offset(20)
            $0.right.equalTo(imagePhoto.snp.right).offset(-20)
            $0.width.height.equalTo(24)
        }
        
        stateView.snp.makeConstraints {
            $0.top.equalTo(imagePhoto.snp.top).offset(20)
            $0.left.equalTo(imagePhoto.snp.left).offset(20)
            $0.height.equalTo(24)
        }
        
        stateLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(12)
            $0.top.bottom.equalToSuperview()
        }
        
        rightStateImage.snp.makeConstraints {
            $0.left.equalTo(stateLabel.snp.right).offset(4)
            $0.right.equalToSuperview().offset(-12)
            $0.centerY.equalToSuperview()
        }
    }
    
    private func setupStatusView() {
        switch leftStatusMode {
        case .green:
            stateView.backgroundColor = Style.Colors.greenState
        case .red:
            stateView.backgroundColor = Style.Colors.secondaryRed
        }
    }
}


