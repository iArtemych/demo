//
//  TabBarItem.swift
//  SMPAssist
//
//  Created by Артем Чурсин on 18.02.2021.
//

import UIKit
import SnapKit

class TabBarItem: UIView {
    
    var isActive = false {
        didSet {
            if isActive {
                self.activateItem()
            } else {
                self.deactivateItem()
            }
        }
    }
    
    private let flightView = UIShadowView(cornerRadius: 27, showShadow: false)
    private let itemTitleLabel = UILabel(frame: CGRect.zero)
    private let itemIconView = UIImageView(frame: CGRect.zero)
    private var pointView = UIView()
    
    private var image: UIImage?
    private var name: String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    convenience init(name: String, image: UIImage?, frame: CGRect) {
        self.init(frame: frame)
        
        self.image = image
        self.name = name
        
        self.layer.backgroundColor = UIColor.clear.cgColor
        self.clipsToBounds = true
        self.layer.masksToBounds = false
        
        setupFlightView()
        setupImageView()
        setupName()
    }
    
    private func setupFlightView() {
        
        self.addSubview(flightView)
        flightView.backgroundColor = Style.Colors.darkBackground
        
        flightView.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY)
            make.height.width.equalTo(54)
        }
        
        flightView.layoutSubviews()
        flightView.bounds = self.flightView.bounds
        flightView.removeShadow()
    }
    
    private func setupImageView() {
        
        flightView.addSubview(itemIconView)
        itemIconView.image = image
        itemIconView.contentMode = .scaleAspectFit
        itemIconView.clipsToBounds = true
        itemIconView.image = itemIconView.image?.withRenderingMode(.alwaysTemplate)
        itemIconView.tintColor = Style.Colors.textWhite
        
        itemIconView.snp.makeConstraints { (make) in
            make.center.equalTo(flightView.snp.center)
            make.width.equalTo(30)
            make.height.equalTo(22)
        }
        
        pointView.isHidden = true
        pointView.layer.cornerRadius = 3
        pointView.backgroundColor = Style.Colors.primaryRed
        flightView.addSubview(pointView)
        pointView.snp.makeConstraints {
            $0.width.height.equalTo(6)
            $0.left.equalTo(itemIconView.snp.right).offset(-3)
            $0.top.equalTo(itemIconView.snp.bottom).offset(-3)
        }
    }
    
    private func setupName() {
        addSubview(itemTitleLabel)
        itemTitleLabel.text = name
        itemTitleLabel.font = Style.Font.titleRegularCustom(value: 13).font
        itemTitleLabel.textColor = Style.Colors.textWhite
        itemTitleLabel.textAlignment = .center
        itemTitleLabel.clipsToBounds = true
//        itemTitleLabel.isHidden = true
        
        itemTitleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.bottom.equalToSuperview().offset(-15)
        }
    }
    
    func reloadTranslitions(name: String) {
        itemTitleLabel.text = name
    }
    
    private func activateItem() {
        itemIconView.tintColor = Style.Colors.primaryRed
        itemTitleLabel.textColor = Style.Colors.primaryRed
//        self.itemTitleLabel.fadeIn(0.5)
        flightView.snp.remakeConstraints({ (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.height.width.equalTo(54)
            make.centerY.equalTo(self.snp.centerY).offset(-10)
        })
        UIView.animate(withDuration: 0.25, animations: {
            self.layoutIfNeeded()
            self.itemTitleLabel.isHidden = false
            self.flightView.showShadow()
        })
    }
    
    private func deactivateItem() {
        itemIconView.tintColor = Style.Colors.textWhite
        itemTitleLabel.textColor = Style.Colors.textWhite
//        self.itemTitleLabel.fadeOut()
        flightView.snp.remakeConstraints { (make) in
            make.centerX.equalTo(self.snp.centerX)
            make.centerY.equalTo(self.snp.centerY)
            make.height.width.equalTo(54)
        }
        UIView.animate(withDuration: 0.25, animations: {
            self.layoutIfNeeded()
            self.flightView.removeShadow()
        })
    }
    
    func isWarningShowing(_ isSowing: Bool) {
        pointView.isHidden = !isSowing
    }
}
