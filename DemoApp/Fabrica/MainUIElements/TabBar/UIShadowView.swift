//
//  UIShadowView.swift
//  
//
//  Created by Артем Чурсин on 12.04.2021.
//

import UIKit

class UIShadowView: UIView {
    let cornerRadius: CGFloat
    let isShowShadow: Bool
    
    init(cornerRadius: CGFloat = 12, showShadow: Bool) {
        self.cornerRadius = cornerRadius
        self.isShowShadow = showShadow
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var bounds: CGRect {
        didSet {
            setupShadow()
        }
    }
    
    private func setupShadow() {
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowOffset = CGSize(width: 0, height: -3)

        self.layer.shadowColor = Style.Colors.primaryRed.cgColor
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = isShowShadow ? 0.8 : 0
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: cornerRadius / 0.75, height: cornerRadius / 0.75)).cgPath
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func removeShadow() {
        self.layer.shadowOpacity = 0
    }
    
    func showShadow() {
        self.layer.shadowOpacity = 0.8
    }
}
