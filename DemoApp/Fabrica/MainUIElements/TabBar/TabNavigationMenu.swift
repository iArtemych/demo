//
//  TabNavigationMenu.swift
//  SMPAssist
//
//  Created by Артем Чурсин on 18.02.2021.
//

import UIKit
import SnapKit

class TabNavigationMenu: UIView {
    
    var itemTapped: ((_ tab: Int) -> Void)?
    var activeItem: Int = 0
    private var buttons = [TabBarItem]()
    private var menuItems = [TabItem]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    convenience init(menuItems: [TabItem], frame: CGRect, visibleHeight: CGFloat) {
        self.init(frame: frame)
        self.menuItems = menuItems

        let cornerRadius: CGFloat = 10
        
        let view = UIView()
        self.addSubview(view)
        view.backgroundColor = Style.Colors.darkBackground
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = false
        view.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(cornerRadius)
            make.height.equalTo(visibleHeight + cornerRadius)
        }
        self.sendSubviewToBack(view)
        
        self.backgroundColor = .clear
        
        self.isUserInteractionEnabled = true
        self.clipsToBounds = true
        
        for i in 0 ..< menuItems.count {
            let itemWidth = self.frame.width / CGFloat(menuItems.count)
            let leadingAnchor = itemWidth * CGFloat(i)
            
            let itemView = self.createTabItem(item: menuItems[i])
            buttons.append(itemView)
            itemView.tag = i
            
            self.addSubview(itemView)
            
            itemView.translatesAutoresizingMaskIntoConstraints = false
            itemView.clipsToBounds = true
            
            itemView.snp.makeConstraints { (make) in
                make.height.equalTo(self.snp.height)
                make.width.equalTo(itemWidth)
                make.left.equalTo(self.snp.left).offset(leadingAnchor)
                make.top.equalTo(self.snp.top)
            }
        }
        self.setNeedsLayout()
        self.layoutIfNeeded()
        self.activateTab(tab: 0)
    }
    
    func createTabItem(item: TabItem) -> TabBarItem {
        
        let tabBarItem = TabBarItem(name: item.displayTitle, image: item.icon, frame: .zero)
        
        tabBarItem.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap)))
        return tabBarItem
    }
    
    @objc func handleTap(_ sender: UIGestureRecognizer) {
        self.switchTab(from: self.activeItem, to: sender.view!.tag)
    }
    
    func switchTab(from: Int, to: Int) {
        guard from != to else {
            return
        }
        self.deactivateTab(tab: from)
        self.activateTab(tab: to)
    }
    
    func activateTab(tab: Int) {
        
        let tabToActivate = self.buttons[tab]
        tabToActivate.isActive = true
        
        self.itemTapped?(tab)
        self.activeItem = tab
    }
    
    func deactivateTab(tab: Int) {
        
        let tabToActivate = self.buttons[tab]
        tabToActivate.isActive = false
    }
    
    func configureNotificationPoint(isActive: Bool) {

    }
    
    func reloadTranslitions() {

    }
}
