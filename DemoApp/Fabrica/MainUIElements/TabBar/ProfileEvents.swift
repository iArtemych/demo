//
//  ProfileEvents.swift
//  
//
//  Created by Артем Чурсин on 12.04.2021.
//

import Foundation

enum ProfileOutEvents {
    case editProfile(ClientApiObject)
}

enum ProfileViewEvents {
    case viewDidLoad
    case settingsAction
    case postPoster(LocaleAdvertisment)
    case hidePoster
    case viewWillAppear
}
