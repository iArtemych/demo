//
//  ShadowView.swift
//  
//
//  Created by Артем Чурсин on 12.04.2021.
//

import UIKit

class ShadowView: UIView {
    
    var shadowLayer: CAShapeLayer!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let _ = shadowLayer else { return }
        layer.insertSublayer(shadowLayer, at: 0)
    }
    
    func generateShadow() {
        shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: bounds.width / 2).cgPath
        shadowLayer.fillColor = UIColor.white.cgColor
        
        shadowLayer.shadowColor = UIColor(red: 0.55, green: 0.55, blue: 0.55, alpha: 0.05).cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0, height: 15)
        shadowLayer.shadowOpacity = 0.8
        shadowLayer.shadowRadius = 2
    }
    
    func removeShadow() {
        shadowLayer?.removeFromSuperlayer()
        shadowLayer = nil
    }
}
