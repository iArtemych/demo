//
//  UITitleWithArrow.swift
//  
//
//  Created by Artem Chursin on 24.04.2021.
//

import UIKit
import SnapKit
import RxSwift

class UITitleWithArrow: UIView {
    private let bag = DisposeBag()
    private lazy var titleLabel: UILabel = {
        var view = UILabel()
        view.font = Style.Font.titleLightCustom(value: 16).font
        view.textColor = Style.Colors.textWhite
        return view
    }()
    
    private lazy var arrowImage: UIImageView = {
        let iv = UIImageView(image: R.image.arrow_right_icon())
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    init() {
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
       commonInit()
   }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        commonInit()
    }
    
    func setData(title: NSAttributedString, needArrow: Bool) {
        self.titleLabel.attributedText = title
        arrowImage.isHidden = !needArrow
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func commonInit() {
        
        addSubview(titleLabel)
        addSubview(arrowImage)
        
        arrowImage.snp.makeConstraints {
            $0.width.equalTo(24)
            $0.height.equalTo(24)
            $0.centerY.equalTo(self.snp.centerY)
            $0.right.equalTo(-25)
        }
        
        titleLabel.snp.remakeConstraints {
            $0.centerY.equalToSuperview()
            $0.left.equalTo(16)
            $0.right.lessThanOrEqualTo(arrowImage.snp.left).offset(8)
        }
    }
}



