//
//  UILoaderView.swift
//  
//
//  Created by Artem Chursin on 03.04.2021.
//

import UIKit
import APNGKit

class UILoaderView: UIView {
    
    var loaderView: APNGImageView!
    
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView(message: String?) {
//        self.backgroundColor = #colorLiteral(red: 0.7058572769, green: 0.7016633153, blue: 0.7090824246, alpha: 0.2988228703)
        self.backgroundColor = #colorLiteral(red: 0.7058823529, green: 0.7016633153, blue: 0.7090824246, alpha: 0.05)
        let image = APNGImage(named: "whiteLoader")
        loaderView = APNGImageView(image: image)
        
        
        self.addSubview(loaderView)
        
        loaderView.snp.makeConstraints {
            $0.width.height.equalTo(40)
            $0.center.equalToSuperview()
        }
        
        if let message = message {
            let label = UILabel(with: message,
                                font: .titleSemiboldCustom(value: 17),
                                color: .black)
            label.numberOfLines = 0
            label.textAlignment = .center
            
            self.addSubview(label)
            
            label.snp.makeConstraints {
                $0.top.equalTo(loaderView.snp.bottom).offset(40)
                $0.width.equalTo(220)
                $0.centerX.equalToSuperview()
            }
        }
    }
}

