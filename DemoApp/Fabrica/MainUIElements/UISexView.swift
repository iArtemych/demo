//
//  UISexView.swift
//  
//
//  Created by Artem Chursin on 30.03.2021.
//

import UIKit
import RxSwift

final class UISexView: UIView {
    
    private let bag = DisposeBag()
    lazy var sexImage = UIImageView()
    lazy var sexTitle = UILabel()
    private var shadowLayer: CAShapeLayer!
    
    var isSelected = false {
        didSet {
            addLight()
        }
    }
    
    init() {
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
       commonInit()
   }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        commonInit()
    }
    
    func addLight() {
        
    }
    
    func setData(title: NSAttributedString, image: UIImage?) {
        sexTitle.attributedText = title
        sexImage.image = image
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func commonInit() {
        addSubview(sexImage)
        addSubview(sexTitle)
        
        sexTitle.textAlignment = .center
        
        sexImage.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.width.height.equalTo(100)
            $0.centerX.equalToSuperview()
        }
        
        sexTitle.snp.makeConstraints {
            $0.top.equalTo(sexImage.snp.bottom).offset(25)
            $0.left.right.equalToSuperview()
        }
    }
}
