//
//  UIProfileEditor.swift
//  
//
//  Created by Artem Chursin on 24.04.2021.
//

import UIKit
import SnapKit
import RxSwift
import PINRemoteImage

class UIProfileEditor: UIView {
    private let bag = DisposeBag()
    
    private var nameView: UITextInput!
    let photoView = UIPhotoView()
    
    private var selectorState: ((Bool)->())?
    
    var textReader: ((String?)->())?
    var imageUpdater: VoidClosure?
    
    init() {
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
       commonInit()
   }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        commonInit()
    }
    
    func setData(text: NSAttributedString?,
                 placeholder: NSAttributedString?,
                 placeholderImage: UIImage?,
                 placeholderImageURL: String?,
                 imageUpdater: @escaping VoidClosure,
                 textReader: @escaping ((String?)->())) {
        
        nameView.textField.attributedText = text
        nameView.textField.attributedPlaceholder = placeholder
        photoView.setData(title: ("Edit Photo")
                            .attributed
                            .color(Style.Colors.secondaryRed)
                            .font(Style.Font.titleLightCustom(value: 16).font),
                          image: placeholderImage,
                          imageURL: placeholderImageURL)
        self.textReader = textReader
        self.imageUpdater = imageUpdater
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func commonInit() {
        
        nameView = UITextInput()
        nameView.setData(title: nil,
                         placeholder: "Nickname"
                            .attributed
                            .color(Style.Colors.placeholderWhite)
                            .font(Style.Font.titleLightCustom(value: 16).font),
                         contentType: .name) { [weak self] (name) in
            self?.textReader?(name)
        } doneAction: {
            
        }
        addSubview(nameView)
        
        photoView.setData(title: "Edit Photo"
                            .attributed
                            .color(Style.Colors.secondaryRed)
                            .font(Style.Font.titleLightCustom(value: 14).font),
                          image: R.image.smile())
        let photoTap = UITapGestureRecognizer()
        photoView.addGestureRecognizer(photoTap)
        photoTap.rx.event.subscribe(onNext: { [weak self] _ in
            self?.imageUpdater?()
        })
        .disposed(by: bag)
        addSubview(photoView)
        
        
        photoView.snp.makeConstraints {
            $0.width.equalTo(80)
            $0.height.equalTo(110)
            $0.top.equalToSuperview().offset(32)
            $0.left.equalToSuperview().offset(24)
        }
        
        nameView.snp.makeConstraints {
            $0.top.equalToSuperview().offset(48)
            $0.left.equalTo(photoView.snp.right).offset(16)
            $0.right.equalToSuperview().offset(-24)
            $0.height.equalTo(55)
        }
    }
    
    @objc
    private func switchChanged(mySwitch: UISwitch) {
        selectorState?(mySwitch.isOn)
    }
}




