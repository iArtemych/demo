//
//  UITitleWithRadio.swift
//  
//
//  Created by Artem Chursin on 03.04.2021.
//

import UIKit
import SnapKit
import RxSwift

class UITitleWithRadio: UIView {
    private let bag = DisposeBag()
    private lazy var titleLabel: UILabel = {
        var view = UILabel()
        view.font = Style.Font.titleRegularCustom(value: 17).font
        return view
    }()
    
    private lazy var checkImage: UIImageView = {
        let iv = UIImageView(image: nil)
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    init() {
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
       commonInit()
   }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        commonInit()
    }
    
    func setData(title: NSAttributedString, isSelect: Bool) {
        self.titleLabel.attributedText = title
        self.checkImage.image = isSelect ? R.image.checked() : R.image.unchecked()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func commonInit() {
        
        addSubview(titleLabel)
        addSubview(checkImage)
        
        checkImage.snp.makeConstraints {
            $0.width.height.equalTo(20)
            $0.centerY.equalTo(self.snp.centerY)
            $0.left.equalTo(18)
        }
        
        titleLabel.snp.remakeConstraints {
            $0.centerY.equalToSuperview()
            $0.left.equalTo(checkImage.snp.right).offset(18)
            $0.right.equalToSuperview().offset(-18)
        }
    }
}



