//
//  UICodeCell.swift
//  
//
//  Created by Artem Chursin on 31.03.2021.
//

import UIKit
import RxSwift

final class UICodeCell: UIView {
    
    private let bag = DisposeBag()
    var textLabel: UILabel = {
        let view = UILabel()
        view.textColor = Style.Colors.primaryRed
        view.font = Style.Font.titleMediumCustom(value: 42).font
        view.textAlignment = .center
        return view
    }()
    private lazy var underline = UIView()
    
    var isActive = false {
        didSet {
            underline.backgroundColor = isActive ? Style.Colors.primaryRed : Style.Colors.secondaryRed
        }
    }
    
    init() {
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
       commonInit()
   }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        commonInit()
    }
    
    func setData(number: String) {
        textLabel.text = number
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        underline.backgroundColor = Style.Colors.secondaryRed
        
        addSubview(textLabel)
        addSubview(underline)
        
        textLabel.snp.makeConstraints {
            $0.right.left.equalToSuperview()
            $0.height.equalTo(75)
            $0.centerY.equalTo(self.snp.centerY)
        }
        
        underline.snp.remakeConstraints {
            $0.top.equalTo(textLabel.snp.bottom).offset(1)
            $0.left.equalTo(textLabel.snp.left)
            $0.right.equalTo(textLabel.snp.right)
            $0.height.equalTo(1)
        }
    }
}


