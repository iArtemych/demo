//
//  UICodeInput.swift
//  
//
//  Created by Artem Chursin on 31.03.2021.
//

import UIKit
import RxSwift

final class UICodeInput: UIView {
    
    private let bag = DisposeBag()
    private var firstTextField = UICodeCell()
    private var secondTextField = UICodeCell()
    private var thirdTextField = UICodeCell()
    private var fourthTextField = UICodeCell()
    var codeArray: [UICodeCell] = []
    var vStack: UIStackView!
    
    lazy var textField: UITextField = {
        var view = UITextField()
        view = UITextField()
        view.keyboardAppearance = .dark
        view.backgroundColor = .white
        view.isHidden = true
        return view
    }()
    
    var textReader: ((String?)->())?
    
    init() {
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
       commonInit()
   }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        commonInit()
    }
    
    func setData(textReader: ((String?)->())?) {
        self.textReader = textReader
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        codeArray = [firstTextField,
                    secondTextField,
                    thirdTextField,
                    fourthTextField]
        
        vStack = UIStackView(arrangedSubviews: codeArray)
        vStack.arrangedSubviews.forEach {
            $0.snp.makeConstraints { (make) in
                make.width.equalTo(48)
            }
        }
        vStack.alignment = .center
        vStack.contentMode = .center
        vStack.spacing = 16
        self.addSubview(vStack)
        
        textField.delegate = self
        textField.keyboardType = .numberPad
        textField.returnKeyType = .done
        textField.addTarget(self, action: #selector(textUpdate), for: .editingChanged)
        addSubview(textField)
        
        vStack.snp.makeConstraints {
            $0.left.right.equalToSuperview()
            $0.centerY.equalToSuperview()
            $0.height.equalTo(80)
        }
        
        textField.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.left.right.equalToSuperview()
        }
    }
    
    func clearField() {
        textField.text = ""
        textUpdate(textField)
    }
    
    //TODO: - update it
    @objc
    private func textUpdate(_ textField: UITextField) {
        textReader?(textField.text)
        guard let code = textField.text else {
            return
        }
        
        for (cellIndex, cell) in codeArray.enumerated() {
            let characters = Array(code)
            let char = characters[safe: cellIndex]
            if let output = char {
                cell.textLabel.text = String(output)
            } else {
                cell.textLabel.text = ""
            }

            code.enumerated().forEach { (index, element) in
                if index == cellIndex {
                    cell.isActive = true
                } else {
                    cell.isActive = false
                }
            }
        }
        
        if code == "" {
            codeArray.enumerated().forEach {
                $1.isActive = $0 == 0
            }
        }
    }
}

extension UICodeInput: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 4
    }
}

