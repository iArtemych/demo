//
//  UIPhotoView.swift
//  
//
//  Created by Artem Chursin on 02.04.2021.
//

import UIKit
import RxSwift
import PINRemoteImage

final class UIPhotoView: UIView {
    
    private let bag = DisposeBag()
    lazy var imagePhoto = UIImageView()
    lazy var titlePhoto = UILabel()
    
    init() {
       super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
       commonInit()
   }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        commonInit()
    }
    
    func setData(title: NSAttributedString,
                 image: UIImage?,
                 imageURL: String? = nil) {
        
        
        titlePhoto.attributedText = title
        imagePhoto.image = image
        if let urlString = imageURL,
           let url = URL(string: urlString) {
            print("url: \(url)")
            imagePhoto.pin_setImage(from: url)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func commonInit() {
        imagePhoto.layer.masksToBounds = true
        imagePhoto.layer.borderWidth = 2
        imagePhoto.layer.borderColor = Style.Colors.primaryRed.cgColor
        imagePhoto.layer.cornerRadius = 40
        
        addSubview(imagePhoto)
        addSubview(titlePhoto)
        
        titlePhoto.textAlignment = .center
        
        imagePhoto.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.width.height.equalTo(80)
            $0.centerX.equalToSuperview()
        }
        
        titlePhoto.snp.makeConstraints {
            $0.top.equalTo(imagePhoto.snp.bottom).offset(4)
            $0.left.right.equalToSuperview()
        }
    }
}

