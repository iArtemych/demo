//
//  Notification.Name+Extensions.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation

extension Notification.Name {
    static let logout = Notification.Name("logout")
}
