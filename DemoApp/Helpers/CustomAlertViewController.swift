//
//  CustomAlertViewController.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit
import SnapKit

class CustomAlertViewController: UIViewController {
    
    let visualEffectView = UIView()
    var alertView: CustomAlert
    
    init(alert: CustomAlert) {
        self.alertView = alert
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        visualEffectView.backgroundColor = #colorLiteral(red: 0.1764705882, green: 0.1921568627, blue: 0.2549019608, alpha: 0.2)
        view.addSubview(visualEffectView)
        visualEffectView.snp.makeConstraints {
            $0.top.bottom.left.right.equalToSuperview()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setAlert()
        animateIn()
    }
    
    private func setAlert() {
        alertView.closeAction = { [weak self] in
            self?.animateOut()
        }
        self.view.addSubview(alertView)
        alertView.snp.makeConstraints {
            $0.center.equalTo(view.snp.center)
            $0.width.equalTo(270)
            $0.height.lessThanOrEqualTo(500)
        }
    }
    
    private func animateIn() {
        alertView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        alertView.alpha = 0
        
        UIView.animate(withDuration: 0.4) {
            self.visualEffectView.alpha = 1
            self.alertView.alpha = 1
            self.alertView.transform = CGAffineTransform.identity
        }
    }
    
    private func animateOut() {
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.visualEffectView.alpha = 0
                        self.alertView.alpha = 0
                        self.alertView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
                       }) { (_) in
            self.alertView.removeFromSuperview()
            self.dismiss(animated: true, completion: nil)
        }
    }
}

