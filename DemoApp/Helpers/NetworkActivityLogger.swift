//
//  NetworkActivityLogger.swift
//  AlamofireNetworkActivityLogger
//
//  The MIT License (MIT)
//
//  Copyright (c) 2016 Konstantin Kabanov
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

// swiftlint:disable cyclomatic_complexity

import Alamofire
import Foundation
import os.log

private extension OSLog {
    private static var subsystem = Bundle.main.bundleIdentifier!
    static let networking = OSLog(subsystem: subsystem, category: "networking")
}

/// The level of logging detail.
public enum NetworkActivityLoggerLevel {
    /// Do not log requests or responses.
    case off

    /// Logs HTTP method, URL, header fields, & request body for requests, and status code, URL, header fields, response string, & elapsed time for responses.
    case debug

    /// Logs HTTP method & URL for requests, and status code, URL, & elapsed time for responses.
    case info

    /// Logs HTTP method & URL for requests, and status code, URL, & elapsed time for responses, but only for failed requests.
    case warn

    /// Equivalent to `.warn`
    case error

    /// Equivalent to `.off`
    case fatal
}

/// `NetworkActivityLogger` logs requests and responses made by Alamofire.SessionManager, with an adjustable level of detail.
public class NetworkActivityLogger {
    // MARK: - Properties

    /// The shared network activity logger for the system.
    public static let shared = NetworkActivityLogger()

    /// The level of logging detail. See NetworkActivityLoggerLevel enum for possible values. .info by default.
    public var level: NetworkActivityLoggerLevel

    /// Omit requests which match the specified predicate, if provided.
    public var filterPredicate: NSPredicate?

    private let queue = DispatchQueue(label: "\(NetworkActivityLogger.self) Queue")

    // MARK: - Internal - Initialization

    init() {
        level = .info
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Logging

    /// Start logging requests and responses.
    public func startLogging() {
        NotificationCenter.default.removeObserver(self)
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(NetworkActivityLogger.requestDidStart(notification:)),
            name: Notification.Name(rawValue: "org.alamofire.notification.name.request.didResume"),
            object: nil
        )

        notificationCenter.addObserver(
            self,
            selector: #selector(NetworkActivityLogger.requestDidFinish(notification:)),
            name: Notification.Name(rawValue: "org.alamofire.notification.name.request.didFinish"),
            object: nil
        )
    }

    // MARK: - Private - Notifications

    @objc private func requestDidStart(notification: Notification) {
        queue.async {
            guard let dataRequest = notification.request as? DataRequest,
                let task = dataRequest.task,
                let request = task.originalRequest,
                let httpMethod = request.httpMethod,
                let requestURL = request.url
                else {
                    return
            }

            if let filterPredicate = self.filterPredicate, filterPredicate.evaluate(with: request) {
                return
            }

            let requestStr = "\(httpMethod) '\(requestURL.absoluteString)'"

            switch self.level {
            case .debug:
                let cURL = dataRequest.cURLDescription()
                let string = " \n⬆️ \(requestStr):\n\(cURL)\n "
                    + .invisibleWhiteSpace
                os_log("%{private}@", log: .networking, type: .debug, string)

            case .info:
                let string = " \n⬆️ \(requestStr)\n "
                    + .invisibleWhiteSpace
                os_log("%{private}@", log: .networking, type: .info, string)
            default:
                break
            }
        }
    }

    @objc private func requestDidFinish(notification: Notification) {
        queue.async {
            guard let dataRequest = notification.request as? DataRequest,
                let task = dataRequest.task,
                let metrics = dataRequest.metrics,
                let request = task.originalRequest,
                let httpMethod = request.httpMethod,
                let requestURL = request.url
                else {
                    return
            }

            if let filterPredicate = self.filterPredicate, filterPredicate.evaluate(with: request) {
                return
            }

            let elapsedTime = metrics.taskInterval.duration

            if let error = task.error {
                switch self.level {
                case .debug, .info, .warn, .error:
                    let string = " \n⬇️⛔️ \(httpMethod) '\(requestURL.absoluteString)'"
                        + " [\(String(format: "%.04f", elapsedTime)) s]:"
                        + "\n\(error)\n "
                        + .invisibleWhiteSpace

                    os_log("%{private}@", log: .networking, type: .default, string)
                default:
                    break
                }
            } else {
                guard let response = task.response as? HTTPURLResponse else {
                    return
                }

                let isStatusSuccessfull = (200..<300).contains(response.statusCode)
                let statusSign = isStatusSuccessfull ? "✅" : "⚠️"

                switch self.level {
                case .debug:

                    var str = " \n⬇️\(statusSign) code: \(String(response.statusCode)) | \(httpMethod) '\(requestURL.absoluteString)'"
                        + " [\(String(format: "%.04f", elapsedTime)) s]:"

                    let headers = self.headersString(from: response.allHeaderFields)
                    str += "\n\(headers)"

                    guard let data = dataRequest.data else { break }
                    var body = "Body:\n"

                    do {
                        let jsonObject = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                        let prettyData = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)

                        if let prettyString = String(data: prettyData, encoding: .utf8) {
                            body += prettyString
                        }
                    } catch {
                        if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                            body += string as String
                        }
                    }
                    print("\(body)")
                    str += .invisibleWhiteSpace
                    os_log("%{private}@", log: .networking, type: .debug, str)

                case .info:
                    let str = " \n⬇️\(statusSign) \(String(response.statusCode)) '\(requestURL.absoluteString)'"
                        + " [\(String(format: "%.04f", elapsedTime)) s]"
                        + .invisibleWhiteSpace

                    os_log("%{private}@", log: .networking, type: .info, str)
                default:
                    break
                }
            }
        }

    }
}

private extension NetworkActivityLogger {
    func headersString(from headers: [AnyHashable: Any]) -> String {
        var str = "Headers: ["
        for (key, value) in headers {
            str += "\n  \(key): \(value)"
        }
        str += "]"
        return str
    }
}

private extension String {
    static let invisibleWhiteSpace =
    """
    \n 
    """
}
