//
//  Reusable.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

protocol Reusable {
    static func reuseIdentifier() -> String
}

extension Reusable {
    static func reuseIdentifier() -> String {
        return String(describing: Self.self) // Identifier is equal to type name of class that adopts protocol
    }
}

extension UITableView {
    func register<CellType: UITableViewCell & Reusable>(_ cellClass: CellType.Type) {
        register(cellClass, forCellReuseIdentifier: CellType.reuseIdentifier())
    }

    func registerForHeaderFooterView<CellType: UITableViewHeaderFooterView & Reusable>(_ cellClass: CellType.Type) {
        register(cellClass, forHeaderFooterViewReuseIdentifier: CellType.reuseIdentifier())
    }

    func dequeueReusableCell<CellType: UITableViewCell & Reusable>(_ cellClass: CellType.Type, for indexPath: IndexPath? = nil) -> CellType {
        var cell: CellType?
        if let indexPath = indexPath {
            cell = dequeueReusableCell(withIdentifier: CellType.reuseIdentifier(), for: indexPath) as? CellType
        } else {
            cell = dequeueReusableCell(withIdentifier: CellType.reuseIdentifier()) as? CellType
        }

        guard let result = cell else {
            fatalError("Unable to dequeue \(String(describing: cellClass)) with reuseId of \(cellClass.reuseIdentifier())")
        }
        return result
    }

    func dequeueReusableHeaderFooterView<CellType: UITableViewHeaderFooterView & Reusable>(_ cellClass: CellType.Type) -> CellType {
        guard let cell = self.dequeueReusableHeaderFooterView(withIdentifier: CellType.reuseIdentifier()) as? CellType
            else { fatalError("Unable to dequeue \(String(describing: cellClass)) with reuseId of \(cellClass.reuseIdentifier())") }

        return cell
    }
}

extension UICollectionView {
    func register<CellType: UICollectionViewCell & Reusable>(_ cellClass: CellType.Type) {
        register(cellClass, forCellWithReuseIdentifier: cellClass.reuseIdentifier())
    }

    func register<CellType: UICollectionReusableView & Reusable>(_ cellClass: CellType.Type, forSupplementaryViewOfKind kind: String) {
        register(cellClass, forSupplementaryViewOfKind: kind, withReuseIdentifier: cellClass.reuseIdentifier())
    }

    func dequeueReusableCell<CellType: UICollectionViewCell & Reusable>(_ cellClass: CellType.Type, for indexPath: IndexPath) -> CellType {
        guard let cell = dequeueReusableCell(withReuseIdentifier: CellType.reuseIdentifier(), for: indexPath) as? CellType else {
            fatalError("Unable to dequeue \(String(describing: cellClass)) with reuseId of \(cellClass.reuseIdentifier())")
        }
        return cell
    }

    func dequeueReusableSupplementaryView<CellType: UICollectionReusableView & Reusable>(
        _ cellClass: CellType.Type,
        ofKind kind: String,
        for indexPath: IndexPath) -> UICollectionReusableView {

        return dequeueReusableSupplementaryView(ofKind: kind,
                                                withReuseIdentifier: cellClass.reuseIdentifier(),
                                                for: indexPath)
    }
}


