//
//  Dates.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation

public extension String {
    func toDate(format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self) ?? Date()
    }
}

public extension Date {
    func toString(format: String) -> String {
        let dateFormatter = DateFormatter()
        let code = "ru"
        let identifier = "\(code.lowercased())_\(code.uppercased())"
        dateFormatter.locale = Locale(identifier: identifier)
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

public extension Date {
    func convertToUTC() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return dateFormatter.string(from: self)
    }
}
