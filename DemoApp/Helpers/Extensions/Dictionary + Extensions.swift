//
//  Dictionary + Extensions.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation

public extension Dictionary {
    static func += <K, V> (left: inout [K: V], right: [K: V]) {
        for (key, value) in right {
            left[key] = value
        }
    }
}

public extension Dictionary {
    func compactMapValues<T>(_ transform: (Key, Value) throws -> T?) rethrows -> [Key: T] {
        return try self.compactMap { (key, value) in
            guard let newValue = try transform(key, value)
            else { return nil }
            return (key, newValue)
        }
    }

    func mapValues<T>(_ transform: (Key, Value) throws -> T) rethrows -> [Key: T] {
        return try self.map { (key, value) in
            (key, try transform(key, value))
        }
    }

    func map<K, V>(_ transform: (Key, Value) throws -> (K, V)) rethrows -> [K: V] {
        return [K: V](uniqueKeysWithValues: try map(transform))
    }

    func compactMap<K, V>(_ transform: (Key, Value) throws -> (K, V)?) rethrows -> [K: V] {
        return [K: V](uniqueKeysWithValues: try compactMap(transform))
    }
}

public extension Dictionary where Key == NSAttributedString.Key {
    func toStringKeyAttributes() -> [String: Any] {
        var attributes = [String: Any]()
        self.forEach { (key, value) in
            attributes[key.rawValue] = value
        }
        return attributes
    }
}

public extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension Dictionary {
    func returnJsonText() -> String {
        if #available(iOS 13.0, *) {
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: self,
                options: []) {
                let theJSONText = String(data: theJSONData,
                                         encoding: .utf8)
                print("JSON string = \(theJSONText!)")
                return theJSONText ?? ""
            }
        } else {
            if let theJSONData = try? JSONSerialization.data(
                withJSONObject: self,
                options: []) {
                let theJSONText = String(data: theJSONData,
                                         encoding: .utf8)
                print("JSON string = \(theJSONText!)")
                return theJSONText ?? ""
            }
        }
        return ""
    }
}


