//
//  UIViewController+ModalExtension.swift
//  
//
//  Created by Артем Чурсин on 07.04.2021.
//

import UIKit

extension UIViewController {
    @discardableResult
    func presentCustomPhotoModal(
        didSelect: ((PhotoModalViewController.ImportType) -> ())?) -> PhotoModalViewController {
        let vc = PhotoModalViewController(willDismiss: nil, didSelect: didSelect)
        
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        return vc
    }
    
    @discardableResult
    func presentAdvertismentModal(mode: AdvertismentModalViewController.AdvertismentMode,
                                  advertisment: LocaleAdvertisment?,
                                  finishAction: ((LocaleAdvertisment, AdvertismentModalViewController.AdvertismentAction)-> ())?) -> AdvertismentModalViewController {
        let vc = AdvertismentModalViewController(mode: mode,
                                                 advertisment: advertisment,
                                                 willDismiss: finishAction)
        
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
        return vc
    }
}
