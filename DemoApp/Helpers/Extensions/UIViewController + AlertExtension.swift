//
//  UIViewController + AlertExtension.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

extension UIViewController {
    
    func presentCustomAlert(_ alert: CustomAlert) {
        let vc = CustomAlertViewController(alert: alert)
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }

    /// Show blocking fullscreen alert for error
//    func present(error: Error) {
//        let alert = CustomAlert(
//            withWarning: LangService.shared.getText(key: "alert.text.title"),
//            message: error.localizedDescription,
//            closeText: LangService.shared.getText(key: "alert.button.close"),
//            type: .error)
//
//        self.presentCustomAlert(alert)
//    }
}
