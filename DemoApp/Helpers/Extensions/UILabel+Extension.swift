//
//  UILabel+Extension.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

extension UILabel {
    
    convenience init(with text: String,
                     font: Style.Font,
                     color: UIColor,
                     textAlignment: NSTextAlignment = .center,
                     numberOfLines: Int = 0) {
        self.init()
        
        self.text = text
        self.font = font.font
        self.textColor = color
        self.textAlignment = .center
        self.numberOfLines = 0
    }
}
