//
//  SlidingAnimationController.swift
//  
//
//  Created by Артем Чурсин on 07.04.2021.
//

import UIKit

class SlidingAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    struct AnimatedProperties {
        let backgroundColor: UIColor?
        let transform: CATransform3D
    }
    
    enum Direction {
        case `in`
        case out
    }
    
    private var direction: Direction
    
    init(withDirection direction: Direction) {
        self.direction = direction
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        var animatedVC: UIViewController!
        var initialProperties: AnimatedProperties!
        var finalProperties: AnimatedProperties!
        
        var finalFrame: CGRect { return transitionContext.finalFrame(for: animatedVC) }

        switch direction {
        case .in:
            guard let toVC = transitionContext.viewController(forKey: .to)  else { return }
            animatedVC = toVC
            initialProperties = AnimatedProperties(backgroundColor: UIColor.clear, transform: CATransform3DMakeTranslation(0, finalFrame.height, 0))
            finalProperties = AnimatedProperties(backgroundColor: #colorLiteral(red: 0.1593177021, green: 0.1681738198, blue: 0.2114116549, alpha: 0.6332940925), transform: CATransform3DIdentity)
        case .out:
            guard let fromVC = transitionContext.viewController(forKey: .from)  else { return }
            animatedVC = fromVC
            finalProperties = AnimatedProperties(backgroundColor: UIColor.clear, transform: CATransform3DMakeTranslation(0, finalFrame.height, 0))
             initialProperties = AnimatedProperties(backgroundColor: #colorLiteral(red: 0.1593177021, green: 0.1681738198, blue: 0.2114116549, alpha: 0.6332940925), transform: CATransform3DIdentity)
        }
        
        // Snapshow with clear background
        animatedVC.view.backgroundColor = UIColor.clear
        guard let snapshot = animatedVC.view.snapshotView(afterScreenUpdates: true) else { return }

        let backgroundView = UIView(frame: finalFrame)
        
        // Initian parameters
        backgroundView.backgroundColor = initialProperties.backgroundColor
        snapshot.layer.transform = initialProperties.transform
        animatedVC.view.isHidden = true

        transitionContext.containerView.addSubview(backgroundView)
        transitionContext.containerView.addSubview(animatedVC.view)
        transitionContext.containerView.addSubview(snapshot)
        
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animateKeyframes(
            withDuration: duration,
            delay: 0,
            options: .calculationModeCubic,
            animations: {
                UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1) {
                    snapshot.layer.transform = finalProperties.transform
                    backgroundView.backgroundColor = finalProperties.backgroundColor
                }
        },
            completion: { _ in
                animatedVC.view.isHidden = false
                animatedVC.view.backgroundColor = finalProperties.backgroundColor
                snapshot.removeFromSuperview()
                backgroundView.removeFromSuperview()
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}

