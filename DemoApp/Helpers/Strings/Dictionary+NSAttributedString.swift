//
//  Dictionary+NSAttributedString.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

typealias StringAttributes = [NSAttributedString.Key: Any]

extension Dictionary where Key == NSAttributedString.Key, Value == Any {
    
    func color(_ color: UIColor) -> [Key: Value] {
        var new = self
        
        new[.foregroundColor] = color
        
        return new
    }
    
    func underline(_ color: UIColor) -> [Key: Value] {
        var new = self
        
        new[.underlineColor] = color
        
        return new
    }
    
    func font(_ font: UIFont?) -> [Key: Value] {
        var new = self
        
        new[.font] = font
        
        return new
    }
    
    func alignment(_ value: NSTextAlignment?, style: NSMutableParagraphStyle? = nil) -> [Key: Value] {
        var new = self
        
        guard let value = value else { return new }
        
        let paragraph: NSMutableParagraphStyle
        
        if let parameterStyle = style {
            paragraph = parameterStyle
        } else if let storedStyle = new[.paragraphStyle] as? NSMutableParagraphStyle {
            paragraph = storedStyle
        } else {
            paragraph = NSMutableParagraphStyle()
        }
        
        paragraph.alignment = value
        new[.paragraphStyle] = paragraph
        
        return new
    }
    
    func lineSpacing(_ value: CGFloat, style: NSMutableParagraphStyle? = nil) -> [Key: Value] {
        var new = self
        
        let paragraph: NSMutableParagraphStyle
        
        if let parameterStyle = style {
            paragraph = parameterStyle
        } else if let storedStyle = new[.paragraphStyle] as? NSMutableParagraphStyle {
            paragraph = storedStyle
        } else {
            paragraph = NSMutableParagraphStyle()
        }
        
        paragraph.lineSpacing = value
        new[.paragraphStyle] = paragraph
        
        return new
    }
    
    func letterSpacing(_ value: CGFloat) -> [Key: Value] {
        var new = self
        
        new[.kern] = value
        
        return new
    }

}


