//
//  NSMutableAttributedString+Extensions.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

extension NSMutableAttributedString {
    
    func attributeRange(using optionalRange: NSRange?) -> NSRange {
        let range: NSRange
        
        if let requiredRange = optionalRange {
            range = requiredRange
        } else {
            range = self.string.range
        }
        
        return range
    }
    
    func style(at range: NSRange) -> NSMutableParagraphStyle? {
        let currentStyle = self.attribute(.paragraphStyle, at: 0, longestEffectiveRange: nil, in: range) as? NSParagraphStyle
        let style = currentStyle?.mutableCopy() as? NSMutableParagraphStyle

        return style
    }
    
    @discardableResult
    func color(at optionalRange: NSRange? = nil, _ color: UIColor) -> Self {
        self.addAttributes(StringAttributes().color(color), range: self.attributeRange(using: optionalRange))
        
        return self
    }
    
    @discardableResult
    func underline(at optionalRange: NSRange? = nil, _ color: UIColor) -> Self {
        self.addAttributes(StringAttributes().underline(color), range: self.attributeRange(using: optionalRange))
        
        return self
    }
    
    @discardableResult
    func font(at optionalRange: NSRange? = nil, _ font: UIFont) -> Self {
        self.addAttributes(StringAttributes().font(font), range: self.attributeRange(using: optionalRange))
        
        return self
    }
    
    @discardableResult
    func alignment(at optionalRange: NSRange? = nil, _ value: NSTextAlignment) -> Self {
        let range = self.attributeRange(using: optionalRange)
        let currentStyle = self.style(at: range)
        self.addAttributes(StringAttributes().alignment(value, style: currentStyle), range: range)
        
        return self
    }
    
    @discardableResult
    func lineSpacing(at optionalRange: NSRange? = nil, _ value: CGFloat) -> Self {
        let range = self.attributeRange(using: optionalRange)
        let currentStyle = self.style(at: range)
        self.addAttributes(StringAttributes().lineSpacing(value, style: currentStyle), range: range)
        
        return self
    }
    
    @discardableResult
    func letterSpacing(at optionalRange: NSRange? = nil, _ value: CGFloat) -> Self {
        self.addAttributes(StringAttributes().letterSpacing(value), range: self.attributeRange(using: optionalRange))
        
        return self
    }
}

