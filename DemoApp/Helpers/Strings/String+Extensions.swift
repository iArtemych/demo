//
//  String+Extensions.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

extension String {

    var attributed: NSMutableAttributedString {
        return NSMutableAttributedString(string: self)
    }
    
    var range: NSRange {
        return NSRange(location: 0, length: self.utf16.count)
    }
    
}

extension String {
    func calculateSize(font: UIFont, width: CGFloat = .greatestFiniteMagnitude) -> CGSize {
        let attributed = self
            .attributed
            .font(font)
        
        return attributed.boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil).size
    }
}

extension String {
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
    
    func deletingSuffix(_ suffix: String) -> String {
        guard self.hasSuffix(suffix) else { return self }
        return String(self.dropLast(suffix.count))
    }
}

extension String {
    func returnPathFile() -> URL {
        return URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[safe: 0] ?? "").appendingPathComponent(self)
    }
}

// MARK: - Email

extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
}

