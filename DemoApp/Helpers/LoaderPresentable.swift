//
//  LoaderPresentable.swift
//  
//
//  Created by Artem Chursin on 03.04.2021.
//

import UIKit
import APNGKit

protocol LoaderPresentable {
    var loaderView: UILoaderView! {get set}
}

extension LoaderPresentable where Self: UIViewController {
    func configureLoader(isLoading: Bool, message: String? = nil, loader: UILoaderView?) {
        guard let loader = loader else {
            return
        }
        loader.setupView(message: message)
        if isLoading {
            self.view.addSubview(loader)
            loader.snp.makeConstraints {
                loaderView.loaderView.startAnimating()
                $0.edges.equalToSuperview()
            }
        } else {
            loaderView.loaderView.stopAnimating()
            loader.removeFromSuperview()
        }
    }
}
