//
//  Style.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

class Style: NSObject {
}

extension UIColor {
    static func getColorProgress(percent: Int) -> UIColor {
        var red = 0
        var green = 0
        var blue = 0

        if percent <= 50 {
            red = 255
            green = 255 * ((percent >= 50 ? 50 : percent)*2) / 100
            blue = 0
        } else {
            red = 255 * ((percent < 50 ? 50 : 100 - percent)*2) / 100
            green = 255
            blue = 0
        }
        return UIColor(
            red: CGFloat(red) / 255.0,
            green: CGFloat(green) / 255.0,
            blue: CGFloat(blue) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    static func hexStringToUIColor (_ hex: String) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if cString.hasPrefix("#") {
            cString = String(cString[cString.index(cString.startIndex, offsetBy: 1)...])
        }
        if (cString.count) != 6 {
            return UIColor.gray
        }
        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension Style {
    enum Colors {
        /// Returns #FFFFFF placeholders, text
        static var white: UIColor { return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) }
        /// Returns #0C0404 screens background, mainScreen buttons
        static var darkBackground: UIColor { return #colorLiteral(red: 0.04705882353, green: 0.01568627451, blue: 0.01568627451, alpha: 1) }
        /// Returns #FFFFFF alpha 75 title
        static var dimWhite: UIColor { return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.75) }
        /// Returns #FFFFFF alpha 55 text
        static var textWhite: UIColor { return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.55) }
        /// Returns #FFFFFF alpha 20 placeholders
        static var placeholderWhite: UIColor { return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.2) }
        /// Returns #57010E dark red elements
        static var secondaryRed: UIColor { return #colorLiteral(red: 0.3411764706, green: 0.003921568627, blue: 0.05490196078, alpha: 1) }
        /// Returns #840407
        static var primaryRed: UIColor { return #colorLiteral(red: 0.5176470588, green: 0.01568627451, blue: 0.02745098039, alpha: 1) }
        /// Returns #1C1C1C
        static var buttonDisabled: UIColor { return #colorLiteral(red: 0.1098039216, green: 0.1098039216, blue: 0.1098039216, alpha: 1) }
        /// Returns #32C325
        static var greenState: UIColor {return #colorLiteral(red: 0.1960784314, green: 0.7647058824, blue: 0.1450980392, alpha: 1)}
    }

    enum Font {
        //new
        case titleRegularCustom(value: CGFloat)
        case titleBoldCustom(value: CGFloat)
        case titleLightCustom(value: CGFloat)
        case titleMediumCustom(value: CGFloat)
        case titleSemiboldCustom(value: CGFloat)

        var font: UIFont {
            switch self {
            // new
            case .titleRegularCustom(value: let value): return UIFont.init(name: "NotoSans-Regular", size: value) ?? UIFont.systemFont(ofSize: value)
            case .titleBoldCustom(value: let value): return UIFont.init(name: "NotoSans-Bold", size: value) ?? UIFont.systemFont(ofSize: value)
            case .titleLightCustom(value: let value): return UIFont.init(name: "NotoSans-Light", size: value) ?? UIFont.systemFont(ofSize: value)
            case .titleMediumCustom(value: let value): return UIFont.init(name: "NotoSans-Medium", size: value) ?? UIFont.systemFont(ofSize: value)
            case .titleSemiboldCustom(value: let value): return UIFont.init(name: "NotoSans-SemiBold", size: value) ?? UIFont.systemFont(ofSize: value)
            }
        }
    }

    enum LineHeight {
        case title

        var lineSpacing: CGFloat {
            switch self {
            case .title: return 0
            }
        }
    }
}

