//
//  AdvertismentModalViewController.swift
//  
//
//  Created by Artem Chursin on 18.04.2021.
//

import UIKit
import RxSwift
import ConvenientImagePicker
import PINRemoteImage

class AdvertismentModalViewController: UIViewController {
    
    enum AdvertismentMode {
        case new, forEdit
    }
    
    enum AdvertismentAction {
        case save, hide
    }

    private var closeButton = UIButton(type: .system)
    private var titleLabel = UILabel()
    private var avatarImage = UIImageView()
    private var advertismentText = UITextView()
    private var hideButton = UIButton(type: .system)
    private var saveButton = DefaultButton()
    
    private let modalHeight = 650
    private var willDismiss: ((LocaleAdvertisment, AdvertismentAction)-> ())?
    private var modalView: UIView!
    private let bag = DisposeBag()
    private let mode: AdvertismentMode
    private var advertisment: LocaleAdvertisment
    
    init(mode: AdvertismentMode,
         advertisment: LocaleAdvertisment?,
         willDismiss: ((LocaleAdvertisment, AdvertismentAction)-> ())? = nil) {

        if let oldAdvertisment = advertisment {
            self.advertismentText.text = oldAdvertisment.text
            if let urlString = oldAdvertisment.imageURL,
               let url = URL(string: urlString) {
                print("url: \(url)")
                avatarImage.pin_setImage(from: url)
            }
            self.advertisment = oldAdvertisment
        } else {
            self.advertisment = LocaleAdvertisment()
        }
        
        self.willDismiss = willDismiss
        self.mode = mode
        super.init(nibName: nil, bundle: nil)
        transitioningDelegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    private func setupView() {
        let dismissView = UIView()
        dismissView.backgroundColor = .clear
        dismissView.isUserInteractionEnabled = true
        let dismissTap = UITapGestureRecognizer(target: self, action: #selector(dismissAction))
        let dismissSwipe = UISwipeGestureRecognizer(target: self, action: #selector(dismissAction))
        dismissSwipe.direction = .down
        dismissView.addGestureRecognizer(dismissTap)
        
        modalView = UIView()
        modalView.backgroundColor = Style.Colors.darkBackground
        modalView.addGestureRecognizer(dismissSwipe)
        
        self.view.addSubview(dismissView)
        self.view.addSubview(modalView)
        
        
        closeButton.setImage(R.image.close(), for: [])
        closeButton.tintColor = Style.Colors.dimWhite
        closeButton.titleLabel?.font = Style.Font.titleMediumCustom(value: 18).font
        closeButton.setTitleColor(Style.Colors.primaryRed, for: [])
        closeButton.rx.tap
            .subscribe { [weak self] (_) in
                self?.dismissAction()
            }.disposed(by: bag)
        
        titleLabel.attributedText = (mode == .new ? "Create advertisment" : "Edit advertisment")
            .attributed
            .font(Style.Font.titleSemiboldCustom(value: 16).font)
            .color(Style.Colors.dimWhite)
        
        avatarImage.layer.masksToBounds = true
        avatarImage.layer.cornerRadius = 16
        avatarImage.layer.borderWidth = 1
        avatarImage.layer.borderColor = Style.Colors.primaryRed.cgColor
        avatarImage.contentMode = .scaleAspectFill
        avatarImage.isUserInteractionEnabled = true
        let photoTap = UITapGestureRecognizer(target: self, action: #selector(editTap))
        
        avatarImage.addGestureRecognizer(photoTap)
        
        advertismentText.backgroundColor = Style.Colors.darkBackground
//        advertismentText.backgroundColor = .red
        advertismentText.font = Style.Font.titleLightCustom(value: 16).font
        advertismentText.tintColor = Style.Colors.primaryRed
        advertismentText.textColor = Style.Colors.textWhite
        advertismentText.keyboardAppearance = .dark
        advertismentText.translatesAutoresizingMaskIntoConstraints = true
        advertismentText.sizeToFit()
        advertismentText.isScrollEnabled = false
        
        let underlineView = UIView()
        underlineView.backgroundColor = Style.Colors.secondaryRed
        
        hideButton.setTitle("Hide", for: [])
        hideButton.tintColor = Style.Colors.placeholderWhite
        hideButton.rx.tap
            .subscribe { [weak self] (_) in
                guard let self = self else {
                    return
                }
                self.willDismiss?(self.advertisment, .hide)
                self.dismissAction()
            }.disposed(by: bag)
        hideButton.isHidden = (mode == .new || advertisment.isHiden)
        
        saveButton.setTitle(( advertisment.isHiden ? "Show" :"Save"), for: [])
        saveButton.rx.tap
            .subscribe { [weak self] (_) in
                guard let self = self else {
                    return
                }
                self.advertisment.text = self.advertismentText.text
                self.advertisment.image = self.avatarImage.image
                self.willDismiss?(self.advertisment, .save)
                self.dismissAction()
            }.disposed(by: bag)
        
        modalView.addSubview(avatarImage)
        modalView.addSubview(titleLabel)
        modalView.addSubview(closeButton)
        modalView.addSubview(advertismentText)
        modalView.addSubview(underlineView)
        modalView.addSubview(hideButton)
        modalView.addSubview(saveButton)
        
        modalView.snp.makeConstraints {
            $0.left.bottom.right.equalToSuperview()
            $0.height.equalTo(modalHeight)
        }
        
        closeButton.snp.makeConstraints {
            $0.top.equalToSuperview().offset(21)
            $0.right.equalToSuperview().offset(-21)
            $0.width.height.equalTo(14)
        }
        
        titleLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(16)
            $0.top.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-49)
        }
        
        avatarImage.snp.makeConstraints {
            $0.top.equalToSuperview().offset(56)
            $0.left.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-16)
            $0.height.equalTo(344)
        }
        
        advertismentText.snp.makeConstraints {
            $0.top.equalTo(avatarImage.snp.bottom).offset(16)
            $0.left.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-16)
        }
        
        underlineView.snp.makeConstraints {
            $0.top.equalTo(advertismentText.snp.bottom).offset(1)
            $0.left.equalTo(advertismentText.snp.left)
            $0.right.equalTo(advertismentText.snp.right)
            $0.height.equalTo(1)
        }
        
        hideButton.snp.makeConstraints {
            $0.top.equalTo(underlineView.snp.bottom).offset(32)
            $0.left.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-16)
            $0.height.equalTo(32)
        }
        
        saveButton.snp.makeConstraints {
            $0.top.equalTo(underlineView.snp.bottom).offset(80)
            $0.left.equalToSuperview().offset(16)
            $0.right.equalToSuperview().offset(-16)
            $0.height.equalTo(56)
        }
        
        dismissView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.bottom.equalTo(modalView.snp.top)
        }
    }
    
    @objc
    private func dismissAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc
    private func editTap() {
//        self.imagePicker.present(from: self.view)
        presentCustomPhotoModal() { [weak self] event in
            self?.dismiss(animated: true, completion: nil)
            switch event {
            case .camera:
                let vc = UIImagePickerController()
                vc.sourceType = .camera
                vc.allowsEditing = true
                vc.delegate = self
                self?.present(vc, animated: true)
            case .library:
                self?.presentPhotoPicker()
            }
        }
    }
    
    @objc
    func presentPhotoPicker() {
        let pickerViewController = PickerViewController()
        pickerViewController.allowMultipleSelection = false
        pickerViewController.numberOfPictureInRow = 3
        pickerViewController.isSimpleMode = true
        pickerViewController.isDarkMode = true
        pickerViewController.isAnimated = true
        pickerViewController.delegate = self
        pickerViewController.isSupportLandscape = false // A Boolean value that determines whether the ability of landscape exists.
        self.present(pickerViewController, animated: true, completion: nil)
    }
}

// MARK: - ImagePickerDelegate
extension AdvertismentModalViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        self.advertisment.image = avatarImage.image
        self.avatarImage.image = image
    }
}

extension AdvertismentModalViewController: UIViewControllerTransitioningDelegate {

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SlidingAnimationController(withDirection: .in)
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SlidingAnimationController(withDirection: .out)
    }
}

extension AdvertismentModalViewController: ConvenientImagePickerDelegate {
    func imagePickerDidCancel(_ selectedImages: [Int : UIImage]) {

    }
    
    func imageDidSelect(_ imagePicker: PickerViewController, index: Int, image: UIImage?) {
        self.advertisment.image = avatarImage.image
        self.avatarImage.image = image
    }
    
    func imageDidDeselect(_ imagePicker: PickerViewController, index: Int, image: UIImage?) {
        
    }
    
    func imageSelectMax(_ imagePicker: PickerViewController, wantToSelectIndex: Int, wantToSelectImage: UIImage?) {
        
    }
}


