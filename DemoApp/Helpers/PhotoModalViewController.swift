//
//  CustomModalViewController.swift
//  
//
//  Created by Артем Чурсин on 07.04.2021.
//

import UIKit
import RxSwift

class PhotoModalViewController: UIViewController {
    
    enum ImportType {
        case camera, library
    }

    private var cameraButton = RoundedCellButton()
    private var libraryButton = RoundedCellButton()
    private var closeButton = UIButton(type: .system)
    
    private let modalHeight = 233
    private var willDismiss: (() -> Void)?
    private var didSelect: ((ImportType) -> Void)?
    private var modalView: UIView!
    private let bag = DisposeBag()
    
    init(willDismiss: (() -> Void)? = nil,
         didSelect: ((ImportType) -> Void)? = nil) {

        self.willDismiss = willDismiss
        self.didSelect = didSelect
        super.init(nibName: nil, bundle: nil)
        transitioningDelegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    private func setupView() {
        let dismissView = UIView()
        dismissView.backgroundColor = .clear
        dismissView.isUserInteractionEnabled = true
        let dismissTap = UITapGestureRecognizer(target: self, action: #selector(dismissAction))
        dismissView.addGestureRecognizer(dismissTap)
        
        modalView = UIView()
        modalView.backgroundColor = Style.Colors.darkBackground
        
        self.view.addSubview(dismissView)
        self.view.addSubview(modalView)
        
        let vStack = UIStackView(arrangedSubviews: [cameraButton, libraryButton])
        vStack.axis = .horizontal
        vStack.spacing = 30
        
        cameraButton.setData(title: "Camera"
                                .attributed
                                .font(Style.Font.titleMediumCustom(value: 16).font)
                                .color(Style.Colors.textWhite),
                             image: R.image.ic_camera()) { [weak self] in
            self?.didSelect?(.camera)
        }
        
        libraryButton.setData(title: "Gallery"
                                .attributed
                                .font(Style.Font.titleMediumCustom(value: 16).font)
                                .color(Style.Colors.textWhite),
                              image: R.image.ic_gallery()) { [weak self] in
            self?.didSelect?(.library)
        }
        
        closeButton.setTitle("Close", for: [])
        closeButton.titleLabel?.font = Style.Font.titleMediumCustom(value: 18).font
        closeButton.setTitleColor(Style.Colors.primaryRed, for: [])
        closeButton.rx.tap
            .subscribe { [weak self] (_) in
                self?.dismissAction()
            }.disposed(by: bag)

        
        modalView.addSubview(vStack)
        modalView.addSubview(closeButton)
        
        cameraButton.snp.makeConstraints {
            $0.width.height.equalTo(120)
        }
        
        libraryButton.snp.makeConstraints {
            $0.width.height.equalTo(120)
        }
        
        vStack.snp.makeConstraints {
            $0.top.equalToSuperview().offset(16)
            $0.centerX.equalToSuperview()
        }
        
        modalView.snp.makeConstraints {
            $0.left.bottom.right.equalToSuperview()
            $0.height.equalTo(modalHeight)
        }
        
        closeButton.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(vStack.snp.bottom).offset(27)
        }
        
        dismissView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.bottom.equalTo(modalView.snp.top)
        }
    }
    
    @objc
    private func dismissAction() {
        self.willDismiss?()
        self.dismiss(animated: true, completion: nil)
    }
}

extension PhotoModalViewController: UIViewControllerTransitioningDelegate {

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SlidingAnimationController(withDirection: .in)
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return SlidingAnimationController(withDirection: .out)
    }
}

