//
//  Request.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Alamofire
import RxAlamofire
import RxSwift
import Device_swift
import RealmSwift

class Request: NSObject {
    private let disposeBag = DisposeBag()
    private let host: String
    private let authDataRepository: AuthDataRepositoryProtocol
    private let clientDataRepository: ClientDataRepositoryProtocol
//    private let settingsRepository: SettingsRepositoryProtocol
    private let realm = try! Realm()
    
    init(host: String, authDataRepository: AuthDataRepositoryProtocol, clientDataRepository: ClientDataRepositoryProtocol) {
        self.host = host
        self.clientDataRepository = clientDataRepository
        self.authDataRepository = authDataRepository
//        self.settingsRepository = SettingsRepository()
    }
    
    private func makeRxRequest<T: Codable>(method: HTTPMethod, path: Path, parameters: [String: Any]?) -> Observable<T> {
        var param: [String: Any]?
        param = parameters
//        param?["version"] = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
//        if let token = authDataRepository.get().first?.accessToken {
//            param?["accessToken"] = token
//        }

        let request: Observable<T> = makeRxAlamofireRequest(method: method, path: path, parameters: param).concatMap { [unowned self] dataRequest in
            self.responseDecodableObject(dataRequest: dataRequest)
        }
        return request
    }
    
//    private func responseDecodableObject<T: Codable>(dataRequest: DataRequest) -> Observable<T> {
//        let observable: Observable<T> = Observable.create { observer -> Disposable in
//            dataRequest.responseDecodableObject(decoder: self.getJSONDecoder()) { (response: AFDataResponse<T>) in
//                if let responseValue = response.value {
//                    do {
//                        if let data = dataRequest.data {
//                            let rError = try JSONDecoder().decode(RequestError.self, from: data)
//                            observer.onError(rError)
//                        } else {
//                            observer.onError(RequestError.unknownError)
//                        }
//
//                    } catch  {
//                        observer.onNext(responseValue)
//                        observer.onCompleted()
//                    }
////                    if let responseValueError = responseValue as? RequestError {
////                        observer.onError(responseValueError)
////                    }
//
//                    //DEBUG
////                    if let responseValueError = responseValue.error {
////                        observer.onError(responseValueError)
////                    }
////                    observer.onNext(responseValue.data!)
////                    observer.onCompleted()
//
////                    observer.onNext(responseValue)
////                    observer.onCompleted()
//                    //DEBUG
//
////                    if let responseValueData  = responseValue.data {
////                        if let responseValueError = responseValue.error {
////                            observer.onError(responseValueError)
////                        }
////                        observer.onNext(responseValueData)
////                        observer.onCompleted()
////                    } else if let responseValueError = responseValue.error {
////                        observer.onError(responseValueError)
////                    }
//                } else if let responseValue = response.error {
//                    observer.onError(RequestError.init(code: responseValue.responseCode ?? 0, description: responseValue.localizedDescription))
//                } else {
//                    observer.onError(RequestError.unknownError)
//                }
//            }
//            return Disposables.create()
//        }
//        return observable
//    }
    private func responseDecodableObject<T: Codable>(dataRequest: DataRequest) -> Observable<T> {
        let observable: Observable<T> = Observable.create { observer -> Disposable in
            dataRequest.responseDecodableObject(decoder: self.getJSONDecoder()) { (response: AFDataResponse<T>) in
                do {
                    if let data = dataRequest.data {
                        let rError = try JSONDecoder().decode(RequestError.self, from: data)
                        observer.onError(rError)
                    } else {
                        observer.onError(RequestError.unknownError)
                    }
                    
                } catch  {
                    if let responseValue = response.value {
                        
                        observer.onNext(responseValue)
                        observer.onCompleted()
                        
                        
                    } else if let responseValue = response.error {
                        observer.onError(RequestError.init(code: responseValue.responseCode ?? 0, description: responseValue.localizedDescription))
                    } else {
                        observer.onError(RequestError.unknownError)
                    }
                }
                
            }
            return Disposables.create()
        }
        return observable
    }
    
    private func makeRxAlamofireRequest(method: HTTPMethod, path: Path, parameters: [String: Any]?) -> Observable<DataRequest> {
        let headers = createHTTPHeaders()

        var params : [String: Any] = parameters ?? ["":""]
                
//        if let token = clientDataRepository.get()?.client?.userToken {
//        if let token = authDataRepository.get().first?.accessToken {
//            params["accessToken"] = token
//            debugPrint("\(params)")
//        }
        let request = RxAlamofire.request(method, "\(host)\(path.url)", parameters: params, encoding: getParameterEncoding(method), headers: headers)
        
        return request
    }
    
    private func createHTTPHeaders() -> HTTPHeaders {
        var parameters = HTTPHeaders()
//        if let authData = authDataRepository.get().first {
//            parameters["token"] = authData.accessToken
//            debugPrint("\(parameters)")
//        }
        if let token = authDataRepository.get().first?.accessToken {
            parameters["Authorization"] = "Bearer \(token)"
        }
//        parameters["Content-Type"] = "application/json"
//        parameters["os"] = "ios"
//        parameters["osVersion"] = UIDevice.current.systemVersion
//        parameters["device"] = UIDevice.current.deviceType.displayName
//        parameters["version"] = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "Unknown"
        
        return parameters
    }
    
    func cancelRequestWith(parameters: [String: Any], path: String, method: HTTPMethod) {
        Alamofire.Session.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
        }
    }
}


extension Request: RequestProtocol {
    func makeRxGetRequest<T>(path: Path, parameters: [String : Any]?) -> Observable<T> where T : Decodable, T : Encodable {
        return makeRxRequest(method: .get, path: path, parameters: parameters)
    }
    
    func makeRxPostRequest<T>(path: Path, parameters: [String : Any]?) -> Observable<T> where T : Decodable, T : Encodable {
        return makeRxRequest(method: .post, path: path, parameters: parameters)
    }
    
    func makeRxDeleteRequest<T>(path: Path, parameters: [String : Any]?) -> Observable<T> where T : Decodable, T : Encodable {
        return makeRxRequest(method: .delete, path: path, parameters: parameters)
    }
    
    func makeRxPutRequest<T>(path: Path, parameters: [String : Any]?) -> Observable<T> where T : Decodable, T : Encodable {
        return makeRxRequest(method: .put, path: path, parameters: parameters)
    }
    
    func makeRxMultipartFormDataRequest<T>(path: Path, data: Data, name: String, fileName: String, mimeType: String, parameters: [String : Any]?) -> Observable<T> where T : Decodable, T : Encodable {
        var headers = createHTTPHeaders()
        headers["Content-Type"] = "multipart/form-data"
        let request: Observable<T> = Observable.create { [unowned self] observer -> Disposable in
            AF.upload(multipartFormData: { multipartFormData in
                if let parameters = parameters {
                    for (key, value) in parameters {
                        multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                    }
                }
                multipartFormData.append(data, withName: name, fileName: fileName, mimeType: mimeType)
            }, to: "\(self.host)\(path.url)", method: .post , headers: headers)
                .responseDecodable(completionHandler: { (response: AFDataResponse<T>) in
                    if let responseValue = response.value {
                        observer.onNext(responseValue)
                        observer.onCompleted()
//                        if let responseValueData  = responseValue.data {
//                            if let responseValueError = responseValue.error {
//                                observer.onError(responseValueError)
//                            }
//                            observer.onNext(responseValueData)
//                            observer.onCompleted()
//                        } else if let responseValueError = responseValue.error {
//                            observer.onError(responseValueError)
//                        }
                    } else if let responseValue = response.error {
                        observer.onError(RequestError.init(code: responseValue.responseCode ?? 0, description: responseValue.errorDescription))
                    } else {
                        observer.onError(RequestError.unknownError)
                    }
                })
            return Disposables.create()
        }
        return request
    }
}

extension Request {
    private func getParameterEncoding(_ method: HTTPMethod) -> ParameterEncoding {
        return method == .get ? URLEncoding.default : JSONEncoding.default
    }
    
    private func getJSONDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        return decoder
    }
}

