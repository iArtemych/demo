//
//  ErrorCode.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

enum ErrorCode {
    case refresh
    case registration
    case userRegistraition
    case other
    case wait
}
