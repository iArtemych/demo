//
//  Response.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation

struct Response<T: Codable> {
    var data: T?
    var error: RequestError?
    var success: Bool?
//    var notification: NotificationApiObject?

    enum CodingKeys: String, CodingKey {
        case success
        case data
        case error
        case notification
    }

    enum ErrorCodingKeys: String, CodingKey {
        case error = "errors"
    }
}

extension Response: Decodable {
    
    init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let success = try? container.decode(Bool.self, forKey: .success), success == false {
            data = nil
            if let error = try? container.decode(RequestError.self, forKey: .data) {
                self.error = error
                DispatchQueue.main.async {
                    if NSLocalizedString("unknown-error", comment: "") != error.description ?? "" {
                    }
                }
            } else {
                error = RequestError.notValidData
            }
        } else if let data = try? container.decode(T.self, forKey: .data) {
            self.data = data
            error = nil
        } else {
            data = nil
            if let error = try? container.decode(RequestError.self, forKey: .error) {
                self.error = error
            } else {
                error = RequestError.nilData
            }
        }
    }
    
}

class Empty: Codable {

    convenience required init(from decoder: Decoder) throws {
        self.init()
    }
}

