//
//  RequestPath.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation

enum Path {
    case empty
    
    //Application
    case updateAccessToken
    
    //Auth
    case checkCode
    case getCode
    
    //Profile
    case profile
    case poster
    
    //support
    case supportRequest
    
    //api/v1/dictionaries/regions
    case getRegions
    
    var url: String {
        switch self {
        case .empty:
            return ""
        //Application
        case .updateAccessToken:
            return "api/v1/auth/refresh-token"
            
        //Auth
        case .getCode:
            return "api/v1/auth/registration"
        case .checkCode:
            return "api/v1/auth/confirm"
            
        //Profile
        case .profile:
            return "api/v1/profile"
        case .poster:
            return "api/v1/profile/poster"
            
        //regions
        case .getRegions:
            return "api/v1/dictionaries/regions"
            
        //support
        case .supportRequest:
            return ""
        }
    }
}


