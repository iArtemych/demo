//
//  RequestProtocol.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Alamofire
import RxSwift

protocol RequestProtocol {
    func makeRxGetRequest<T: Codable>(path: Path, parameters: [String: Any]?) -> Observable<T>
    func makeRxPostRequest<T: Codable>(path: Path, parameters: [String: Any]?) -> Observable<T>
    func makeRxDeleteRequest<T: Codable>(path: Path, parameters: [String: Any]?) -> Observable<T>
    func makeRxPutRequest<T: Codable>(path: Path, parameters: [String: Any]?) -> Observable<T>
    func makeRxMultipartFormDataRequest<T: Codable>(path: Path, data: Data, name: String, fileName: String, mimeType: String, parameters: [String: Any]?) -> Observable<T>
    func cancelRequestWith(parameters: [String: Any], path: String, method: HTTPMethod)
}


enum RequestResult<T> {
    case success(T)
    case failure(Error)
}

