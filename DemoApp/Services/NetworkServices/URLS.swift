//
//  URLS.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import UIKit

class URLS {
    enum Server {
        case develop
        case production
    }
    
    static var apiImage: String { return "" }
    static var api: String { return getApi() }

    static var isMocked: Bool {
        return true
    }

    static var isDebugServer: Bool {
        return isDebug != .production
    }
    
    static var isDebugMode = false

    private static var isDebug: Server {
        #if DEBUG
            return .develop
        #else
            return .production
        #endif
    }

    private static func getApi() -> String {
        var serverUrl = ""
        switch isDebug {
        case .develop:
            isDebugMode = UserDefaults.standard.bool(forKey: "server")
            if isDebugMode {
                serverUrl = ""
            } else {
                serverUrl = ""
            }
        case .production:
            serverUrl = ""
        }
        return serverUrl
    }
}
