//
//  RequestManager.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Alamofire
import RxSwift
import RealmSwift
import Foundation

class RequestManager: NSObject {
    private let disposeBag = DisposeBag()
    private let request: RequestProtocol
    var authorizationInteractor: AuthorizationInteractor?
    var clientDataRepository: ClientDataRepositoryProtocol?
    var authDataRepository: AuthDataRepositoryProtocol?

    class var instance: RequestManager {
        struct Singleton {
            static let instance = RequestManager(request: Request(host: URLS.api, authDataRepository: AuthDataRepository(realm: try! Realm()), clientDataRepository: ClientDataRepository(realm: try! Realm())))
        }
        return Singleton.instance
    }

    private var isAuthRequest = false
    private var requests: [(type: RequestType, path: Path, parameters: [String : Any]?)] = []

    init(request: RequestProtocol) {
        self.request = request
    }
}

extension RequestManager: RequestProtocol {
    //  отмена запроса
    func cancelRequestWith(parameters: [String : Any], path: String, method: HTTPMethod) {
        request.cancelRequestWith(parameters: parameters, path: path, method: method)
    }

    // get запрос
    func makeRxGetRequest<T>(path: Path, parameters: [String : Any]?) -> Observable<T> where T : Decodable, T : Encodable {
        let getRequest: Observable<T> = request.makeRxGetRequest(path: path, parameters: parameters).catchError { [unowned self] error in
            return self.checkError(error: error, type: .get, path: path, parameters: parameters)
        }
        return getRequest
    }

    // post запрос
    func makeRxPostRequest<T>(path: Path, parameters: [String : Any]?) -> Observable<T> where T : Decodable, T : Encodable {
        let postRequest: Observable<T> = request.makeRxPostRequest(path: path, parameters: parameters).catchError { [unowned self] error in
            return self.checkError(error: error, type: .post, path: path, parameters: parameters)
        }
        return postRequest
    }

    // delete запрос
    func makeRxDeleteRequest<T>(path: Path, parameters: [String : Any]?) -> Observable<T> where T : Decodable, T : Encodable {
        let deleteRequest: Observable<T> = request.makeRxDeleteRequest(path: path, parameters: parameters).catchError { [unowned self] error in
            return self.checkError(error: error, type: .delete, path: path, parameters: parameters)
        }
        return deleteRequest
    }

    // put запрос
    func makeRxPutRequest<T>(path: Path, parameters: [String : Any]?) -> Observable<T> where T : Decodable, T : Encodable {
        let putRequest: Observable<T> = request.makeRxPutRequest(path: path, parameters: parameters).catchError { [unowned self] error in
            return self.checkError(error: error, type: .put, path: path, parameters: parameters)
        }
        return putRequest
    }

    // запрос для отправки файлов
    func makeRxMultipartFormDataRequest<T>(path: Path, data: Data, name: String, fileName: String, mimeType: String, parameters: [String : Any]?) -> Observable<T> where T : Decodable, T : Encodable {
        return request.makeRxMultipartFormDataRequest(path: path, data: data, name: name, fileName: fileName, mimeType: mimeType, parameters: parameters)
    }
}

extension RequestManager {

    // в случае ошибки, проверяем является ли ошибка токена
    private func handlerError(_ error: Error) -> ErrorCode {
        guard let error = error as? RequestError else { return .other }
        // isAuthRequest для отправки только одного запроса на авторизацию и рефреш токена
        //остальные запросы сохраняются в массив для повторной отправки после авторизации
        print("fuckink code \(error.code)")
        if isAuthRequest && error.code != 1000 { return .wait }

        switch error.code {
        case 403: // ошибка рефреша
            return .refresh
        case 1000: // ошибка авторизации
            logout()
            return .other
            
            
        case 1110, 1001: // ошибка авторизации
            return .registration
        case 400:
//            logout()
            return .other
//            return .userRegistraition
        case 401:
            // если токен и рефреш токен отсутсвует, выходим из аккаунта и заново регистрируем пользователя
            isAuthRequest = false

            return .registration
        case 1120: // ошибка токена
            logout()
            return .other
        default:
            return .other
        }
    }

    //разлогин пользователя и переход на главный экран
    private func logout() {
        authorizationInteractor?.logout()
    }

    // .refresh, .registration - создаем запрос(рефреш токен, авторизация и тд) в зависимости от ошибки
    // .other - возвращаем ошибку
    // .wait - добавляем запросы в массив requests
    private func checkError<T: Codable>(error: Error, type: RequestType, path: Path, parameters: [String : Any]?) -> Observable<T> {
        print(error)
        switch handlerError(error) {
        case .refresh:
            return makeRxAuthRequest(type: type, path: path, parameters: parameters)
        case .registration:
            return makeRxAuthRequest(type: type, path: path, parameters: parameters)
        case .other:
            return Observable.error(error)
        case .wait:
            requests.append((type: type, path: path, parameters: parameters))
            return Observable.empty()
        case .userRegistraition:
//            makeRxAuthRequest(type: type, path: path, parameters: parameters)
            return Observable.error(error)
        }
    }
    
    // авторизация пользователя
    func makeRxAuthRequest<T: Codable>(type: RequestType, path: Path, parameters: [String: Any]?) -> Observable<T> {
        isAuthRequest = true
        
        return authorizationInteractor!.updateRefreshToken().flatMap({ [unowned self] _ in
//            print(response)
            return self.createRepeatRequest(type: type, path: path, parameters: parameters)
        }).retry(5).catchError({ [unowned self] error in
            self.isAuthRequest = false
            self.authorizationInteractor?.logout()
            return self.makeRxAuthRequest(type: type, path: path, parameters: parameters)
        })
    }

    // заново посылаем запросы из массива
    private func createRepeatRequest<T: Codable>(type: RequestType,
                                                 path: Path,
                                                 parameters: [String: Any]?) -> Observable<T> {

        requests.append((type: type, path: path, parameters: parameters))
        print("Запрос на авторизацию или обновления токена прошел успешно")

        for request in requests {
            switch request.type {
            case .get:
                return makeRxGetRequest(path: request.path, parameters: request.parameters)
            case .post:
                return makeRxPostRequest(path: request.path, parameters: request.parameters)
            case .put:
                return makeRxPutRequest(path: request.path, parameters: request.parameters)
            case .delete:
                return makeRxDeleteRequest(path: request.path, parameters: request.parameters)
            }
        }
        self.isAuthRequest = false
        requests.removeAll()
        return Observable.empty()
    }
}
