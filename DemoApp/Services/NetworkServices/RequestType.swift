//
//  RequestType.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation

enum RequestType {
    case get
    case post
    case delete
    case put
}

