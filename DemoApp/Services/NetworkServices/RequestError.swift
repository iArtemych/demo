//
//  RequestError.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation

struct RequestError: Error, Codable {
    let code: Int
    let description: String?

    static var unknownError: RequestError {
        return RequestError(code: 9999, description: String.Errors.unknownError)
    }
    static var notValidData: RequestError {
        return RequestError(code: 9999, description: String.Errors.notValidData)
    }
    static var nilData: RequestError {
        return RequestError(code: 9999, description: String.Errors.nilData)
    }
    
    enum CodingKeys: String, CodingKey {
        case code, description
    }
    
    public init(code: Int, description: String?) {
        self.code = code
        self.description = description
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        code = try container.decode(Int.self, forKey: .code)
        description = try container.decode(String.self, forKey: .description)
    }
}

extension String {
    enum Errors {
        static var unknownError: String {
            return NSLocalizedString("unknown-error", comment: "")
        }
        static var notValidData: String {
            return "Not valid data"
        }
        static var nilData: String {
            return "Nil data"
        }
    }
}

// MARK: - LocalizedError

extension RequestError: LocalizedError {
    var errorDescription: String? {
        return description
    }
    
    var errorCode: Int {
        return code
    }
}


