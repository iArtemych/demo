//
//  AuthorizationInteractor.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import RxSwift
import Realm

protocol AuthorizationInteractor {
//    ///application/generate/token/list/
//    func registration() -> Observable<AuthData>
//    ///application/guide/list/
//    func getGuideList() -> Observable<GuideList>
    
    ///api/v1/auth/registration
    func getCode(email: String) -> Observable<Empty>
    ///api/v1/auth/confirm
    func checkCode(code: String, email: String) -> Observable<CheckCode>
    ///logout
    func logout()
    
    ///api/v1/auth/refresh-token
    func updateRefreshToken() -> Observable<AuthData>
    
    ///api/v1/profile
    func checkProfile() -> Observable<ClientApiObject>
    
    ///api/v1/profile
    func uploadProfile(profile: ClientApiObject) -> Observable<Empty>
    
    ///api/v1/profile
    func getRegionList() -> Observable<[Region]>
}
