//
//  AuthorizationService.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import RxSwift
import Realm

class AuthorizationService: AuthorizationInteractor {
    
    var request: RequestProtocol = RequestManager.instance
    private let disposeBag = DisposeBag()
    let authDataRepository: AuthDataRepositoryProtocol
    let clientDataRepository: ClientDataRepositoryProtocol
    
    init(authDataRepository: AuthDataRepositoryProtocol , clientDataRepository: ClientDataRepositoryProtocol) {
        self.authDataRepository = authDataRepository
        self.clientDataRepository = clientDataRepository
    }
    
    ///api/v1/auth/registration
    func getCode(email: String) -> Observable<Empty> {
        let parameters = ["email": email]
        let requestCreate: Observable<Empty> = request.makeRxPutRequest(path: .getCode, parameters: parameters)
        return requestCreate
    }
    
    ///api/v1/auth/confirm
    func checkCode(code: String, email: String) -> Observable<CheckCode> {
        var parameters: [String: Any] = ["code": code]
        parameters["email"] = email
        let requestCreate: Observable<CheckCode> = request.makeRxPutRequest(path: .checkCode, parameters: parameters)
//        requestCreate.subscribe(onNext: { [weak self] authUser in
//            guard let strongSelf = self else { return }
//            let authRealm = AuthDataRealm()
//            authRealm.accessToken = authUser.token
//            authRealm.refreshToken = authUser.refreshToken
//            strongSelf.authDataRepository.save(authData: authRealm)
//        }, onError: { error in
//
//        }, onCompleted: {
//
//        }).disposed(by: self.disposeBag)
        return requestCreate
    }
    
    ///api/v1/profile
    func checkProfile() -> Observable<ClientApiObject> {
        let parameters: [String: Any] = [:]
        let requestCreate: Observable<ClientApiObject> = request.makeRxGetRequest(path: .profile, parameters: parameters)
//        requestCreate.subscribe(onNext: { [weak self] authUser in
//            guard let self = self else { return }
//            if authUser.email != nil {
//                self.clientDataRepository.save(authData: authUser)
//            }
//        }, onError: nil, onCompleted: nil)
//        .disposed(by: self.disposeBag)
        return requestCreate
    }
    
    ///api/v1/profile
    func uploadProfile(profile: ClientApiObject) -> Observable<Empty> {
        let parameters: [String: Any] = profile.toParametrs()
        let requestCreate: Observable<Empty> = request.makeRxPutRequest(path: .profile, parameters: parameters)
        return requestCreate
    }
    
    func logout() {
        guard let token = authDataRepository.get().first?.accessToken,
           !token.isEmpty else { return }

        authDataRepository.deleteAllData()
        clientDataRepository.deleteAllOldAuthData()
        NotificationCenter.default.post(name: .logout, object: nil)
    }
    
    ///api/v1/auth/refresh-token
    func updateRefreshToken() -> Observable<AuthData> {
        var parameters: [String: Any] = [:]
        let refreshToken = authDataRepository.get().first?.refreshToken
        parameters["refreshToken"] = refreshToken
        let requestCreate: Observable<AuthData> = request.makeRxPutRequest(path: .updateAccessToken, parameters: parameters)
        requestCreate.subscribe(onNext: { [weak self] authUser in
            guard let strongSelf = self else { return }
            let authRealm = AuthDataRealm()
            authRealm.accessToken = authUser.accessToken
            authRealm.refreshToken = authUser.refreshToken
            authRealm.accessTokenLifeTime = authUser.accessTokenLifeTime
            strongSelf.authDataRepository.save(authData: authRealm)
        }, onError: { error in

        }, onCompleted: {

        }).disposed(by: self.disposeBag)
        return requestCreate
    }
    
    ///api/v1/dictionaries/regions
    func getRegionList() -> Observable<[Region]> {
        let parameters: [String: Any] = [:]
        let requestCreate: Observable<[Region]> = request.makeRxGetRequest(path: .getRegions, parameters: parameters)
        return requestCreate
    }
    
//    //MARK: - Profile
//    ///mobile/profile/city/list/
//    func getCityList(latitude: Double?, longitude: Double?) -> Observable<CityList> {
//        var parameters: [String: Any] = [:]
//        parameters["latitude"] = latitude
//        parameters["longitude"] = longitude
//        let requestCreate: Observable<CityList> = request.makeRxPostRequest(path: .cityList, parameters: parameters)
//        return requestCreate
//    }
//
//    ///mobile/profile/save/
//    func saveProfile(cityId: Int, email: String, specializationId: Int) -> Observable<ClientApiObject> {
//        var parameters: [String: Any] = [:]
//        parameters["cityId"] = cityId
//        parameters["email"] = email
//        parameters["specializationId"] = specializationId
//        let requestCreate: Observable<ClientApiObject> = request.makeRxPutRequest(path: .saveProfile, parameters: parameters)
//
//        requestCreate.subscribe(onNext: { [weak self] authUser in
//            guard let strongSelf = self else { return }
//            strongSelf.clientDataRepository.save(authData: authUser)
//        }, onError: { error in
//
//        }, onCompleted: {
//
//        }).disposed(by: self.disposeBag)
//
//        return requestCreate
//    }
//
//    ///mobile/profile/get/
//    func getProfile() -> Observable<ClientApiObject> {
//        let parameters: [String: Any] = [:]
//        let requestCreate: Observable<ClientApiObject> = request.makeRxPostRequest(path: .getProfile, parameters: parameters)
//        return requestCreate
//    }
}
