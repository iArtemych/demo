//
//  ProfileInteractor.swift
//  
//
//  Created by Артем Чурсин on 19.04.2021.
//

import RxSwift

protocol ProfileInteractor {
    ///logout
    func logout()
    
    ///api/v1/profile
    func getProfile() -> Observable<ClientApiObject>
    
    ///api/v1/profile/poster
    func getPoster() -> Observable<Poster>
    
    ///api/v1/profile/poster
    func postPoster(image: String,
                    originalImageName: String,
                    text: String) -> Observable<Poster>
    
    ///api/v1/profile/poster
    func hidePoster() -> Observable<Empty>
}
