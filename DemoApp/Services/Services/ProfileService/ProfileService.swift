//
//  ProfileService.swift
//  
//
//  Created by Артем Чурсин on 19.04.2021.
//

import RxSwift
import Realm

class ProfileService: ProfileInteractor {
    
    var request: RequestProtocol = RequestManager.instance
    private let disposeBag = DisposeBag()
    
    
    func logout() {
        RequestManager.instance.authorizationInteractor?.logout()
    }
    
    func getProfile() -> Observable<ClientApiObject> {
        let parameters: [String: Any] = [:]
        let requestCreate: Observable<ClientApiObject> = request.makeRxGetRequest(path: .profile, parameters: parameters)
        return requestCreate
    }
    
    func getPoster() -> Observable<Poster> {
        let parameters: [String: Any] = [:]
        let requestCreate: Observable<Poster> = request.makeRxGetRequest(path: .poster, parameters: parameters)
        return requestCreate
    }
    
    func postPoster(image: String,
                    originalImageName: String,
                    text: String) -> Observable<Poster> {
        var parameters: [String: Any] = [:]
        parameters["image"] = image
        parameters["originalImageName"] = originalImageName
        parameters["text"] = text
        let requestCreate: Observable<Poster> = request.makeRxPostRequest(path: .poster, parameters: parameters)
        return requestCreate
    }
    
    func hidePoster() -> Observable<Empty> {
        let parameters: [String: Any] = [:]
        let requestCreate: Observable<Empty> = request.makeRxDeleteRequest(path: .poster, parameters: parameters)
        return requestCreate
    }
}
