//
//  AppDelegate.swift
//  DemoApp
//
//  Created by Artem Chursin on 04.05.2021.
//

import UIKit
import IQKeyboardManagerSwift
import RxSwift
//import Firebase
//import FirebaseMessaging
import Bagel

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var rootController: UINavigationController {
      return self.window!.rootViewController as! UINavigationController
    }

    private lazy var applicationCoordinator: Coordinator = self.makeCoordinator()
    
    private var authDataRepository: AuthDataRepositoryProtocol!
    private var clientDataRepository: ClientDataRepositoryProtocol!
    private let bag = DisposeBag()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        #if DEBUG
        Bagel.start()
        #endif
        
//        FirebaseApp.configure()
        
        createRepositories()
        setupLogging()
        
        IQKeyboardManager.shared.enable = true
        
        let notification = launchOptions?[.remoteNotification] as? [String: AnyObject]
        let deepLink = DeepLinkOption.build(with: notification)
        applicationCoordinator.start(with: deepLink)
        createInteractorForRequestManager()
        
        return true
    }

}

extension AppDelegate {
    private func makeCoordinator() -> Coordinator {
        let coordinatorFactory = CoordinatorFactoryImp()

        return ApplicationCoordinator(router: RouterImp(rootController: self.rootController),
                                      coordinatorFactory: coordinatorFactory,
                                      clientDataRepository: clientDataRepository,
                                      authDataRepository: authDataRepository)
    }
    
    private func createInteractorForRequestManager() {

        let interactor: AuthorizationInteractor = AuthorizationService(
            authDataRepository: authDataRepository,
            clientDataRepository: clientDataRepository)
        RequestManager.instance.authorizationInteractor = interactor
        RequestManager.instance.clientDataRepository = clientDataRepository
        RequestManager.instance.authDataRepository = authDataRepository
    }
    
    private func createRepositories() {
        let repositoriesFactory = RepositoriesFactory()
        self.authDataRepository = repositoriesFactory.createAuthDataRepository()
        self.clientDataRepository = repositoriesFactory.createClientDataRepository()
    }
    
    private func setupLogging() {
        NetworkActivityLogger.shared.level = .debug
        NetworkActivityLogger.shared.startLogging()
    }
}

