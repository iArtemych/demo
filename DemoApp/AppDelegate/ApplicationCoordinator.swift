//
//  ApplicationCoordinator.swift

//
//  Created by Артем Чурсин on 05.04.2021.
//

import Foundation
import RealmSwift

final class ApplicationCoordinator: BaseCoordinator {

    private let coordinatorFactory: CoordinatorFactory
    private let router: Router
    private var notificationSubscription: NSObjectProtocol?
    private let authDataRepository: AuthDataRepositoryProtocol
    private let clientDataRepository: ClientDataRepositoryProtocol

    init(router: Router,
         coordinatorFactory: CoordinatorFactory,
         clientDataRepository: ClientDataRepositoryProtocol,
         authDataRepository: AuthDataRepositoryProtocol) {

        self.router = router
        self.coordinatorFactory = coordinatorFactory
        self.authDataRepository = authDataRepository
        self.clientDataRepository = clientDataRepository

        super.init()

        notificationSubscription = NotificationCenter.default.addObserver(
            forName: .logout,
            object: nil,
            queue: .main,
            using: { [weak self] notification in
                guard let strongSelf = self else { return }

                strongSelf.router.popToRootModule(animated: false)
                strongSelf.runAuthFlow()
            })
    }
    
    override func start(with option: DeepLinkOption?) {
        //start with deepLink
        if let option = option {
            switch option {
            case .login: runLaunchFlow()
            default:
                childCoordinators.forEach { coordinator in
                    coordinator.start(with: option)
                }
            }
            // default start
        } else {
            runLaunchFlow()
            //TEST
//            runMainFlow()
            //TEST
        }
    }
    
    private func runLaunchFlow() {
        let coordinator = coordinatorFactory.makeLaunchCoordinatorBox(router: router,
                                                                      authDataRepository: authDataRepository,
                                                                      clientDataRepository: clientDataRepository)
        coordinator.finishFlow = { [weak self, weak coordinator] nextFlow in
            self?.removeDependency(coordinator)
            self?.runAuthFlow()
            switch nextFlow {
            case .showAuth:
                self?.runAuthFlow()
            case .showMain:
                self?.runMainFlow()
            }
        }
        addDependency(coordinator)
        coordinator.start()
    }
    
    private func runAuthFlow() {
        let coordinator = coordinatorFactory.makeAuthCoordinatorBox(router: router,
                                                                    authDataRepository: authDataRepository,
                                                                    clientDataRepository: clientDataRepository)
        coordinator.finishFlow = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
            self?.runMainFlow()
        }
        addDependency(coordinator)
        coordinator.start()
    }

    private func runMainFlow() {
        let coordinator = coordinatorFactory.makeTabbarCoordinator(router: router, clientDataRepository: ClientDataRepository(realm: try! Realm()))
        addDependency(coordinator)
        coordinator.start()
    }
}


