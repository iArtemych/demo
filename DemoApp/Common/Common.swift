//
//  Common.swift
//  
//
//  Created by Artem Chursin on 25.03.2021.
//

import Foundation
import RxSwift
import RxCocoa

// MARK: - Types

typealias VoidClosure = () -> Void
typealias BoolClosure = (Bool) -> Void
typealias IntClosure = (Int) -> Void
typealias StringClosure = (String) -> Void
typealias DateClosure = (Date) -> Void

